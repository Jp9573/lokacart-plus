package com.mobile.ict.lokacartplus.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Toshiba on 20/3/17.
 */
@SuppressWarnings("DefaultFileTemplate")
public class ProductTypeDetail {

    @SerializedName("typename")
    private String name;

    public ProductTypeDetail(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
