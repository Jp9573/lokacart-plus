package com.mobile.ict.lokacartplus.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.model.ProductDetail;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.adapter.ConfirmOrderAdapter;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConfirmOrderActivity extends AppCompatActivity {

    private TextView tvShowAddress;
    private TextView tvCommentArea;
    private Button buttonPlaceOrder;
    private final String TAG = ConfirmOrderActivity.class.getSimpleName();
    private String total = null;
    private String address = MemberDetails.getAddress();
    private EditText etAddress, etCommentDialog;
    private Dialog changeAddressDialogView, commentDialogView;
    private NetworkCommunicator networkCommunicator;
    private Context mContext;
    private int size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);
        mContext = this;
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Confirm Order");

        networkCommunicator = NetworkCommunicator.getInstance();

        tvCommentArea = findViewById(R.id.tvCommentArea);
        tvCommentArea.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));
        tvCommentArea.setMovementMethod(new ScrollingMovementMethod());
        tvCommentArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCommentDialog();
            }
        });

        tvShowAddress = findViewById(R.id.tvShowAddress);
        tvShowAddress.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));
        tvShowAddress.setText(MemberDetails.getAddress());

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView confirmOrderRecyclerView = findViewById(R.id.confirm_order_recycler_view);
        confirmOrderRecyclerView.setHasFixedSize(false);
        confirmOrderRecyclerView.setNestedScrollingEnabled(false);

        confirmOrderRecyclerView.setLayoutManager(layoutManager);

        Bundle extras = getIntent().getExtras();
        total = extras.getString("cartTotal");
        TextView cartTotal = findViewById(R.id.tvTotal);
        cartTotal.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));
        cartTotal.setText(String.valueOf("\u20B9 " + total));

        ConfirmOrderAdapter confirmOrderAdapter = new ConfirmOrderAdapter();
        confirmOrderRecyclerView.setAdapter(confirmOrderAdapter);

        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        buttonPlaceOrder = findViewById(R.id.buttonPlaceOrder);
        buttonPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               buttonPlaceOrder.setEnabled(false);
                if(Master.isNetworkAvailable(getApplicationContext())){
                    Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
                    placeOrderFunction();
                }else {
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                }

            }
        });

        findViewById(R.id.tvChangeAddress).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChangeAddressDialog();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(networkCommunicator==null) {
            networkCommunicator = NetworkCommunicator.getInstance();
        }
    }

    private void placeOrderFunction(){

        JSONArray orderItemsArray = new JSONArray();

        size = Master.cartList.size();
        for(int i = 0; i< size; i++){
            JSONObject orderItemsObject = new JSONObject();
            ProductDetail productDetail = Master.cartList.get(i);
            try {
                orderItemsObject.put("prod_id", String.valueOf(productDetail.getId()));
                orderItemsObject.put("quantity", String.valueOf(productDetail.getQuantity()));
                orderItemsObject.put("unit_rate", String.valueOf(productDetail.getUnitRate()));
                orderItemsObject.put("gst",String.valueOf(productDetail.getGst()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            orderItemsArray.put(orderItemsObject);
        }

        JSONObject orderObject = new JSONObject();
        try {
            orderObject.put("phone", String.valueOf(MemberDetails.getMobileNumber()));
            orderObject.put("comments", String.valueOf(tvCommentArea.getText().toString().trim()));
            orderObject.put("amount", String.valueOf(total));
            orderObject.put("address", address);
            orderObject.put("orderItems", orderItemsArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        networkCommunicator.data(Master.getPlaceOrderAPI(),
                Request.Method.POST,
                orderObject,
                false,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null) {
                            checkStatus(obj);
                        }
                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
                        buttonPlaceOrder.setEnabled(true);
                    }
                },Master.CONFIRM_ORDER_ACTIVITY_TAG, getApplicationContext());

    }

    private void checkStatus(JSONObject response){

        try {
            if(response.getString("status").equals("success")){

                Master.CART_ITEM_COUNT = 0;

                size = Master.cartList.size();
                int size2 = Master.productDetailList.size();
                for (int i = 0; i < size; i++) {
                    for (int j = 0; j < size2; ++j) {
                        if (Master.productDetailList.get(j).getId().equals(Master.cartList.get(i).getId())) {
                            Master.productDetailList.get(j).setQuantity(0);
                            break;
                        }
                    }
                }


                Master.cartList.clear();

                DBHelper dbHelper=DBHelper.getInstance(this);
                dbHelper.deleteCart(MemberDetails.getMobileNumber());

                Intent i = new Intent(ConfirmOrderActivity.this, OrderSubmitActivity.class);
                startActivity(i);
                finish();
            }
            else {
                parseJSON(response);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void parseJSON(JSONObject jsonObject)
    {
        JSONArray error;

        try {

            error = jsonObject.getJSONArray("error");
            StringBuilder errorString = new StringBuilder();

            for(int i=0;i<error.length();i++){
                JSONObject jo = error.getJSONObject(i);

                if(error.length()==1){
                    errorString.append("Only ").append(jo.getString("available")).append(" ").append(jo.getString("unit")).append(" available of ").append(jo.getString("name"));
                    break;
                } else {
                    errorString.append("Only ").append(jo.getString("available")).append(" ").append(jo.getString("unit")).append(" available of ").append(jo.getString("name")).append("\n");
                }

            }

            Toast.makeText(getApplicationContext(), errorString.toString(), Toast.LENGTH_LONG).show();

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void openChangeAddressDialog(){

        changeAddressDialogView = new Dialog(ConfirmOrderActivity.this);
        changeAddressDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        changeAddressDialogView.setContentView(R.layout.dialog_change_address);
        changeAddressDialogView.setCancelable(true);
        changeAddressDialogView.setCanceledOnTouchOutside(false);

        changeAddressDialogView.show();

        etAddress = changeAddressDialogView.findViewById(R.id.etAddress);
        etAddress.setText(tvShowAddress.getText().toString().trim());
        Button buttonChangeAddress = changeAddressDialogView.findViewById(R.id.buttonChangeAddress);
        buttonChangeAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etAddress.getText().toString().trim().equals("")){
                    changeAddressDialogView.dismiss();
                    tvShowAddress.setText(etAddress.getText().toString().trim());
                    address = etAddress.getText().toString().trim();
                    Toast.makeText(getApplicationContext(), "Delivery Address Changed Successfully! ", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void openCommentDialog(){

        commentDialogView = new Dialog(ConfirmOrderActivity.this);
        commentDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        commentDialogView.setContentView(R.layout.dialog_comment_order);
        commentDialogView.setCancelable(true);
        commentDialogView.setCanceledOnTouchOutside(false);

        commentDialogView.show();

        etCommentDialog = commentDialogView.findViewById(R.id.etCommentDialog);
        etCommentDialog.setText(tvCommentArea.getText().toString().trim());
        Button buttonSendComment = commentDialogView.findViewById(R.id.buttonSendComment);
        buttonSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etCommentDialog.getText().toString().trim().equals("")){
                    commentDialogView.dismiss();
                    tvCommentArea.setText(etCommentDialog.getText().toString().trim());
                    Toast.makeText(getApplicationContext(), "Comments Added Successfully! ", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
