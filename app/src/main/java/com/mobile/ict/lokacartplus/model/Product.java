package com.mobile.ict.lokacartplus.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by jay on 10/1/18.
 */

@SuppressWarnings("DefaultFileTemplate")
public class Product {
    @SerializedName("products")
    public ArrayList<ProductDetail> productDetails = new ArrayList<>();
}
