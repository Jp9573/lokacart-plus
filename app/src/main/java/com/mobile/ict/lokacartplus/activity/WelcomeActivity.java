package com.mobile.ict.lokacartplus.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.utils.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

public class WelcomeActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private AutoScrollCountDownTimer autoScrollCountDownTimer;
    private LinearLayout dotsLayout;
    private int[] layouts, ids, drawables;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_welcome);

        viewPager = findViewById(R.id.view_pager);
        dotsLayout = findViewById(R.id.layoutDots);
        Button loginButton = findViewById(R.id.login_button);
        Button registerButton = findViewById(R.id.register_button);
        AppCompatTextView skipAndShop = findViewById(R.id.skipandshop);
        skipAndShop.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_arrow_forward_black_24dp,0); // to support devices below 5.0
        skipAndShop.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{ R.layout.welcome_slide1,
                R.layout.welcome_slide2,
                R.layout.welcome_slide3 };

        ids = new int[]{ R.id.stepperImageView1,
                R.id.stepperImageView2,
                R.id.stepperImageView3 };

        drawables = new int[]{ R.drawable.stepper_a,
                R.drawable.stepper_b,
                R.drawable.stepper_c };

        // adding bottom dots
        addBottomDots(0);

        MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        //auto scroll
        autoScrollCountDownTimer = new AutoScrollCountDownTimer();
        viewPager.setCurrentItem(3);  //to initiate from first slide
        autoScrollCountDownTimer.start();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoScrollCountDownTimer.cancel();
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoScrollCountDownTimer.cancel();
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
            }
        });

        skipAndShop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoScrollCountDownTimer.cancel();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Glide.get(getApplicationContext()).clearDiskCache();
                    }
                }).start();

                Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(i);
                finish();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        DBHelper dbHelper=DBHelper.getInstance(this);
        dbHelper.getProfile();

        if (SharedPreferenceConnector.readString(this, "redirectedfrom", "").equals("Scrolling_Activity")) {
            SharedPreferenceConnector.writeString(this, "redirectedfrom", "");

        } else if(MemberDetails.getMobileNumber() != null) {
            if(!MemberDetails.getMobileNumber().equals("guest")) {
                finish();
            }
        }

    }

    private void addBottomDots(int currentPage) {
        TextView[] dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    //	viewpager change listener
    private final ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            ImageView stepperImageView = view.findViewById(ids[position]);
            Glide.with(getApplication()).load(drawables[position]).thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(stepperImageView);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    public class AutoScrollCountDownTimer extends CountDownTimer {

        public AutoScrollCountDownTimer() {
            super((long) 600000, (long) 4000);
        }

        @Override
        public void onFinish() {
            //logic
            int current = viewPager.getCurrentItem();
            if(current==2){
                current = -1;
            }
            viewPager.setCurrentItem(current+1,true);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            int current = viewPager.getCurrentItem();
            if(current==2){
                current = -1;
            }
            viewPager.setCurrentItem(current+1,true);

        }
    }
}
