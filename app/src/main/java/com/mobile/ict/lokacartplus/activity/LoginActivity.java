package com.mobile.ict.lokacartplus.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.utils.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.interfaces.SmsListener;
import com.mobile.ict.lokacartplus.utils.SmsReceiver;
import com.mobile.ict.lokacartplus.utils.Validation;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class LoginActivity extends AppCompatActivity {

    private EditText eEnterPhoneNumber;
    private EditText eEnterPassword, etEnterPhoneNumberDialog, etOTP, etEnterPhoneNumberTimeupOTP;
    private Button buttonGenerateOtp;
    private Button buttonRegenerateOtp;
    private Dialog forgetPasswordDialogView, verifyingOTPDialogView, timeupOTPDialogView;
    private DBHelper dbHelper;
    private String otpJSON = "", otpSMS = "";
    private LinearLayout dotsLayout;
    private AutoScrollCountDownTimer autoScrollCountDownTimer;
    private TextInputLayout enterPasswordEditTextLayout;
    private NetworkCommunicator networkCommunicator;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mContext = this;
        eEnterPhoneNumber = findViewById(R.id.enterPhoneNumberEditText);
        eEnterPassword = findViewById(R.id.enterPasswordEditText);
        enterPasswordEditTextLayout = findViewById(R.id.enterPasswordEditTextLayout);
        networkCommunicator = NetworkCommunicator.getInstance();

        dbHelper = DBHelper.getInstance(this);

        Button bLogIn = findViewById(R.id.loginbutton);

            bLogIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(validation()){
                        if(Master.isNetworkAvailable(getApplicationContext())){
                            Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
                            makeLoginReq();
                        }else {
                            Toast.makeText(mContext, getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                        }
                    }

                }
            });


        findViewById(R.id.tvRegisterNow).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
            }
        });



        findViewById(R.id.forgetpassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Here, thisActivity is the current activity
                if (ContextCompat.checkSelfPermission(LoginActivity.this,
                        Manifest.permission.READ_SMS)
                        != PackageManager.PERMISSION_GRANTED) {

                    // Should we show an explanation?
                    //noinspection StatementWithEmptyBody
                    if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this,
                            Manifest.permission.READ_SMS)) {

                        // Show an explanation to the user *asynchronously* -- don't block
                        // this thread waiting for the user's response! After the user
                        // sees the explanation, try again to request the permission.

                    } else {

                        // No explanation needed, we can request the permission.

                        ActivityCompat.requestPermissions(LoginActivity.this,
                                new String[]{Manifest.permission.READ_SMS},
                                Master.MY_PERMISSIONS_REQUEST_READ_SMS);

                        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                        // app-defined int constant. The callback method gets the
                        // result of the request.
                    }
                } else {

                    SmsReceiver.bindListener(new SmsListener() {
                        @Override
                        public void messageReceived(String messageText) {
                            otpSMS = messageText;
                            etOTP.setText(otpSMS);

                            if(etOTP.getText().toString().trim().equals(otpJSON)){
                                autoScrollCountDownTimer.cancel();
                                verifyingOTPDialogView.dismiss();
                                Snackbar.make(findViewById(R.id.forgetpassword), "OTP Verified. " , Snackbar.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), ResetPasswordActivity.class);
                                i.putExtra("Phonenumber",etEnterPhoneNumberDialog.getText().toString().trim());
                                startActivity(i);
                            }
                        }
                    });


                    if(!otpSMS.equals("")) {
                        etOTP.setText(otpSMS);
                    }
                }

                openForgetPasswordDialog();

            }
        });

    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case Master.MY_PERMISSIONS_REQUEST_READ_SMS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    SmsReceiver.bindListener(new SmsListener() {
                        @Override
                        public void messageReceived(String messageText) {
                            otpSMS = messageText;
                            etOTP.setText(otpSMS);

                            if(etOTP.getText().toString().trim().equals(otpJSON)){
                                autoScrollCountDownTimer.cancel();
                                verifyingOTPDialogView.dismiss();
                                Snackbar.make(findViewById(R.id.forgetpassword), "OTP Verified. " , Snackbar.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), ResetPasswordActivity.class);
                                i.putExtra("Phonenumber",etEnterPhoneNumberDialog.getText().toString().trim());
                                startActivity(i);
                            }
                        }
                    });

                    if(!otpSMS.equals("")) {
                        etOTP.setText(otpSMS);
                    }

                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }



    private void openForgetPasswordDialog(){

        forgetPasswordDialogView = new Dialog(LoginActivity.this);
        forgetPasswordDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        forgetPasswordDialogView.setContentView(R.layout.dialog_forget_password);
        forgetPasswordDialogView.setCancelable(true);
        forgetPasswordDialogView.setCanceledOnTouchOutside(false);

        forgetPasswordDialogView.show();

        etEnterPhoneNumberDialog = forgetPasswordDialogView.findViewById(R.id.etEnterPhoneNumberDialog);

        buttonGenerateOtp = forgetPasswordDialogView.findViewById(R.id.buttonGenerateOtp);

        buttonGenerateOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Validation.isValidPhoneNumber(etEnterPhoneNumberDialog.getText().toString().trim())){
                    forgetPasswordDialogView.dismiss();

                    buttonGenerateOtp.setEnabled(false);
                    if(Master.isNetworkAvailable(getApplicationContext())){
                        Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
                        generateOtpFunction(etEnterPhoneNumberDialog.getText().toString().trim());
                    }else {
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Please enter the correct phone number.", Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private void generateOtpFunction(final String phonenumber){

        JSONObject generateOtpObject = new JSONObject();
        try {
            generateOtpObject.put("phonenumber", phonenumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        networkCommunicator.data(Master.getGenerateOTPAPI(),
                Request.Method.POST,
                generateOtpObject,
                false,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null) {
                            try {
                                otpJSON = obj.getString("otp");

                                Snackbar.make(findViewById(R.id.forgetpassword), "Sent OTP to your device. ", Snackbar.LENGTH_LONG).show();
                                openVerifyingOTPDialog(phonenumber);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        Toast.makeText(getApplicationContext(),getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
                        buttonRegenerateOtp.setEnabled(true);
                        buttonGenerateOtp.setEnabled(true);
                    }
                },Master.LOGIN_ACTIVITY_TAG, getApplicationContext());

    }


    private void openVerifyingOTPDialog(final String phonenumber){

        verifyingOTPDialogView = new Dialog(LoginActivity.this);
        verifyingOTPDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        verifyingOTPDialogView.setContentView(R.layout.dialog_verifyingotp);
        verifyingOTPDialogView.setCancelable(true);
        verifyingOTPDialogView.setCanceledOnTouchOutside(false);

        verifyingOTPDialogView.show();

        dotsLayout = verifyingOTPDialogView.findViewById(R.id.layoutDots);

        //auto scroll
        autoScrollCountDownTimer = new AutoScrollCountDownTimer();
        autoScrollCountDownTimer.start();

        etOTP = verifyingOTPDialogView.findViewById(R.id.etOTP);

        if(!otpSMS.equals("")) {
            etOTP.setText(otpSMS);
        }

        Button verifyOTP = verifyingOTPDialogView.findViewById(R.id.verifyOTP);
        verifyOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!etOTP.getText().toString().trim().equals("")){

                    if(etOTP.getText().toString().trim().equals(otpJSON)){
                        autoScrollCountDownTimer.cancel();
                        verifyingOTPDialogView.dismiss();
                        Snackbar.make(findViewById(R.id.forgetpassword), "OTP Verified. " , Snackbar.LENGTH_LONG).show();
                        Intent i = new Intent(getApplicationContext(), ResetPasswordActivity.class);
                        i.putExtra("Phonenumber",phonenumber);
                        startActivity(i);
                    } else {
                        Toast.makeText(getApplicationContext(), "OTP mismatch, please enter it again. ",
                                Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

    private void openTimeupOTPDialog(){

        timeupOTPDialogView = new Dialog(LoginActivity.this);
        timeupOTPDialogView.requestWindowFeature(Window.FEATURE_NO_TITLE);

        timeupOTPDialogView.setContentView(R.layout.dialog_timeup_otp);
        timeupOTPDialogView.setCancelable(true);
        timeupOTPDialogView.setCanceledOnTouchOutside(false);

        try {
            timeupOTPDialogView.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        etEnterPhoneNumberTimeupOTP = timeupOTPDialogView.findViewById(R.id.etEnterPhoneNumberTimeupOTP);

        buttonRegenerateOtp = timeupOTPDialogView.findViewById(R.id.buttonRegenerateOtp);
        buttonRegenerateOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Validation.isValidPhoneNumber(etEnterPhoneNumberTimeupOTP.getText().toString().trim())){
                    timeupOTPDialogView.dismiss();

                    buttonRegenerateOtp.setEnabled(false);
                    if(Master.isNetworkAvailable(getApplicationContext())){
                        Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
                        generateOtpFunction(etEnterPhoneNumberTimeupOTP.getText().toString().trim());
                    }else {
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }

    @Override
    protected void onStop() {
        super.onStop();

        // Stopping broadcast listener
        ComponentName receiver = new ComponentName(this, SmsReceiver.class);
        PackageManager pm = this.getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(dbHelper==null) dbHelper= DBHelper.getInstance(this);

        if(networkCommunicator==null) {
            networkCommunicator = NetworkCommunicator.getInstance();
        }
        // Starting broadcast listener for auto-verify otp
        ComponentName receiver = new ComponentName(this, SmsReceiver.class);
        PackageManager pm = this.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

    }

    private void makeLoginReq() {
        Map<String, String> params = new HashMap<>();
        params.put(Master.PASSWORD, eEnterPassword.getText().toString().trim());
        params.put(Master.MOBILENUMBER, eEnterPhoneNumber.getText().toString().trim());

        networkCommunicator.data(Master.getLoginAPI(),
                Request.Method.POST,
                new JSONObject(params),
                false,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null)
                            checkStatus(obj);
                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        Toast.makeText(getApplicationContext(),getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
                    }
                },Master.LOGIN_ACTIVITY_TAG, getApplicationContext());

    }

    private void checkStatus(JSONObject response){

        try {
            if(response.getString("status").equals("success")){

                // if user had registered email id then it wont throw exception, else it will!
                try {
                    new MemberDetails(response.getString("firstname"), response.getString("lastname"), response.getString("address"), response.getString("pincode"), response.getString("email"), response.getString("phone"));
                    Master.cartList = dbHelper.getCartDetails(MemberDetails.getMobileNumber());
                } catch (JSONException e) {
                    e.printStackTrace();
                    new MemberDetails(response.getString("firstname"), response.getString("lastname"), response.getString("address"), response.getString("pincode"), response.getString("phone"));
                }

                Glide.get(getApplicationContext()).clearMemory(); //clears memory of stepper activity images.

                SharedPreferenceConnector.writeBoolean(getApplicationContext(), Master.STEPPER, false);

                if(Master.REDIRECTED_FROM_CART){

                    dbHelper.changeMobileNumberCart("guest", response.getString("phone"));  //change mobile number in local db

                    dbHelper.addProfile();

                    Master.REDIRECTED_FROM_CART = false;

                    Intent i = new Intent(getApplicationContext(), CartActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();

                } else if(Master.REDIRECTED_FROM_FEEDBACK){

                    dbHelper.changeMobileNumberCart("guest", response.getString("phone"));  //change mobile number in local db

                    dbHelper.addProfile();

                    Master.REDIRECTED_FROM_FEEDBACK = false;

                    super.onBackPressed();
                    /*Intent i = new Intent(getApplicationContext(), CartActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();*/

                } else {

                    dbHelper.addProfile();

                    Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
            }
            else {
                Toast.makeText(getApplicationContext(), response.getString("error"),
                        Toast.LENGTH_LONG).show();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



    private boolean validation(){

        if(!Validation.isValidPhoneNumber(eEnterPhoneNumber.getText().toString().trim())){
            eEnterPhoneNumber.setError("Please enter valid Phone Number");
            return false;
        }
        if(!Validation.isValidPassword(eEnterPassword.getText().toString().trim())){
            //eEnterPassword.setError("Please enter valid password");
            //enterPasswordEditTextLayout.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
            enterPasswordEditTextLayout.setBackgroundResource(R.drawable.tv_border_red);
            //enterPasswordEditTextLayout.setErrorEnabled(true);
            //enterPasswordEditTextLayout.setError("Please enter valid password");

            Toast.makeText(getApplication(), "Please enter valid password", Toast.LENGTH_LONG).show();

            return false;
        }

        enterPasswordEditTextLayout.setBackgroundResource(R.color.etBackground);
        return true;
    }


    public class AutoScrollCountDownTimer extends CountDownTimer {

        public AutoScrollCountDownTimer() {
            super((long) 300000, (long) 600);
        }

        int count = 0;

        @Override
        public void onFinish() {
            //logic
            verifyingOTPDialogView.dismiss();
            openTimeupOTPDialog();
        }

        @Override
        public void onTick(long millisUntilFinished) {
            count++;

            TextView[] dots = new TextView[3];

            int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
            int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

            dotsLayout.removeAllViews();

            for (int i = 0; i < dots.length; i++) {
                dots[i] = new TextView(getApplicationContext());
                dots[i].setText(Html.fromHtml("&#8226;"));
                dots[i].setTextSize(35);
                dots[i].setTextColor(colorsInactive[count%3]);
                dotsLayout.addView(dots[i]);
            }

            if (dots.length > 0)
                dots[count%3].setTextColor(colorsActive[count%3]);

        }
    }

}
