package com.mobile.ict.lokacartplus.model;

import com.google.gson.annotations.SerializedName;

import java.text.DecimalFormat;

/**
 * Created by Toshiba on 11-04-2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class OrdersDetail {

    @SerializedName("isPaid")
    private boolean isPaid;
    @SerializedName("delivery_date")
    private String delivery_date;
    @SerializedName("quantity")
    private int quantity;
    @SerializedName("rate")
    private double rate;
    @SerializedName("orderItemId")
    private String orderItemId;
    @SerializedName("imageUrl")
    private String imageUrl;
    @SerializedName("name")
    private String name;
    @SerializedName("status")
    private String status;
    @SerializedName("organization name")
    private String organization;
    @SerializedName("unit")
    private String unit;

    public boolean isPaid() {
        return isPaid;
    }

    public String getDelivery_date() {

        if(delivery_date.equals("") || delivery_date.toLowerCase().equals("not applicable"))
            return "NA";
        else
            return delivery_date.substring(0,10);
    }

    public int getQuantity() {
        return quantity;
    }

    public double getRate() {
        DecimalFormat df2 = new DecimalFormat("#.00");
        return Double.valueOf(df2.format(rate));
        //return rate;
    }

    public String getOrderItemId() {
        return orderItemId;
    }

    public String getName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrganization() {
        return organization;
    }

}
