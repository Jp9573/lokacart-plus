package com.mobile.ict.lokacartplus.interfaces;

/**
 * Created by jay on 17/1/18.
 */

public interface NoInternetConnectionListener {
    void showNoInternetConnectionLayout();
}
