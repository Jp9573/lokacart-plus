package com.mobile.ict.lokacartplus.model;

/**
 * Created by Toshiba on 24/3/17.
 */
@SuppressWarnings("DefaultFileTemplate")
public class MemberDetails {

    private static String fname;
    private static String lname;
    private static String password;
    private static String address;
    private static String pincode;
    private static String email;
    private static String mobileNumber;

    public static void setMemberDetails() {
        fname = lname = password = address = pincode = email = mobileNumber = "guest";
    }

    public MemberDetails(String fname, String lname,
                        String address,
                        String pincode,
                        String email,
                        String mobileNumber){

        MemberDetails.fname = fname;
        MemberDetails.lname = lname;
        MemberDetails.address = address;
        MemberDetails.pincode = pincode;
        MemberDetails.email = email;
        MemberDetails.mobileNumber = mobileNumber;

    }

    public MemberDetails(String fname, String lname,
                         String address,
                         String pincode,
                         String mobileNumber){

        MemberDetails.fname = fname;
        MemberDetails.lname = lname;
        MemberDetails.address = address;
        MemberDetails.pincode = pincode;
        MemberDetails.email = "";
        MemberDetails.mobileNumber = mobileNumber;

    }

    public static String getFname() {
        return fname;
    }

    public static void setFname(String fname) {
        MemberDetails.fname = fname;
    }

    public static String getLname() {
        return lname;
    }

    public static void setLname(String lname) {
        MemberDetails.lname = lname;
    }

    public static String getAddress() {
        return address;
    }

    public static void setAddress(String address) {
        MemberDetails.address = address;
    }

    public static String getPincode() {
        return pincode;
    }

    public static void setPincode(String pincode) {
        MemberDetails.pincode = pincode;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        MemberDetails.email = email;
    }

    public static String getMobileNumber() {
        return mobileNumber;
    }

    public static void setMobileNumber(String mobileNumber) {
        MemberDetails.mobileNumber = mobileNumber;
    }
}
