package com.mobile.ict.lokacartplus.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.mobile.ict.lokacartplus.interfaces.NoInternetConnectionListener;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.utils.RecyclerItemClickListener;
import com.mobile.ict.lokacartplus.adapter.HomepageAdapter;


public class HomePageFragment extends Fragment {

    private View rootView;

    public HomePageFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home_page, container, false);

        //code in onActivityCreated was here...

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        RecyclerView homepageRecyclerView = rootView.findViewById(R.id.homepage_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        homepageRecyclerView.setNestedScrollingEnabled(false);
        homepageRecyclerView.setHasFixedSize(false);

        RecyclerView.LayoutManager homepageLayoutManager = new LinearLayoutManager(getActivity());
        homepageRecyclerView.setLayoutManager(homepageLayoutManager);

        int[] images = new int[8];

        images[0] = R.drawable.homes_screen_0;
        images[1] = R.drawable.homes_screen_1;
        images[2] = R.drawable.homes_screen_2;
        images[3] = R.drawable.homes_screen_3;
        images[4] = R.drawable.homes_screen_4;
        images[5] = R.drawable.homes_screen_5;
        images[6] = R.drawable.homes_screen_6;
        images[7] = R.drawable.homes_screen_7;

        final String[] productTypeNames = new String[8];

        productTypeNames[0] = "Leafy Vegetables";
        productTypeNames[1] = "Fruits";
        productTypeNames[2] = "Vegetables";
        productTypeNames[3] = "Dry Fruits";
        productTypeNames[4] = "Spices";
        productTypeNames[5] = "Dairy Products";
        productTypeNames[6] = "Grains";
        productTypeNames[7] = "Exotic Vegetables";

        RecyclerView.Adapter homepageAdapter = new HomepageAdapter(getContext(), images);
        homepageRecyclerView.setAdapter(homepageAdapter);


        homepageRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), homepageRecyclerView,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(int position) {
                        // do whatever

                        if(Master.isNetworkAvailable(getContext())){

                            Bundle b = new Bundle();
                            b.putString("typename", productTypeNames[position]);

                            ProductFragment productFragment = new ProductFragment();

                            productFragment.setArguments(b);

                            getActivity().getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.fragment_container, productFragment, Master.PRODUCT_FRAGMENT_TAG)
                                    .commit();

                            Master.navItemIndex = Master.NAV_PRODUCT_INDEX;
                            Master.CURRENT_TAG = Master.PRODUCT_FRAGMENT_TAG;

                        }else {
                            // toast of no internet connection
                            Toast.makeText(getContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                            NoInternetConnectionListener listener = (NoInternetConnectionListener) getContext();
                            listener.showNoInternetConnectionLayout();
                        }

                    }

                    /*@Override public void onLongItemClick(View view, int position) {
                    }*/
                })
        );
    }

    @Override
    public void onStart() {
        super.onStart();
        Master.isFromCart = false;
    }

}
