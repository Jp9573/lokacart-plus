package com.mobile.ict.lokacartplus.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.utils.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.utils.Validation;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPasswordActivity extends AppCompatActivity {

    private EditText etEnterPassword, etConfirmPassword;
    private DBHelper dbHelper;
    private TextInputLayout confirmPasswordEditTextLayout, enterPasswordEditTextLayout;
    private NetworkCommunicator networkCommunicator;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        dbHelper = DBHelper.getInstance(this);
        mContext = this;

        final String phonenumber = getIntent().getExtras().getString("Phonenumber");

        enterPasswordEditTextLayout = findViewById(R.id.enterPasswordEditTextLayout);
        confirmPasswordEditTextLayout = findViewById(R.id.confirmPasswordEditTextLayout);

        networkCommunicator = NetworkCommunicator.getInstance(/*getApplicationContext()*/);

        etEnterPassword = findViewById(R.id.etEnterPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        Button confirmButton = findViewById(R.id.confirmButton);
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validation()){
                    if(Master.isNetworkAvailable(getApplicationContext())){
                        Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
                        makeJsonObjReq(phonenumber);
                    }else {
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(networkCommunicator==null) {
            networkCommunicator = NetworkCommunicator.getInstance();
        }
    }

    private boolean validation(){

        if(!Validation.isValidPassword(etEnterPassword.getText().toString().trim())){
            enterPasswordEditTextLayout.requestFocus();
            enterPasswordEditTextLayout.setBackgroundResource(R.drawable.tv_border_red);
            Toast.makeText(getApplication(), "Please enter valid password", Toast.LENGTH_LONG).show();
            return false;
        }else {
            enterPasswordEditTextLayout.requestFocus();
            enterPasswordEditTextLayout.setBackgroundResource(R.color.etBackground);
        }

        if(!Validation.isPasswordMatching(etEnterPassword.getText().toString().trim(), etConfirmPassword.getText().toString().trim())){
            confirmPasswordEditTextLayout.requestFocus();
            confirmPasswordEditTextLayout.setBackgroundResource(R.drawable.tv_border_red);
            Toast.makeText(getApplication(), "Password didn't match", Toast.LENGTH_LONG).show();
            return false;
        } else{
            confirmPasswordEditTextLayout.requestFocus();
            confirmPasswordEditTextLayout.setBackgroundResource(R.color.etBackground);
        }

        return true;

    }

    private void makeJsonObjReq(final String phonenumber) {
        Map<String, String> params = new HashMap<>();
        params.put(Master.PASSWORD, etEnterPassword.getText().toString().trim());
        params.put("phonenumber", phonenumber);

        networkCommunicator.data(Master.getChangePasswordAPI(),
                Request.Method.POST,
                new JSONObject(params),
                false,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;

                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null)
                            checkStatus(obj);
                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        Toast.makeText(getApplicationContext(),getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
                    }
                },Master.RESET_PASSWORD_ACTIVITY_TAG, getApplicationContext());

    }

    private void checkStatus(JSONObject response){

        try {
            if(response.getString("status").equals("success")){

                Toast.makeText(getApplicationContext(), "Password changed Successfully! ", Toast.LENGTH_LONG).show();

                try {
                    new MemberDetails(response.getString("firstname"), response.getString("lastname"), response.getString("address"), response.getString("pincode"), response.getString("email"), response.getString("phone"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    new MemberDetails(response.getString("firstname"), response.getString("lastname"), response.getString("address"), response.getString("pincode"), response.getString("phone"));
                }

                Glide.get(getApplicationContext()).clearMemory(); //clears memory of stepper activity images.

                SharedPreferenceConnector.writeBoolean(getApplicationContext(), Master.STEPPER, false);

                if(Master.REDIRECTED_FROM_CART){

                    dbHelper.changeMobileNumberCart("guest",response.getString("phone"));  //change mobile number in local db
                    dbHelper.addProfile();

                    Master.REDIRECTED_FROM_CART = false;

                    Intent i = new Intent(getApplicationContext(), CartActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();

                } else if(Master.REDIRECTED_FROM_FEEDBACK){

                    dbHelper.changeMobileNumberCart("guest",response.getString("phone"));  //change mobile number in local db
                    dbHelper.addProfile();

                    Master.REDIRECTED_FROM_FEEDBACK = false;

                    Intent i = new Intent(getApplicationContext(), FeedbackActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();

                } else {

                    dbHelper.addProfile();

                    Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
            } else {
                Toast.makeText(getApplicationContext(), response.getString("error"),
                        Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
