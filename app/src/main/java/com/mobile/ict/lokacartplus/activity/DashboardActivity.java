package com.mobile.ict.lokacartplus.activity;

import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobile.ict.lokacartplus.interfaces.NoInternetConnectionListener;
import com.mobile.ict.lokacartplus.model.Product;
import com.mobile.ict.lokacartplus.model.ProductDetail;
import com.mobile.ict.lokacartplus.model.ProductTypeDetail;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.utils.RecyclerItemClickListener;
import com.mobile.ict.lokacartplus.utils.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.adapter.ProductTypeAdapter;
import com.mobile.ict.lokacartplus.adapter.SearchBarAdapter;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.fragment.AboutUsFragment;
import com.mobile.ict.lokacartplus.fragment.ContactUsFragment;
import com.mobile.ict.lokacartplus.fragment.EditProfileFragment;
import com.mobile.ict.lokacartplus.fragment.HomePageFragment;
import com.mobile.ict.lokacartplus.fragment.ProductFragment;
import com.mobile.ict.lokacartplus.fragment.ProfileFragment;
import com.mobile.ict.lokacartplus.fragment.SearchFragment;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DashboardActivity extends AppCompatActivity implements
        ProfileFragment.OnProfileFragmentInteractionListener,
        EditProfileFragment.OnEditProfileFragmentInteractionListener,
        ContactUsFragment.AccessNavigationView,
        NoInternetConnectionListener {

    private DBHelper dbHelper;

    private int searchBarSuggestionFlag = 0, searchItemClickListnerFlag = 0;
    private static ArrayList<String> products;
    private String query_entered = null;
    private RecyclerView listView;

    private static final int TIME_INTERVAL = 1000; // # milliseconds, desired time passed between two back presses.
    private long mBackPressed = 0;

    private NavigationView navigationView;
    private DrawerLayout drawer;
    private RelativeLayout rlNoInternetConnection;
    private RelativeLayout rlProductType;
    private NestedScrollView contentScrolling;

    private ImageView imgNavHeaderBg;
    private ImageButton ivEditProfile;
    private TextView tvNavHeadName, tvNavHeadPhoneNumber, tvNavHeadEmail;
    private Toolbar toolbar;
    private Button button_footer_item_Log_Out;

    private String token = "";

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;
    private String[] cartProductIds = null;

    private Handler mHandler;

    private NetworkCommunicator networkCommunicator;
    private Context mContext;
    private int size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // To reduce overdraw
        getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.BlueBackground)));

        //init array lists
        Master.productDetailList = new ArrayList<>();
        Master.cartList = new ArrayList<>();
        Master.product_type = new ArrayList<>();
        Master.product_type.clear();

        DBHelper dbHelper = DBHelper.getInstance(getApplicationContext());
        dbHelper.getProfile();

        mContext = this;

        networkCommunicator = NetworkCommunicator.getInstance();

        drawer = findViewById(R.id.drawer_layout);

        mHandler = new Handler();

        rlNoInternetConnection = findViewById(R.id.rlNoInternetConnection);
        rlProductType = findViewById(R.id.rl);
        contentScrolling = findViewById(R.id.content_scrolling);
        ImageView ivRetry = findViewById(R.id.ivRetry);

        ivRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Master.isNetworkAvailable(getApplicationContext())){
                    try {
                        if (checkPlayServices()) {
                            token = FirebaseInstanceId.getInstance().getToken();
                            Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
                            registerToken(token);
                            FirebaseMessaging.getInstance().subscribeToTopic("loka-plus");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                    rlNoInternetConnection.setVisibility(View.GONE);
                    rlProductType.setVisibility(View.VISIBLE);
                    contentScrolling.setVisibility(View.VISIBLE);
                }else {
                    // toast of no internet connection
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                    rlNoInternetConnection.setVisibility(View.VISIBLE);
                    rlProductType.setVisibility(View.GONE);
                    contentScrolling.setVisibility(View.GONE);
                }

            }
        });

        navigationView = findViewById(R.id.nav_view);

        // Navigation view header
        View navHeader = navigationView.getHeaderView(0);
        tvNavHeadName = navHeader.findViewById(R.id.tvNavHeadName);
        tvNavHeadName.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        tvNavHeadPhoneNumber = navHeader.findViewById(R.id.tvNavHeadPhoneNumber);
        tvNavHeadPhoneNumber.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        imgNavHeaderBg = navHeader.findViewById(R.id.img_header_bg);

        tvNavHeadEmail = navHeader.findViewById(R.id.tvNavHeadEmail);
        tvNavHeadEmail.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        ivEditProfile = navHeader.findViewById(R.id.ivEditProfile);


        Button button_footer_item_Share = navigationView.findViewById(R.id.footer_item_Share);
        button_footer_item_Share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setType("text/plain");
                    i.putExtra(Intent.EXTRA_SUBJECT, "Lokacart Plus");
                    String intentExtraText = "Let me recommend you this application\n";
                    intentExtraText = intentExtraText + "https://play.google.com/store/apps/details?id=" + appPackageName;
                    i.putExtra(Intent.EXTRA_TEXT, intentExtraText);
                    startActivity(Intent.createChooser(i, "Share via"));
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        });

        button_footer_item_Log_Out = navigationView.findViewById(R.id.footer_item_Log_Out);

        if(MemberDetails.getMobileNumber() != null) {
            if (MemberDetails.getMobileNumber().equals(String.valueOf("guest"))) {
                button_footer_item_Log_Out.setText(getString(R.string.login));
            } else {
                button_footer_item_Log_Out.setText(getString(R.string.log_out));
            }
        }
        button_footer_item_Log_Out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MemberDetails.getMobileNumber() != null) {
                    if (!MemberDetails.getMobileNumber().equals(String.valueOf("guest"))) {
                        if (Master.isNetworkAvailable(getApplicationContext())) {
                            drawer.closeDrawers();
                            LogoutDialog();
                        } else {
                            // toast of no internet connection
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                            rlNoInternetConnection.setVisibility(View.VISIBLE);
                            rlProductType.setVisibility(View.GONE);
                            contentScrolling.setVisibility(View.GONE);
                        }

                    } else {
                        drawer.closeDrawers();
                        Intent i = new Intent(getApplication(), LoginActivity.class);
                        startActivity(i);
                    }
                }

            }
        });

        // load toolbar titles from string resources
        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        //DBHelper dbHelper = DBHelper.getInstance(getApplicationContext());
        cartProductIds = dbHelper.getCartProductIds(MemberDetails.getMobileNumber());

        // load nav menu header data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        RecyclerView mRecyclerView = findViewById(R.id.horizontal_recycler_view);

        // use this setting to improve perfoalrmance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(
                getApplicationContext(),
                LinearLayoutManager.HORIZONTAL,
                false
        );
        mRecyclerView.setLayoutManager(mLayoutManager);

        // productTypeAPIcall - May be useful in future.
        /*if(Master.isNetworkAvailable(getApplicationContext())){
            Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
            makeProductTypeRequest();
        }else {
            // toast of no internet connection
            Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
            rlNoInternetConnection.setVisibility(View.VISIBLE);
            findViewById(R.id.rl).setVisibility(View.GONE);
            findViewById(R.id.content_scrolling).setVisibility(View.GONE);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }*/

        int[] images = new int[8];

        images[0] = R.drawable.sampleimage0;
        images[1] = R.drawable.sampleimage1;
        images[2] = R.drawable.sampleimage2;
        images[3] = R.drawable.sampleimage3;
        images[4] = R.drawable.sampleimage4;
        images[5] = R.drawable.sampleimage5;
        images[6] = R.drawable.sampleimage6;
        images[7] = R.drawable.sampleimage7;

        String[] product_name = new String[8];

        product_name[0]= "Fruits";
        product_name[1]= "Vegetables";
        product_name[2]= "Grains";
        product_name[3]= "Spices";
        product_name[4]= "Dairy Products";
        product_name[5]= "Leafy Vegetables";
        product_name[6]= "Exotic Vegetables";
        product_name[7]= "Dry Fruits";

        if(Master.product_type != null) {
            Master.product_type.clear();
        }else {
            Master.product_type = new ArrayList<>();
        }

        for (String product : product_name) {
            Master.product_type.add(new ProductTypeDetail(product));
        }

        RecyclerView.Adapter productTypeAdapter = new ProductTypeAdapter(getApplicationContext(), images);
        mRecyclerView.setAdapter(productTypeAdapter);

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplication(), mRecyclerView,new RecyclerItemClickListener.OnItemClickListener() {

                    @Override public void onItemClick(int position) {

                        if(Master.isNetworkAvailable(getApplicationContext())){

                            ProductFragment productFragment = (ProductFragment) getSupportFragmentManager().findFragmentByTag(Master.PRODUCT_FRAGMENT_TAG);
                            if(productFragment == null) {
                                Bundle b = new Bundle();
                                b.putString("typename", Master.product_type.get(position).getName());
                                productFragment = new ProductFragment();
                                productFragment.setArguments(b);
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.fragment_container, productFragment, Master.PRODUCT_FRAGMENT_TAG)
                                        .commit();
                            }else {
                                productFragment.onDataChange(Master.product_type.get(position).getName());
                            }

                            Master.navItemIndex = Master.NAV_PRODUCT_INDEX;
                            Master.CURRENT_TAG = Master.PRODUCT_FRAGMENT_TAG;
                        }else {
                            // toast of no internet connection
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                            rlNoInternetConnection.setVisibility(View.VISIBLE);
                            rlProductType.setVisibility(View.GONE);
                            contentScrolling.setVisibility(View.GONE);
                            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        }

                    }

                })
        );

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            Master.navItemIndex = Master.NAV_HOME_INDEX;
            Master.CURRENT_TAG = Master.HOME_TAG;

            // Create a new Fragment to be placed in the activity layout
            HomePageFragment homePageFragment = new HomePageFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            //homePageFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, homePageFragment).commit();

        }

        if(MemberDetails.getMobileNumber() != null) {
            if (!MemberDetails.getMobileNumber().equals("guest")) {

                if (Master.isNetworkAvailable(getApplicationContext())) {
                    try {
                        // Get token
                        if (checkPlayServices()) {
                            token = FirebaseInstanceId.getInstance().getToken();

                            Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
                            registerToken(token);
                            FirebaseMessaging.getInstance().subscribeToTopic("loka-plus");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    // toast of no internet connection
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                    showNoInternetConnectionLayout();
                }

            }
        }

    }

    @Override
    public void showNoInternetConnectionLayout(){
        rlNoInternetConnection.setVisibility(View.VISIBLE);
        rlProductType.setVisibility(View.GONE);
        contentScrolling.setVisibility(View.GONE);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    private void registerToken(String token) {

        Map<String, String> params = new HashMap<>();
        params.put(Master.TOKEN, token);
        params.put("number", MemberDetails.getMobileNumber());

        networkCommunicator.data(Master.getRegisterTokenAPI(),
                Request.Method.POST,
                new JSONObject(params),
                true,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null) {
                            try {
                                if (obj.getString("response").equals("success")) {
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        Toast.makeText(getApplicationContext(),getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
                    }
                },Master.SCROLLING_ACTIVITY_TAG, getApplicationContext());
    }

    private void deregisterToken() {

        Map<String, String> params = new HashMap<>();
        params.put("number", MemberDetails.getMobileNumber());

        networkCommunicator.data(Master.getDeregisterTokenAPI(),
                Request.Method.POST,
                new JSONObject(params),
                true,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null) {

                            try {
                                if(obj.getString("response").equals("success")){

                                    dbHelper.deleteProfile();
                                    //dbHelper.deleteCart(MemberDetails.getMobileNumber());
                                    //setting guest values of member
                                    MemberDetails.setMemberDetails();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        Toast.makeText(getApplicationContext(),getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
                    }
                },Master.SCROLLING_ACTIVITY_TAG, getApplicationContext());

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, 1)
                        .show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

    public void changeToolBar(String productType)
    {
        setSupportActionBar(toolbar);
        toolbar.setTitle(productType);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
    }


    @Override
    public boolean onSupportNavigateUp() {
        toolbar.setTitle("Lokacart Plus");
        setUpNavigationView();
        rlProductType.setVisibility(View.VISIBLE);

        if (Master.navItemIndex < Master.NAV_HOME_INDEX || Master.navItemIndex == Master.NAV_SEARCH_INDEX) {
            if (Master.navItemIndex < Master.NAV_HOME_INDEX) {
                navigationView.getMenu().getItem(Master.navItemIndex).setChecked(false);
            } else if (Master.navItemIndex == Master.NAV_SEARCH_INDEX) {
                rlProductType.setVisibility(View.VISIBLE);
            }
            Master.navItemIndex = Master.NAV_HOME_INDEX;
            Master.CURRENT_TAG = Master.HOME_TAG;
            loadHomeFragment();
        } else if (Master.navItemIndex == Master.NAV_EDIT_PROFILE_INDEX) {
            Master.navItemIndex = Master.NAV_PROFILE_INDEX;
            Master.CURRENT_TAG = Master.PROFILE_FRAGMENT_TAG;
            navigationView.getMenu().getItem(Master.navItemIndex).setChecked(true);
            loadHomeFragment();
        } else if(Master.navItemIndex == Master.NAV_HOME_INDEX){
            if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                System.exit(0);
            }
            mBackPressed = System.currentTimeMillis();
        }

        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
            return;
        }

        // This code loads home fragment when back key is pressed
        // when user is in other fragment than productDetails
        boolean shouldLoadHomeFragOnBackPress = true;
        if (shouldLoadHomeFragOnBackPress) {
            // checking if user is on other navigation menu
            // rather than productDetails

            if (Master.navItemIndex < Master.NAV_HOME_INDEX || Master.navItemIndex == Master.NAV_SEARCH_INDEX) {
                if (Master.navItemIndex < Master.NAV_HOME_INDEX) {
                    navigationView.getMenu().getItem(Master.navItemIndex).setChecked(false);
                } else if (Master.navItemIndex == Master.NAV_SEARCH_INDEX) {
                    rlProductType.setVisibility(View.VISIBLE);
                }
                Master.navItemIndex = Master.NAV_HOME_INDEX;
                Master.CURRENT_TAG = Master.HOME_TAG;
                toolbar.setNavigationIcon(null);
                setUpNavigationView();
                loadHomeFragment();
                return;
            } else if (Master.navItemIndex == Master.NAV_EDIT_PROFILE_INDEX) {
                Master.navItemIndex = Master.NAV_PROFILE_INDEX;
                Master.CURRENT_TAG = Master.PROFILE_FRAGMENT_TAG;
                navigationView.getMenu().getItem(Master.navItemIndex).setChecked(true);
                loadHomeFragment();
                return;
            } else if( Master.navItemIndex == Master.NAV_HOME_INDEX ){
                if (mBackPressed + TIME_INTERVAL > System.currentTimeMillis()) {
                    System.exit(0);
                }
                mBackPressed = System.currentTimeMillis();
            }

        }

        super.onBackPressed();
    }

    private void LogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));

        builder.setMessage(getResources().getString(R.string.alert_do_you_want_to_Logout));
        builder.setCancelable(true);
        builder.setPositiveButton(
                getString(R.string.dialog_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                        try {

                            Glide.get(getApplicationContext()).clearMemory(); //clears memory of images.

                            Master.productDetailList.clear();
                            Master.cartList.clear();
                            Master.product_type.clear();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        SharedPreferenceConnector.writeBoolean(getApplicationContext(), Master.STEPPER, true);

                        if(Master.isNetworkAvailable(getApplicationContext())){
                            Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
                            deregisterToken();
                            try {
                                FirebaseInstanceId.getInstance().deleteInstanceId();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            FirebaseMessaging.getInstance().unsubscribeFromTopic("loka-plus");
                        }else {
                            // toast of no internet connection
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                            rlNoInternetConnection.setVisibility(View.VISIBLE);
                            rlProductType.setVisibility(View.GONE);
                            contentScrolling.setVisibility(View.GONE);

                        }

                        SharedPreferenceConnector.writeString(getApplicationContext(), "redirectedfrom", "Scrolling_Activity");
                        Intent i = new Intent(getApplication(),WelcomeActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                        DashboardActivity.this.finish();

                    }
                });

        builder.setNegativeButton(
                getString(R.string.dialog_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                }
        );
        AlertDialog alert11 = builder.create();
        alert11.show();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if(Master.isFromCart && Master.productTypeValue != null)
            onBackToProductFragment();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Master.isFromCart = true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(MemberDetails.getMobileNumber() != null) {
            if (MemberDetails.getMobileNumber().equals("guest")) {
                button_footer_item_Log_Out.setText(getString(R.string.login));
            } else {
                button_footer_item_Log_Out.setText(getString(R.string.log_out));
            }
        }

        if(dbHelper==null)dbHelper=DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();

        if(networkCommunicator==null) {
            networkCommunicator = NetworkCommunicator.getInstance();
        }

        Master.CART_ITEM_COUNT = dbHelper.getCartItemsCount(MemberDetails.getMobileNumber());

        loadNavHeader();

        invalidateOptionsMenu();

        try {

            if (SharedPreferenceConnector.readString(this, "redirectto", "").equals("view_orders")) {
                SharedPreferenceConnector.writeString(this, "redirectto", "");
                Master.navItemIndex = Master.NAV_ORDERS_INDEX;
                Master.CURRENT_TAG = Master.ORDERS_FRAGMENT_TAG;
                //loadHomeFragment();

                navigationView.getMenu().getItem(Master.navItemIndex).setChecked(false);
                startActivity(new Intent(getApplication(), OrdersActivity.class));
            } else if (SharedPreferenceConnector.readString(this, "redirectto", "").equals("homepage")) {
                SharedPreferenceConnector.writeString(this, "redirectto", "");
                toolbar.setNavigationIcon(null); // to remove whichever icon is there in action bar
                setUpNavigationView(); // to load nav drawer and hamburger icon in action bar
                if(Master.navItemIndex < Master.NAV_HOME_INDEX){
                    navigationView.getMenu().getItem(Master.navItemIndex).setChecked(false);
                }
                Master.navItemIndex = Master.NAV_HOME_INDEX;
                Master.CURRENT_TAG = Master.HOME_TAG;
                loadHomeFragment();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        if(Master.isFromCart && Master.productTypeValue != null)
            onBackToProductFragment();

    }

    private void makeSearchReq(final String text) {

        Map<String, String> params = new HashMap<>();
        params.put(Master.QUERY, text);
        params.put(Master.SORT_FLAG, "0");

        networkCommunicator.data(Master.getSearchAndSort(),
                Request.Method.POST,
                new JSONObject(params),
                true,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        products.clear(); // clears the previous list

                        if (Master.productDetailList != null)
                            Master.productDetailList.clear(); // clears the previous list

                        String response = (String)result;
                        parseJSONString(response, text);

                    }
        }, new NetworkResponse.ErrorListener()
        {
            @Override
            public void onError(NetworkException error) {
                Master.dismissProgressDialog();
                Toast.makeText(getApplicationContext(),getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
            }
        },Master.SCROLLING_ACTIVITY_TAG, getApplicationContext());
    }

    private void parseJSONString(String jsonString, String text){

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        Product productGSON = gson.fromJson(jsonString, Product.class);

        if(Master.productDetailList != null)
            Master.productDetailList.clear();
        Master.productDetailList = productGSON.productDetails;

        for(ProductDetail p: productGSON.productDetails) {
            String name = p.getName();
            products.add(name);

            String id = p.getId();
            if(isProductInCart(id) == 1)
            {
                DBHelper dbHelper = DBHelper.getInstance(getApplicationContext());
                p.setQuantity(dbHelper.getCartQuantity(MemberDetails.getMobileNumber(),id));
                updateCart(p);
            }else {
                p.setQuantity(0);
            }

        }

        if(searchBarSuggestionFlag==1){
            searchBarSuggestion(text);
            searchBarSuggestionFlag = 0;
        }
        if(searchItemClickListnerFlag==1){
            searchItemClickListner();
            searchItemClickListnerFlag = 0;
        }
    }

    private int isProductInCart(String id){
        for (String cartProductId : cartProductIds) {
            if (cartProductId.equals(id)) {
                return 1;
            }
        }
        return 0;
    }


    private void updateCart(ProductDetail productDetail)
    {
        size = Master.cartList.size();
        for (int z = 0; z < size; z++) {
            ProductDetail pD = Master.cartList.get(z);
            if (pD.getId().equals(productDetail.getId())) {

                pD.setId(productDetail.getId());
                pD.setImageUrl(productDetail.getImageUrl());
                pD.setGst(productDetail.getGst());
                pD.setMoq(productDetail.getMoq());
                pD.setName(productDetail.getName());
                pD.setQuantity(productDetail.getQuantity());
                pD.setOrganization(productDetail.getOrganization());
                pD.setUnit(productDetail.getUnit());
                pD.setUnitRate(productDetail.getUnitRate());

                pD.setItemTotal(pD.getUnitRate() * pD.getQuantity());

                dbHelper.updateProduct(
                        String.valueOf(pD.getUnitRate()),
                        String.valueOf(pD.getQuantity()),
                        String.valueOf(pD.getItemTotal()),
                        String.valueOf(pD.getName()),
                        MemberDetails.getMobileNumber(),
                        String.valueOf(pD.getOrganization()),
                        String.valueOf(pD.getId()),
                        String.valueOf(pD.getImageUrl()),
                        String.valueOf(pD.getUnit()),
                        String.valueOf(pD.getMoq()),
                        String.valueOf(pD.getGst())
                );
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        products = new ArrayList<>();
        listView = findViewById(R.id.search_list_view);
        listView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        listView.setLayoutManager(layoutManager);

        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        MenuItem item = menu.findItem(R.id.action_cart);

        LayerDrawable icon = (LayerDrawable) item.getIcon();
        Master.setBadgeCount(this, icon, Master.CART_ITEM_COUNT);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final MenuItem searchMenuItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchMenuItem.getActionView();

        //Oreo Supported!
        new MenuItem.OnActionExpandListener(){

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                rlProductType.setVisibility(View.GONE);

                if(findViewById(R.id.rlNoMatchSearchSuggestion) != null){
                    findViewById(R.id.rlNoMatchSearchSuggestion).setVisibility(View.GONE);
                }


                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                FrameLayout layout = findViewById(R.id.fragment_container);
                layout.setVisibility(View.VISIBLE);

                rlProductType.setVisibility(View.VISIBLE);

                FrameLayout layout2 = findViewById(R.id.search_framelayout);
                layout2.setVisibility(View.GONE);

                if(Master.navItemIndex != Master.NAV_SEARCH_INDEX) {

                    Master.navItemIndex = Master.NAV_HOME_INDEX;
                    Master.CURRENT_TAG = Master.HOME_TAG;
                    toolbar.setNavigationIcon(null);
                    setUpNavigationView();
                    loadHomeFragment();

                }

                return true;
            }
        };

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        //when you change text in query field
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }


            @Override
            public boolean onQueryTextChange(String newText) {

                query_entered = newText;

                if (TextUtils.isEmpty(newText) || newText.length()<0) {
                    listView.setAdapter(null);
                } else {

                    if(Master.isNetworkAvailable(getApplicationContext())){
                        //code to display filtered results based on query entered
                        searchBarSuggestionFlag = 1;
                        makeSearchReq(query_entered);
                    }else {
                        // toast of no internet connection
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                        rlNoInternetConnection.setVisibility(View.VISIBLE);
                        rlProductType.setVisibility(View.GONE);
                        contentScrolling.setVisibility(View.GONE);
                        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    }

                    //clear the list view before everytime new query entered
                    listView.setAdapter(null);

                }

                return true;
            }
        });

        //to collapse query field when not in use
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean queryTextFocused) {
                if (!queryTextFocused) {
                    searchMenuItem.collapseActionView();
                    searchView.setQuery("", false);

                    FrameLayout layout = findViewById(R.id.fragment_container);
                    layout.setVisibility(View.VISIBLE);
                    rlProductType.setVisibility(View.VISIBLE);
                }
            }
        });

        listView.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplication(), listView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(final int position) {

                        //to close search menu when clicked on back from product details page
                        searchMenuItem.collapseActionView();
                        searchView.setQuery("", false);
                        searchView.clearFocus();
                        searchView.setIconified(true);

                        if(Master.isNetworkAvailable(getApplicationContext())){
                            searchItemClickListnerFlag=1;
                            //code to display filtered results based on query entered
                            try {
                                makeSearchReq(products.get(position));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else {
                            // toast of no internet connection
                            Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                            rlNoInternetConnection.setVisibility(View.VISIBLE);
                            rlProductType.setVisibility(View.GONE);
                            contentScrolling.setVisibility(View.GONE);
                            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                        }

                        searchMenuItem.collapseActionView();
                        searchView.setQuery("", false);
                        searchView.clearFocus();
                        searchView.setIconified(true);
                    }

                    /*@Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }*/
                })
        );

        return true;

    }

    private void searchItemClickListner() {


        FrameLayout layout = findViewById(R.id.fragment_container);
        layout.setVisibility(View.VISIBLE);

        FrameLayout layout2 = findViewById(R.id.search_framelayout);
        layout2.setVisibility(View.GONE);

                Master.navItemIndex = Master.NAV_SEARCH_INDEX;
                Master.CURRENT_TAG = Master.SEARCH_FRAGMENT_TAG;

                SearchFragment searchFragment = new SearchFragment();
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, searchFragment, Master.SEARCH_FRAGMENT_TAG)
                        .commit();

        if(products.size()>0) {
            findViewById(R.id.rlNoMatchSearchSuggestion).setVisibility(View.GONE);
        } else {
            findViewById(R.id.rlNoMatchSearchSuggestion).setVisibility(View.VISIBLE);
        }


    }


    private void searchBarSuggestion(String newText){
        ArrayList<SpannableString> spannableProducts = new ArrayList<>();

        size = products.size();
        if(size > 0) {

            findViewById(R.id.rlNoMatchSearchSuggestion).setVisibility(View.GONE);

            for (int i = 0; i < size; i++) {
                String item = products.get(i).toLowerCase();
                String qr = newText.toLowerCase();

                try {
                    int index = item.indexOf(qr);
                    if (index >= 0) {

                        SpannableString greenSpannable = new SpannableString(item);
                        greenSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#439e47")), index, index + qr.length(), 0);

                        if (item.contains(qr)) {
                            spannableProducts.add(greenSpannable);
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            SearchBarAdapter searchBarAdapter = new SearchBarAdapter(spannableProducts);

        //now update list view with new adapter
        listView.setAdapter(searchBarAdapter);

        } else {
            findViewById(R.id.rlNoMatchSearchSuggestion).setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            //use the query to search your data somehow

            if(Master.isNetworkAvailable(getApplicationContext())){
                searchItemClickListnerFlag = 1;
                //code to display filtered results based on query entered
                makeSearchReq(query);
            }else {
                // toast of no internet connection
                Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                rlNoInternetConnection.setVisibility(View.VISIBLE);
                rlProductType.setVisibility(View.GONE);
                contentScrolling.setVisibility(View.GONE);
                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            }

        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            Intent i = new Intent(DashboardActivity.this, CartActivity.class);
                startActivity(i);
            return true;
        }

        if(id == R.id.action_search){
            FrameLayout layout = findViewById(R.id.fragment_container);
            layout.setVisibility(View.GONE);
            rlProductType.setVisibility(View.GONE);
            FrameLayout layout2 = findViewById(R.id.search_framelayout);
            layout2.setVisibility(View.VISIBLE);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //May be useful in future extension!
    /*private void makeProductTypeRequest() {

        System.out.println("  ++onMakeProductTypeRequest++  ");
        Master.dismissProgressDialog();
        networkCommunicator.data(Master.getProductTypeAPI(),
                Request.Method.GET,
                null,
                true,
                new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        Log.d(TAG, result.toString());
                        String response = (String)result;
                        parseJSON(response);
                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        Log.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
                    }
                },Master.SCROLLING_ACTIVITY_TAG, getApplicationContext());

    }

    private void parseJSON(String jsonString){
        List<String> typename = new ArrayList<>();

        if(Master.product_type != null)
            Master.product_type.clear();

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        ProductType productType = gson.fromJson(jsonString, ProductType.class);

        Master.product_type = productType.productTypeDetails;

        for(ProductTypeDetail productTypeDetail : productType.productTypeDetails){
            typename.add(productTypeDetail.getName());
        }

        productTypeAdapter = new ProductTypeAdapter(getApplicationContext(), images);
        mRecyclerView.setAdapter(productTypeAdapter);

    }*/

    @Override
    public void onProfileFragmentInteraction() {

        Master.navItemIndex = Master.NAV_EDIT_PROFILE_INDEX;
        Master.CURRENT_TAG = Master.EDIT_PROFILE_FRAGMENT_TAG;

        EditProfileFragment editProfileFragment = new EditProfileFragment();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, editProfileFragment )
                .commit();

    }

    @Override
    public void onEditProfileFragmentInteraction() {

        Master.navItemIndex = Master.NAV_PROFILE_INDEX;
        Master.CURRENT_TAG = Master.PROFILE_FRAGMENT_TAG;

        ProfileFragment profileFragment = new ProfileFragment();

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, profileFragment )
                .commit();

    }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, phonenumber, notifications action view (dot)
     */
    public void loadNavHeader() {
        // name, phonenumber, email
        if(MemberDetails.getMobileNumber() != null) {
            if (!MemberDetails.getMobileNumber().equals("guest")) {

                String msg = MemberDetails.getFname() + " " + MemberDetails.getLname();
                tvNavHeadName.setText(msg);
                tvNavHeadPhoneNumber.setText(MemberDetails.getMobileNumber());
                tvNavHeadEmail.setText(MemberDetails.getEmail());

            } else if (MemberDetails.getMobileNumber().equals("guest")) {
                tvNavHeadName.setText(getString(R.string.guest));
            }
        }

        // loading header background image
        Glide.with(this).load(R.drawable.nav_header)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(R.drawable.profile_pic)
                .into(imgNavHeaderBg);

        imgNavHeaderBg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Master.navItemIndex = Master.NAV_PROFILE_INDEX;
                Master.CURRENT_TAG = Master.PROFILE_FRAGMENT_TAG;
                loadHomeFragment();
            }
        });


        // edit profile button
        ivEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Master.navItemIndex = Master.NAV_PROFILE_INDEX;
                Master.CURRENT_TAG = Master.PROFILE_FRAGMENT_TAG;
                loadHomeFragment();
            }
        });

    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {

        if(Master.navItemIndex != Master.NAV_CONTACT_US_INDEX) {

            // selecting appropriate nav menu item
            selectNavMenu();

            // set toolbar title
            setToolbarTitle();

            // if user select the current navigation menu again, don't do anything
            // just close the navigation drawer
            if (getSupportFragmentManager().findFragmentByTag(Master.CURRENT_TAG) != null) {
                drawer.closeDrawers();
                return;
            }

            // Sometimes, when fragment has huge data, screen seems hanging
            // when switching between navigation menus
            // So using runnable, the fragment is loaded with cross fade effect
            // This effect can be seen in GMail app

                Runnable mPendingRunnable = new Runnable() {
                    @Override
                    public void run() {
                        // update the main content by replacing fragments
                        Fragment fragment = getHomeFragment();
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.fragment_container, fragment, Master.CURRENT_TAG);
                        fragmentTransaction.commitAllowingStateLoss();
                    }
                };

            // If mPendingRunnable is not null, then add to the message queue
            if (mPendingRunnable != null) {
                mHandler.post(mPendingRunnable);
            }

            //Closing drawer on item click
            drawer.closeDrawers();

        }

    }

    private Fragment getHomeFragment() {
        switch (Master.navItemIndex) {
            case 0:
                // ProductDetail
                rlProductType.setVisibility(View.VISIBLE);
                ProductFragment productFragment = new ProductFragment();
                Bundle b = new Bundle();
                try {
                    b.putString("typename", Master.product_type.get(0).getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                productFragment.setArguments(b);
                return productFragment;
            /*case 1:
                // OrdersDetail fragment
                rlProductType.setVisibility(View.GONE);
                return new OrdersFragment();*/
            case 2:
                // Profile fragment
                rlProductType.setVisibility(View.GONE);
                return new ProfileFragment();
            case 3:
                // AboutUS
                rlProductType.setVisibility(View.GONE);
                return new AboutUsFragment();
            case 11:
                // HomePage
                rlProductType.setVisibility(View.VISIBLE);
                return new HomePageFragment();

            case 12:
                // EditProfile fragment
                rlProductType.setVisibility(View.VISIBLE);
                return new EditProfileFragment();

            case 13:
                // Search fragment
                rlProductType.setVisibility(View.VISIBLE);
                return new SearchFragment();

            default:
                rlProductType.setVisibility(View.VISIBLE);
                return new ProductFragment();
        }
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[Master.navItemIndex]);
    }

    private void selectNavMenu() {
        if(Master.navItemIndex == Master.NAV_CONTACT_US_INDEX){
            navigationView.getMenu().getItem(Master.navItemIndex).setChecked(false);
        } else if(Master.navItemIndex < Master.NAV_HOME_INDEX){
             navigationView.getMenu().getItem(Master.navItemIndex).setChecked(true);
        }
    }

    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                FragmentManager fragmentManager = getFragmentManager();
                for(int i= 0;i<fragmentManager.getBackStackEntryCount();i++) {
                    fragmentManager.popBackStack();
                }

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_product:
                        Master.navItemIndex = Master.NAV_PRODUCT_INDEX;
                        Master.CURRENT_TAG = Master.PRODUCT_FRAGMENT_TAG;
                        break;

                    case R.id.nav_orders:
                        Master.navItemIndex = Master.NAV_ORDERS_INDEX;
                        Master.CURRENT_TAG = Master.ORDERS_FRAGMENT_TAG;
                        navigationView.getMenu().getItem(Master.navItemIndex).setChecked(false);
                        startActivity(new Intent(getApplication(), OrdersActivity.class));
                        drawer.closeDrawers();
                        return false;

                    case R.id.nav_profile:
                        Master.navItemIndex = Master.NAV_PROFILE_INDEX;
                        Master.CURRENT_TAG = Master.PROFILE_FRAGMENT_TAG;
                        break;

                    case R.id.nav_about_us:
                        Master.navItemIndex = Master.NAV_ABOUT_US_INDEX;
                        Master.CURRENT_TAG = Master.ABOUT_US_FRAGMENT_TAG;
                        break;

                    case R.id.nav_contact_us:
                        //if(prevMaster.NavItemIndex != 4)
                        Master.prevNavItemIndex = Master.navItemIndex;
                        Master.navItemIndex = Master.NAV_CONTACT_US_INDEX;
                        Master.CURRENT_TAG = Master.CONTACT_US_FRAGMENT_TAG;
                        navigationView.getMenu().getItem(Master.navItemIndex).setChecked(false);
                        BottomSheetDialogFragment bottomSheetDialogFragment = new ContactUsFragment();
                        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
                        drawer.closeDrawers();
                        break;

                    case R.id.nav_watch_video:
                        Master.navItemIndex = Master.NAV_WATCH_VIDEOS_INDEX;
                        Master.CURRENT_TAG = Master.WATCH_VIDEOS_TAG;
                        SharedPreferenceConnector.writeString(getApplicationContext(), "redirectto", "homepage");
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                        intent.setData(Uri.parse("https://lvweb.lokavidya.com/"));
                        startActivity(intent);
                        drawer.closeDrawers();
                        return true;

                    case R.id.nav_faqs:
                        // launch new intent instead of loading fragment
                        Master.navItemIndex = Master.NAV_FAQS_INDEX;
                        Master.CURRENT_TAG = Master.FAQs_TAG;
                        SharedPreferenceConnector.writeString(getApplicationContext(), "redirectto", "homepage");
                        startActivity(new Intent(getApplication(), FAQsActivity.class));
                        drawer.closeDrawers();
                        return true;

                    case R.id.nav_feedback:
                        // launch new intent instead of loading fragment
                        Master.navItemIndex = Master.NAV_FEEDBACK_INDEX;
                        Master.CURRENT_TAG = Master.FEEDBACK_ACTIVITY_TAG;
                        SharedPreferenceConnector.writeString(getApplicationContext(), "redirectto", "homepage");
                        startActivity(new Intent(getApplication(), FeedbackActivity.class));
                        drawer.closeDrawers();
                        return true;

                    default:
                        Master.navItemIndex = Master.NAV_PRODUCT_INDEX;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer);

        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);

        //Setting the actionbarToggle to drawer layout
        //drawer.setDrawerListener(actionBarDrawerToggle);
        drawer.addDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private void onBackToProductFragment() {
        Master.isFromCart = false;
        ProductFragment productFragment = (ProductFragment) getSupportFragmentManager().findFragmentByTag(Master.PRODUCT_FRAGMENT_TAG);

        if(productFragment == null) {
            Bundle b = new Bundle();
            b.putString("typename", Master.productTypeValue);
            productFragment = new ProductFragment();
            productFragment.setArguments(b);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, productFragment, Master.PRODUCT_FRAGMENT_TAG)
                    .commit();
        }else {
            productFragment.onDataChange(Master.productTypeValue);
        }
    }

    @Override
    public void changeNavigationItemSelection(int index, Boolean value) {
        navigationView.getMenu().getItem(index).setChecked(value);
    }


}