package com.mobile.ict.lokacartplus.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.Button;

import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.utils.Master;

/**
 * Created by Toshiba on 01-06-2017.
 */


@SuppressWarnings("DefaultFileTemplate")
public class ContactUsFragment extends BottomSheetDialogFragment implements View.OnClickListener{

    private final BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_contact_us, null);
        dialog.setContentView(contentView);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        Button contactUsButton = contentView.findViewById(R.id.button_contact_us);
        Button mailUsButton = contentView.findViewById(R.id.button_mail_us);
        Button websiteButton = contentView.findViewById(R.id.button_website);
        Button facebookButton = contentView.findViewById(R.id.button_facebook);

        contactUsButton.setOnClickListener(this);
        mailUsButton.setOnClickListener(this);
        websiteButton.setOnClickListener(this);
        facebookButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.button_contact_us:
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:+919773232509"));
                startActivity(intent);
                break;
            case R.id.button_mail_us:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "lokacart@cse.iitb.ac.in", null));
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
                break;
            case R.id.button_website:
                String url = "http://ruralict.cse.iitb.ac.in/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.button_facebook:
                Intent facebookIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/RuralICT.iitb/"));
                startActivity(facebookIntent);
                break;

        }
    }

    @Override
    public void onDetach(){
        AccessNavigationView accessNavigationView = (AccessNavigationView) getContext();
        accessNavigationView.changeNavigationItemSelection(Master.navItemIndex, false);
        if(Master.prevNavItemIndex < Master.NAV_HOME_INDEX){
            accessNavigationView.changeNavigationItemSelection(Master.prevNavItemIndex, true);
        }

        super.onDetach();
    }

    public interface AccessNavigationView{
        void changeNavigationItemSelection(int index, Boolean value);
    }

}