package com.mobile.ict.lokacartplus.fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobile.ict.lokacartplus.activity.DashboardActivity;
import com.mobile.ict.lokacartplus.interfaces.NoInternetConnectionListener;
import com.mobile.ict.lokacartplus.model.Product;
import com.mobile.ict.lokacartplus.model.ProductDetail;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.adapter.ProductAdapter;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ProductFragment extends Fragment {
    private final String TAG = ProductFragment.class.getSimpleName();

    private RecyclerView productRecyclerView;
    private RecyclerView.Adapter productAdapter;
    private RelativeLayout relativeLayoutNoProducts;

    private DBHelper dbHelper;
    private String[] cartProductIds = null;

    private NetworkCommunicator networkCommunicator;
    private Context mContext;
    private View productFragmentView;

    public ProductFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getContext();
        if (getArguments() != null) {
            Master.productTypeValue = getArguments().getString("typename");

            dbHelper = DBHelper.getInstance(getContext());
            cartProductIds = dbHelper.getCartProductIds(MemberDetails.getMobileNumber());
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        productFragmentView = inflater.inflate(R.layout.fragment_product, container, false);

        // This is used for implementing back button
        ((DashboardActivity)getActivity()).changeToolBar(Master.productTypeValue);

        //code in onActivityCreated was here...

        return productFragmentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        productRecyclerView = productFragmentView.findViewById(R.id.product_recycler_view);

        networkCommunicator = NetworkCommunicator.getInstance(/*getContext()*/);

        relativeLayoutNoProducts = productFragmentView.findViewById(R.id.rlNoProducts);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        productRecyclerView.setNestedScrollingEnabled(false);
        productRecyclerView.setHasFixedSize(false);

        RecyclerView.LayoutManager productLayoutManager = new LinearLayoutManager(getActivity());
        productRecyclerView.setLayoutManager(productLayoutManager);

        if(Master.isNetworkAvailable(getContext())){
            Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
            makeJsonObjReq();
        }else {
            // toast of no internet connection
            Toast.makeText(getContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
            NoInternetConnectionListener listener = (NoInternetConnectionListener) getContext();
            listener.showNoInternetConnectionLayout();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if(dbHelper==null)
            dbHelper= DBHelper.getInstance(getActivity());

        dbHelper.getSignedInProfile();

        cartProductIds = dbHelper.getCartProductIds(MemberDetails.getMobileNumber());

        if(networkCommunicator==null) {
            networkCommunicator = NetworkCommunicator.getInstance(/*getContext()*/);
        }

        getActivity().invalidateOptionsMenu();

    }

    public void onDataChange(String productName) {
        if(Master.isNetworkAvailable(getContext())){
            Master.productTypeValue = productName;
            ((DashboardActivity)getActivity()).changeToolBar(Master.productTypeValue);
            Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
            makeJsonObjReq();
        }else {
            Toast.makeText(getContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
            NoInternetConnectionListener listener = (NoInternetConnectionListener) getContext();
            listener.showNoInternetConnectionLayout();
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        ProductFragment productFragment = (ProductFragment) getFragmentManager().findFragmentByTag(Master.PRODUCT_FRAGMENT_TAG);
        if(productFragment != null)
            getFragmentManager().beginTransaction().remove(productFragment).commit();
    }

    private void makeJsonObjReq() {

        Map<String, String> params = new HashMap<>();
        params.put(Master.PRODUCT_TYPE, Master.productTypeValue);
        JSONObject jo = new JSONObject(params);

        networkCommunicator.data(Master.getTypeWiseProductsAPI(),
                Request.Method.POST,
                jo,
                true,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;

                        parseJSON(response);

                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        error.printStackTrace();
                        Toast.makeText(getContext(),getContext().getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();

                    }
                },Master.PRODUCT_FRAGMENT_TAG, getContext());

    }

    private void parseJSON(String jsonString){

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        Product gsonArrays = gson.fromJson(jsonString, Product.class);

        if(Master.productDetailList != null)
            Master.productDetailList.clear();

        Master.productDetailList = gsonArrays.productDetails;

        for(ProductDetail p : gsonArrays.productDetails){

            String id = p.getId();
            if(isProductInCart(id) == 1)
            {
                dbHelper = DBHelper.getInstance(getContext());
                p.setQuantity(dbHelper.getCartQuantity(MemberDetails.getMobileNumber(), id));
                updateCart(p);
            }else {
                p.setQuantity(0);
            }
        }

        if (Master.productDetailList.size() > 0) {
            productAdapter = new ProductAdapter(getContext());
            productRecyclerView.setAdapter(productAdapter);
            productAdapter.notifyDataSetChanged();
        } else {
            productRecyclerView.setVisibility(View.GONE);
            relativeLayoutNoProducts.setVisibility(View.VISIBLE);
        }

    }

    private int isProductInCart(String id){
        for (String cartProductId : cartProductIds) {
            if (cartProductId.equals(id)) return 1;
        }
        return 0;
    }

    private void updateCart(ProductDetail productDetail) {
        int size = Master.cartList.size();
        for (int z = 0; z < size; z++) {

            if (Master.cartList.get(z).getId().equals(productDetail.getId())) {

                Master.cartList.get(z).setId(productDetail.getId());
                Master.cartList.get(z).setImageUrl(productDetail.getImageUrl());
                Master.cartList.get(z).setGst(productDetail.getGst());
                Master.cartList.get(z).setMoq(productDetail.getMoq());
                Master.cartList.get(z).setName(productDetail.getName());
                Master.cartList.get(z).setQuantity(productDetail.getQuantity());
                Master.cartList.get(z).setOrganization(productDetail.getOrganization());
                Master.cartList.get(z).setUnit(productDetail.getUnit());
                Master.cartList.get(z).setUnitRate(productDetail.getUnitRate());

                Master.cartList.get(z).setItemTotal(Master.cartList.get(z).getUnitRate() * Master.cartList.get(z).getQuantity());

                dbHelper.updateProduct(
                        String.valueOf(Master.cartList.get(z).getUnitRate()),
                        String.valueOf(Master.cartList.get(z).getQuantity()),
                        String.valueOf(Master.cartList.get(z).getItemTotal()),
                        String.valueOf(Master.cartList.get(z).getName()),
                        MemberDetails.getMobileNumber(),
                        String.valueOf(Master.cartList.get(z).getOrganization()),
                        String.valueOf(Master.cartList.get(z).getId()),
                        String.valueOf(Master.cartList.get(z).getImageUrl()),
                        String.valueOf(Master.cartList.get(z).getUnit()),
                        String.valueOf(Master.cartList.get(z).getMoq()),
                        String.valueOf(Master.cartList.get(z).getGst())

                );
            }
        }
    }

}
