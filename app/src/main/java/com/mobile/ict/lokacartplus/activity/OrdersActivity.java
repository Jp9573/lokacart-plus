package com.mobile.ict.lokacartplus.activity;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.adapter.CurrentOrdersAdapter;
import com.mobile.ict.lokacartplus.adapter.PastOrdersAdapter;
import com.mobile.ict.lokacartplus.model.MemberDetails;
import com.mobile.ict.lokacartplus.model.Orders;
import com.mobile.ict.lokacartplus.model.OrdersDetail;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrdersActivity extends AppCompatActivity implements CurrentOrdersAdapter.OrdersInterface {

    SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView currentOrdersRecyclerView;
    private CurrentOrdersAdapter currentOrdersAdapter;
    private PastOrdersAdapter pastOrdersAdapter;
    Context mContext;
    private TextView tvNoOrders;
    private static NetworkCommunicator networkCommunicator;
    private Toolbar toolbar;
    boolean isOnCurrentOrders = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);

        mContext = this;
        swipeRefreshLayout = findViewById(R.id.swipable_scrolling);
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_green_dark,
                android.R.color.holo_red_dark,
                android.R.color.holo_blue_dark,
                android.R.color.holo_orange_dark);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(Master.isNetworkAvailable(getApplicationContext())){
                    Master.showProgressDialog(mContext, getString(R.string.fetching_order));
                    swipeRefreshLayout.setRefreshing(true);
                    showOrdersFunction();
                }else {
                    // toast of no internet connection
                    Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        networkCommunicator = NetworkCommunicator.getInstance();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        currentOrdersRecyclerView = findViewById(R.id.current_orders_recycler_view);
        currentOrdersRecyclerView.setHasFixedSize(true);
        currentOrdersRecyclerView.setNestedScrollingEnabled(false);
        currentOrdersRecyclerView.setLayoutManager(layoutManager);

        final TextView tvCurrentOrders = findViewById(R.id.tvCurrentOrders);
        final TextView tvPastOrders = findViewById(R.id.tvPastOrders);

        tvNoOrders = findViewById(R.id.tvNoPastOrders);

        tvCurrentOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvCurrentOrders.setBackgroundResource(R.color.BlueBackground);
                tvPastOrders.setBackgroundResource(R.color.colorWhiteAccent);
                currentOrdersRecyclerView.setAdapter(currentOrdersAdapter);
                tvCurrentOrders.setTextSize(18);
                tvPastOrders.setTextSize(14);
                isOnCurrentOrders = true;
                if(currentOrdersAdapter.getItemCount() == 0) {
                    tvNoOrders.setVisibility(View.VISIBLE);
                }else {
                    tvNoOrders.setVisibility(View.GONE);
                }
            }
        });

        tvCurrentOrders.setBackgroundResource(R.color.colorWhiteAccent);
        tvCurrentOrders.setTextSize(18);

        tvPastOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvPastOrders.setBackgroundResource(R.color.BlueBackground);
                tvCurrentOrders.setBackgroundResource(R.color.colorWhiteAccent);
                currentOrdersRecyclerView.setAdapter(pastOrdersAdapter);
                tvPastOrders.setTextSize(18);
                tvCurrentOrders.setTextSize(14);
                isOnCurrentOrders = false;
                if(pastOrdersAdapter.getItemCount() == 0)
                    tvNoOrders.setVisibility(View.VISIBLE);
                else
                    tvNoOrders.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showOrdersFunction(){

        JSONObject phoneNumber = new JSONObject();
        try {
            phoneNumber.put("number", String.valueOf(MemberDetails.getMobileNumber()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        networkCommunicator.data(Master.getOrdersAPI(),
                Request.Method.POST,
                phoneNumber,
                false,
                new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        swipeRefreshLayout.setRefreshing(false);
                        Master.dismissProgressDialog();
                        String response = (String)result;

                        ArrayList<OrdersDetail> currentOrders = new ArrayList<>();
                        ArrayList<OrdersDetail> pastOrders = new ArrayList<>();

                        GsonBuilder gsonBuilder = new GsonBuilder();
                        Gson gson = gsonBuilder.create();
                        Orders ordersGSON = gson.fromJson(response, Orders.class);

                        for (OrdersDetail ordersDetail : ordersGSON.ordersDetailArrayList) {
                            if(ordersDetail.isPaid() || ordersDetail.getStatus().equals("cancelled")){
                                pastOrders.add(ordersDetail);
                            } else {
                                currentOrders.add(ordersDetail);
                            }
                        }

                        if(currentOrders.size() == 0) {
                            tvNoOrders.setVisibility(View.VISIBLE);
                        } else {
                            tvNoOrders.setVisibility(View.GONE);
                        }

                        /*if(currentOrders.size()==0){
                            if(isOnCurrentOrders)
                                tvNoCurrentOrders.setVisibility(View.VISIBLE);
                        } else {
                            if(!isOnCurrentOrders)
                                tvNoCurrentOrders.setVisibility(View.GONE);
                        }*/

                        currentOrdersAdapter = new CurrentOrdersAdapter(OrdersActivity.this, currentOrders, pastOrders, OrdersActivity.this);
                        pastOrdersAdapter = new PastOrdersAdapter(pastOrders);

                        if(isOnCurrentOrders) {
                            currentOrdersRecyclerView.setAdapter(currentOrdersAdapter);
                        }else {
                            currentOrdersRecyclerView.setAdapter(pastOrdersAdapter);
                        }

                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        swipeRefreshLayout.setRefreshing(false);
                        Master.dismissProgressDialog();
                        Toast.makeText(mContext,"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
                    }
                },Master.ORDERS_FRAGMENT_TAG, mContext);

    }

    @Override
    public void updateAdapter(int position,
                              ArrayList<OrdersDetail> currentOrders,
                              ArrayList<OrdersDetail> pastOrders){

        currentOrders.get(position).setStatus("cancelled");
        pastOrders.add(currentOrders.get(position));
        pastOrdersAdapter.notifyDataSetChanged();
        //pastOrdersRecyclerView.invalidate();

        if(pastOrders.size()>1){
            tvNoOrders.setVisibility(View.GONE);
        }

        currentOrders.remove(position);
        currentOrdersRecyclerView.removeViewAt(position);
        currentOrdersAdapter.notifyItemRemoved(position);
        currentOrdersAdapter.notifyItemRangeChanged(position, currentOrders.size());
        currentOrdersAdapter.notifyDataSetChanged();
        currentOrdersRecyclerView.invalidate();

    }

    @Override
    public void onResume() {
        super.onResume();

        if(networkCommunicator==null) {
            networkCommunicator = NetworkCommunicator.getInstance();
        }

        if(Master.isNetworkAvailable(getApplicationContext())){
            Master.showProgressDialog(mContext, getString(R.string.fetching_order));
            showOrdersFunction();
        }else {
            // toast of no internet connection
            Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
        }

    }

}
