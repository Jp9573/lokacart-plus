package com.mobile.ict.lokacartplus.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.interfaces.DeleteProductListener;
import com.mobile.ict.lokacartplus.model.ProductDetail;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import java.text.DecimalFormat;

/**
 * Created by Toshiba on 25/3/17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    private final Context mContext;
    private DBHelper dbHelper;
    private final TextView cartTotal;
    private final TextView tvItemTotal;
    private double sum = 0.0;
    private final DeleteProductListener deleteProductListener;
    private final DecimalFormat df2 = new DecimalFormat("0.00");
    private int size;

    public CartAdapter(Context mContext, TextView cartTotal, TextView tvItemTotal) {
        this.mContext = mContext;
        this.cartTotal = cartTotal;
        this.tvItemTotal = tvItemTotal;
        this.deleteProductListener = (DeleteProductListener) mContext;

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public final TextView tvProductName;
        public final TextView tvMoq;
        public final TextView tvPrice;
        public final TextView tvOrgName;
        public final TextView tvUnit;
        public final TextView tvGst;
        public final ImageView productImageView;
        public final ImageButton minusButton;
        public final ImageButton plusButton;
        public final ImageButton deleteButton;
        public final TextView etKgs;

        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout
            tvProductName = v.findViewById(R.id.tvProductName);
            tvProductName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvMoq = v.findViewById(R.id.tvMoq);
            tvMoq.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvPrice = v.findViewById(R.id.tvPrice);
            tvPrice.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvGst = v.findViewById(R.id.tvGst);
            tvGst.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrgName = v.findViewById(R.id.tvOrgName);
            tvOrgName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_LIGHT));

            etKgs = v.findViewById(R.id.etKgs);
            etKgs.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvUnit = v.findViewById(R.id.tvUnit);
            tvUnit.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            productImageView = v.findViewById(R.id.product_imageview);
            minusButton = v.findViewById(R.id.buttonMinus);
            plusButton = v.findViewById(R.id.buttonPlus);
            deleteButton = itemView.findViewById(R.id.buttonDelete);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_cart,parent,false);
        final ViewHolder vh = new ViewHolder(v);

        // to hide keyboard after enter is pressed
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow( v.getWindowToken(), 0);

        return vh;
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder,  final int position) {

        final ProductDetail pD = Master.cartList.get(position);

        try{
            holder.tvProductName.setText(pD.getName());
            holder.tvOrgName.setText(pD.getOrganization());
            holder.tvPrice.setText(Html.fromHtml("1 "+ pD.getUnit() + " for \u20B9 " + "<font color=#439e47>" + pD.getUnitRate() + "</font>"));
            String msg = mContext.getResources().getString(R.string.textview_gst)+ " "+ pD.getGst()+mContext.getResources().getString(R.string.textview_percent);
            holder.tvGst.setText(msg);

            msg = "Min. Order - "+ pD.getMoq()+" "+ pD.getUnit();
            holder.tvMoq.setText(msg);
            holder.etKgs.setText(String.valueOf(pD.getQuantity()));
            holder.tvUnit.setText(String.valueOf(pD.getUnit()));

            holder.etKgs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showEditEtKgsDialog(holder.etKgs);
                }
            });

            holder.tvUnit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showEditEtKgsDialog(holder.etKgs);
                }
            });


            TextWatcher etKgsTextWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    dbHelper = DBHelper.getInstance(mContext);

                    if (!holder.etKgs.getText().toString().equals("")) {

                        try {

                            holder.minusButton.setEnabled(true);

                            int qty = Integer.parseInt(holder.etKgs.getText().toString());

                            if (qty < pD.getMoq()) {

                                Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                                holder.etKgs.setText(String.valueOf(pD.getMoq()));
                                pD.setQuantity(pD.getMoq());
                                dbHelper.updateProduct(
                                        String.valueOf(pD.getUnitRate()),
                                        String.valueOf(pD.getMoq()),
                                        String.valueOf(pD.getUnitRate() * pD.getMoq()),
                                        String.valueOf(pD.getName()),
                                        String.valueOf(MemberDetails.getMobileNumber()),
                                        String.valueOf(pD.getOrganization()),
                                        String.valueOf(pD.getId()),
                                        String.valueOf(pD.getImageUrl()),
                                        String.valueOf(pD.getUnit()),
                                        String.valueOf(pD.getMoq()),
                                        String.valueOf(pD.getGst())
                                );

                                //set updated item total
                                Master.cartList.get(position).setItemTotal(pD.getUnitRate() * pD.getQuantity());

                            } else {
                                pD.setQuantity(qty);
                                dbHelper.updateProduct(
                                        String.valueOf(pD.getUnitRate()),
                                        String.valueOf(qty),
                                        String.valueOf(pD.getUnitRate()*qty),
                                        String.valueOf(pD.getName()),
                                        String.valueOf(MemberDetails.getMobileNumber()),
                                        String.valueOf(pD.getOrganization()),
                                        String.valueOf(pD.getId()),
                                        String.valueOf(pD.getImageUrl()),
                                        String.valueOf(pD.getUnit()),
                                        String.valueOf(pD.getMoq()),
                                        String.valueOf(pD.getGst())
                                );

                                //set updated item total
                                Master.cartList.get(position).setItemTotal(pD.getUnitRate() * pD.getQuantity());
                            }

                        } catch (NumberFormatException ignored) {
                        }
                    } else {

                        Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                        holder.etKgs.setText(String.valueOf(pD.getMoq()));
                        pD.setQuantity(pD.getMoq());
                        dbHelper.updateProduct(
                                String.valueOf(pD.getUnitRate()),
                                String.valueOf(pD.getMoq()),
                                String.valueOf(pD.getUnitRate() * pD.getMoq()),
                                String.valueOf(pD.getName()),
                                String.valueOf(MemberDetails.getMobileNumber()),
                                String.valueOf(pD.getOrganization()),
                                String.valueOf(pD.getId()),
                                String.valueOf(pD.getImageUrl()),
                                String.valueOf(pD.getUnit()),
                                String.valueOf(pD.getMoq()),
                                String.valueOf(pD.getGst())
                        );

                        //set updated item total
                        Master.cartList.get(position).setItemTotal(pD.getUnitRate() * pD.getQuantity());
                    }

                    sum = 0.0;
                    size = Master.cartList.size();
                    for (int i = 0; i < size; i++) {
                        sum = sum + Master.cartList.get(i).getItemTotal();
                    }

                    int tot = (int)Math.round(sum);

                    cartTotal.setText(String.valueOf("\u20B9 " + df2.format(tot)));

                    setTvItemTotal();

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            };

            holder.etKgs.addTextChangedListener(etKgsTextWatcher);

            /*holder.etKgs.setOnEditorActionListener(new EditText.OnEditorActionListener() {

                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if(actionId == EditorInfo.IME_ACTION_DONE){

                        dbHelper = DBHelper.getInstance(mContext);

                        if (!holder.etKgs.getText().toString().equals("")) {

                            try {

                                holder.minusButton.setEnabled(true);

                                int qty = Integer.parseInt(holder.etKgs.getText().toString());

                                if (qty < pD.getMoq()) {

                                    Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                                    holder.etKgs.setText(String.valueOf(pD.getMoq()));
                                    pD.setQuantity(pD.getMoq());
                                    dbHelper.updateProduct(
                                            String.valueOf(pD.getUnitRate()),
                                            String.valueOf(pD.getMoq()),
                                            String.valueOf(pD.getUnitRate() * pD.getMoq()),
                                            String.valueOf(pD.getName()),
                                            String.valueOf(MemberDetails.getMobileNumber()),
                                            String.valueOf(pD.getOrganization()),
                                            String.valueOf(pD.getId()),
                                            String.valueOf(pD.getImageUrl()),
                                            String.valueOf(pD.getUnit()),
                                            String.valueOf(pD.getMoq()),
                                            String.valueOf(pD.getGst())
                                    );

                                    //set updated item total
                                    Master.cartList.get(position).setItemTotal(pD.getUnitRate() * pD.getQuantity());

                                } else {
                                    pD.setQuantity(qty);
                                    dbHelper.updateProduct(
                                            String.valueOf(pD.getUnitRate()),
                                            String.valueOf(qty),
                                            String.valueOf(pD.getUnitRate()*qty),
                                            String.valueOf(pD.getName()),
                                            String.valueOf(MemberDetails.getMobileNumber()),
                                            String.valueOf(pD.getOrganization()),
                                            String.valueOf(pD.getId()),
                                            String.valueOf(pD.getImageUrl()),
                                            String.valueOf(pD.getUnit()),
                                            String.valueOf(pD.getMoq()),
                                            String.valueOf(pD.getGst())
                                    );

                                    //set updated item total
                                    Master.cartList.get(position).setItemTotal(pD.getUnitRate() * pD.getQuantity());
                                }

                            } catch (NumberFormatException ignored) {
                            }
                        } else {

                            Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                            holder.etKgs.setText(String.valueOf(pD.getMoq()));
                            pD.setQuantity(pD.getMoq());
                            dbHelper.updateProduct(
                                    String.valueOf(pD.getUnitRate()),
                                    String.valueOf(pD.getMoq()),
                                    String.valueOf(pD.getUnitRate() * pD.getMoq()),
                                    String.valueOf(pD.getName()),
                                    String.valueOf(MemberDetails.getMobileNumber()),
                                    String.valueOf(pD.getOrganization()),
                                    String.valueOf(pD.getId()),
                                    String.valueOf(pD.getImageUrl()),
                                    String.valueOf(pD.getUnit()),
                                    String.valueOf(pD.getMoq()),
                                    String.valueOf(pD.getGst())
                            );

                            //set updated item total
                            Master.cartList.get(position).setItemTotal(pD.getUnitRate() * pD.getQuantity());
                        }

                        sum = 0.0;
                        size = Master.cartList.size();
                        for (int i = 0; i < size; i++) {
                            sum = sum + Master.cartList.get(i).getItemTotal();
                        }

                        int tot = (int)Math.round(sum);

                        cartTotal.setText(String.valueOf("\u20B9 " + df2.format(tot)));

                        setTvItemTotal();
                        
                        // to hide keyboard after enter is pressed
                        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow( v.getWindowToken(), 0);

                        return true;
                    }
                    return false;
                }
            });

            holder.etKgs.setOnFocusChangeListener( new View.OnFocusChangeListener(){

                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if(!hasFocus){

                        dbHelper = DBHelper.getInstance(mContext);

                        if (!holder.etKgs.getText().toString().equals("")) {

                            try {

                                holder.minusButton.setEnabled(true);

                                int qty = Integer.parseInt(holder.etKgs.getText().toString());

                                if (qty < pD.getMoq()) {

                                    Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                                    holder.etKgs.setText(String.valueOf(pD.getMoq()));
                                    pD.setQuantity(pD.getMoq());
                                    dbHelper.updateProduct(
                                            String.valueOf(pD.getUnitRate()),
                                            String.valueOf(pD.getMoq()),
                                            String.valueOf(pD.getUnitRate() * pD.getMoq()),
                                            String.valueOf(pD.getName()),
                                            String.valueOf(MemberDetails.getMobileNumber()),
                                            String.valueOf(pD.getOrganization()),
                                            String.valueOf(pD.getId()),
                                            String.valueOf(pD.getImageUrl()),
                                            String.valueOf(pD.getUnit()),
                                            String.valueOf(pD.getMoq()),
                                            String.valueOf(pD.getGst())
                                    );

                                } else {
                                    pD.setQuantity(qty);
                                    dbHelper.updateProduct(
                                            String.valueOf(pD.getUnitRate()),
                                            String.valueOf(qty),
                                            String.valueOf(pD.getUnitRate()*qty),
                                            String.valueOf(pD.getName()),
                                            String.valueOf(MemberDetails.getMobileNumber()),
                                            String.valueOf(pD.getOrganization()),
                                            String.valueOf(pD.getId()),
                                            String.valueOf(pD.getImageUrl()),
                                            String.valueOf(pD.getUnit()),
                                            String.valueOf(pD.getMoq()),
                                            String.valueOf(pD.getGst())
                                    );
                                }

                            } catch (NumberFormatException ignored) {
                            }
                        } else {

                            Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                            holder.etKgs.setText(String.valueOf(pD.getMoq()));
                            pD.setQuantity(pD.getMoq());
                            dbHelper.updateProduct(
                                    String.valueOf(pD.getUnitRate()),
                                    String.valueOf(pD.getMoq()),
                                    String.valueOf(pD.getUnitRate() * pD.getMoq()),
                                    String.valueOf(pD.getName()),
                                    String.valueOf(MemberDetails.getMobileNumber()),
                                    String.valueOf(pD.getOrganization()),
                                    String.valueOf(pD.getId()),
                                    String.valueOf(pD.getImageUrl()),
                                    String.valueOf(pD.getUnit()),
                                    String.valueOf(pD.getMoq()),
                                    String.valueOf(pD.getGst())
                            );
                        }

                        sum = 0.0;
                        size = Master.cartList.size();
                        for (int i = 0; i < size; i++) {
                            sum = sum + Master.cartList.get(i).getItemTotal();
                        }

                        int tot = (int)Math.round(sum);
                        cartTotal.setText(String.valueOf("\u20B9 " +df2.format(tot)));

                        setTvItemTotal();

                    }
                }
            });*/


        }catch (Exception e){
            e.printStackTrace();
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Glide.with(mContext).load(pD.getImageUrl()).thumbnail(0.5f)
                    .crossFade()
                    .placeholder(R.mipmap.ic_launcher_icon)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.productImageView);
        } else {
            Drawable drawable = ResourcesCompat.getDrawable(mContext.getResources(),R.mipmap.ic_launcher_icon,null);
            Glide.with(mContext).load(pD.getImageUrl()).thumbnail(0.5f)
                    .crossFade()
                    .placeholder(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.productImageView);
        }

        final int adapterPosition = holder.getAdapterPosition();

        final ProductDetail pDA = Master.cartList.get(adapterPosition);

        holder.plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.tvMoq.setVisibility(View.VISIBLE);
                holder.minusButton.setVisibility(View.VISIBLE);
                holder.etKgs.setVisibility(View.VISIBLE);
                holder.tvUnit.setVisibility(View.VISIBLE);

                dbHelper = DBHelper.getInstance(mContext);
                int qty =  pDA.getQuantity();
                if(qty == 0){
                    qty = pDA.getMoq();
                    pDA.setQuantity(qty);
                    pDA.setItemTotal(pDA.getUnitRate()*qty);
                    dbHelper.addProduct(
                            String.valueOf(pDA.getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(pDA.getUnitRate()*qty),
                            String.valueOf(pDA.getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(pDA.getOrganization()),
                            String.valueOf(pDA.getId()),
                            String.valueOf(pDA.getImageUrl()),
                            String.valueOf(pDA.getUnit()),
                            String.valueOf(pDA.getMoq()),
                            String.valueOf(pD.getGst())
                    );
                } else {
                    qty++;
                    pDA.setQuantity(qty);
                    pDA.setItemTotal(pDA.getUnitRate()*qty);
                    dbHelper.updateProduct(
                            String.valueOf(pDA.getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(pDA.getUnitRate()*qty),
                            String.valueOf(pDA.getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(pDA.getOrganization()),
                            String.valueOf(pDA.getId()),
                            String.valueOf(pDA.getImageUrl()),
                            String.valueOf(pDA.getUnit()),
                            String.valueOf(pDA.getMoq()),
                            String.valueOf(pD.getGst())
                    );
                }

                holder.etKgs.setText(String.valueOf(qty));

                sum = 0.0;
                size = Master.cartList.size();
                for (int i = 0; i < size; i++) {
                    sum = sum + Master.cartList.get(i).getItemTotal();
                }

                int tot = (int)Math.round(sum);
                cartTotal.setText(String.valueOf("\u20B9 " +df2.format(tot)));
                setTvItemTotal();

            }
        });

        holder.minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dbHelper = DBHelper.getInstance(mContext);
                int qty =  pDA.getQuantity();

                if((qty-1) < pDA.getMoq()){
                    Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_LONG).show();
                }else {
                    qty--;
                    pDA.setQuantity(qty);
                    pDA.setItemTotal(pDA.getUnitRate()*qty);
                    dbHelper.updateProduct(
                            String.valueOf(pDA.getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(pDA.getUnitRate()*qty),
                            String.valueOf(pDA.getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(pDA.getOrganization()),
                            String.valueOf(pDA.getId()),
                            String.valueOf(pDA.getImageUrl()),
                            String.valueOf(pDA.getUnit()),
                            String.valueOf(pDA.getMoq()),
                            String.valueOf(pD.getGst())
                    );
                    holder.etKgs.setText(String.valueOf(qty));
                }

                sum = 0.0;
                size = Master.cartList.size();
                for (int i = 0; i < size; i++) {
                    sum = sum + Master.cartList.get(i).getItemTotal();
                }

                int tot = (int)Math.round(sum);
                cartTotal.setText(String.valueOf("\u20B9 " +df2.format(tot)));

                setTvItemTotal();

            }
        });

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteProductListener.deleteProduct(adapterPosition, Master.cartList.get(adapterPosition).getId());
            }
        });

    }

    void showEditEtKgsDialog(final TextView etkgs) {
        LayoutInflater li = LayoutInflater.from(mContext);
        View dialogView = li.inflate(R.layout.edit_kgs_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);

        alertDialogBuilder.setTitle("Enter Quantity");
        alertDialogBuilder.setView(dialogView);
        final EditText userInput = dialogView.findViewById(R.id.editTextQty);
        userInput.setText(etkgs.getText().toString());

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                etkgs.setText(userInput.getText());
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    private void setTvItemTotal(){
        if(Master.CART_ITEM_COUNT>1) {
            String msg = "You have " + Master.CART_ITEM_COUNT + " items";
            tvItemTotal.setText(msg);
        } else {
            String msg = "You have " + Master.CART_ITEM_COUNT + " item";
            tvItemTotal.setText(msg);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return Master.cartList.size();
    }


}