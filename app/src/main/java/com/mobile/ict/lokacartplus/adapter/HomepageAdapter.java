package com.mobile.ict.lokacartplus.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mobile.ict.lokacartplus.R;

/**
 * Created by Toshiba on 16-02-2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class HomepageAdapter extends RecyclerView.Adapter<HomepageAdapter.ViewHolder> {
    //private String[] mDataset;
    private final int[] images;
    private final Context mContext;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        //public final CardView mCardView;
        public final ImageView homepageImageView;

        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout
            homepageImageView = v.findViewById(R.id.homepage_imageview);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public HomepageAdapter(Context context, int[] images) {
        mContext = context;
        this.images = images;
    }

    @Override
    public HomepageAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_homepage,parent,false);

        // Return the ViewHolder
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Glide.with(mContext).load(images[position]).thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.homepageImageView);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return images.length;
    }
}