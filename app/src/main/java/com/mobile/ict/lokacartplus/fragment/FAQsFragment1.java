package com.mobile.ict.lokacartplus.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.utils.RecyclerItemClickListener;
import com.mobile.ict.lokacartplus.activity.FAQsActivity;
import com.mobile.ict.lokacartplus.adapter.FAQsAdapter;

public class FAQsFragment1 extends Fragment {

    private View rootView;

    public FAQsFragment1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_faqs1, container, false);

        //code in onActivityCreated was here...

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FAQsActivity.navItem = 1;

        String[] FAQsCategory = getResources().getStringArray(R.array.category_faqs_titles);

        RecyclerView faqsRecyclerView = rootView.findViewById(R.id.faqs_recycler_view);

        RecyclerView.LayoutManager faqsLayoutManager = new LinearLayoutManager(getActivity());
        faqsRecyclerView.setLayoutManager(faqsLayoutManager);

        RecyclerView.Adapter faqsAdapter = new FAQsAdapter(FAQsCategory);
        faqsRecyclerView.setAdapter(faqsAdapter);

        faqsRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), faqsRecyclerView,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(int position) {

                        Bundle b = new Bundle();
                        b.putInt("position", position);

                        FAQsFragment2 faQsFragment2 = new FAQsFragment2();
                        faQsFragment2.setArguments(b);

                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fragment_container, faQsFragment2 )
                                .commit();

                    }

                    /*@Override public void onLongItemClick(View view, int position) {
                        // do whatever
                    }*/
                })
        );
    }

}
