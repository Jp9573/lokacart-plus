package com.mobile.ict.lokacartplus.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.R;

public class AboutUsFragment extends Fragment implements View.OnClickListener{

    private TextView tvTitleAboutLokacartPlus,
            tvContentAboutLokacartPlus,
            tvTitleDesignAndDevelopment,
            tvContentDesignAndDevelopment,
            tvTitleCosponsoredByNabard,
            tvContentCosponsoredByNabard,
            tvTitleCosponsoredByMSR,
            tvContentCosponsoredByMSR,
            tvTitleDisclaimer,
            tvContentDisclaimer,
            tvTitleTermsAndConditions,
            tvContentTermsAndConditions;

    private View rootView;

    public AboutUsFragment() {}

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setHasOptionsMenu(true);
        rootView = inflater.inflate(R.layout.fragment_about_us, container, false);

        //code in onActivityCreated was here...

        return rootView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tvTitleAboutLokacartPlus = rootView.findViewById(R.id.tvTitleAboutLokacartPlus);
        tvTitleAboutLokacartPlus.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvTitleDesignAndDevelopment = rootView.findViewById(R.id.tvTitleDesignAndDevelopment);
        tvTitleDesignAndDevelopment.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvTitleCosponsoredByNabard = rootView.findViewById(R.id.tvTitleCosponsoredByNabard);
        tvTitleCosponsoredByNabard.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvTitleCosponsoredByMSR = rootView.findViewById(R.id.tvTitleCosponsoredByMSR);
        tvTitleCosponsoredByMSR.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvTitleDisclaimer = rootView.findViewById(R.id.tvTitleDisclaimer);
        tvTitleDisclaimer.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvTitleTermsAndConditions = rootView.findViewById(R.id.tvTitleTermsAndConditions);
        tvTitleTermsAndConditions.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));


        tvContentAboutLokacartPlus = rootView.findViewById(R.id.tvContentAboutLokacartPlus);
        tvContentAboutLokacartPlus.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvContentDesignAndDevelopment = rootView.findViewById(R.id.tvContentDesignAndDevelopment);
        tvContentDesignAndDevelopment.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvContentCosponsoredByNabard = rootView.findViewById(R.id.tvContentCosponsoredByNabard);
        tvContentCosponsoredByNabard.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvContentCosponsoredByMSR = rootView.findViewById(R.id.tvContentCosponsoredByMSR);
        tvContentCosponsoredByMSR.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvContentDisclaimer = rootView.findViewById(R.id.tvContentDisclaimer);
        tvContentDisclaimer.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvContentTermsAndConditions = rootView.findViewById(R.id.tvContentTermsAndConditions);
        tvContentTermsAndConditions.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

        tvTitleAboutLokacartPlus.setOnClickListener(this);
        tvTitleDesignAndDevelopment.setOnClickListener(this);
        tvTitleCosponsoredByNabard.setOnClickListener(this);
        tvTitleCosponsoredByMSR.setOnClickListener(this);
        tvTitleDisclaimer.setOnClickListener(this);
        tvTitleTermsAndConditions.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view==tvTitleAboutLokacartPlus)
        {
            tvContentAboutLokacartPlus.setVisibility(View.VISIBLE);
            tvContentDesignAndDevelopment.setVisibility(View.GONE);
            tvContentCosponsoredByNabard.setVisibility(View.GONE);
            tvContentCosponsoredByMSR.setVisibility(View.GONE);
            tvContentDisclaimer.setVisibility(View.GONE);
            tvContentTermsAndConditions.setVisibility(View.GONE);
        }

        else if(view==tvTitleDesignAndDevelopment)
        {
            tvContentAboutLokacartPlus.setVisibility(View.GONE);
            tvContentDesignAndDevelopment.setVisibility(View.VISIBLE);
            tvContentCosponsoredByNabard.setVisibility(View.GONE);
            tvContentCosponsoredByMSR.setVisibility(View.GONE);
            tvContentDisclaimer.setVisibility(View.GONE);
            tvContentTermsAndConditions.setVisibility(View.GONE);
        }

        else if(view==tvTitleCosponsoredByNabard)
        {
            tvContentAboutLokacartPlus.setVisibility(View.GONE);
            tvContentDesignAndDevelopment.setVisibility(View.GONE);
            tvContentCosponsoredByNabard.setVisibility(View.VISIBLE);
            tvContentCosponsoredByMSR.setVisibility(View.GONE);
            tvContentDisclaimer.setVisibility(View.GONE);
            tvContentTermsAndConditions.setVisibility(View.GONE);
        }

        else if(view==tvTitleCosponsoredByMSR)
        {
            tvContentAboutLokacartPlus.setVisibility(View.GONE);
            tvContentDesignAndDevelopment.setVisibility(View.GONE);
            tvContentCosponsoredByNabard.setVisibility(View.GONE);
            tvContentCosponsoredByMSR.setVisibility(View.VISIBLE);
            tvContentDisclaimer.setVisibility(View.GONE);
            tvContentTermsAndConditions.setVisibility(View.GONE);
        }

        else if(view==tvTitleDisclaimer)
        {
            tvContentAboutLokacartPlus.setVisibility(View.GONE);
            tvContentDesignAndDevelopment.setVisibility(View.GONE);
            tvContentCosponsoredByNabard.setVisibility(View.GONE);
            tvContentCosponsoredByMSR.setVisibility(View.GONE);
            tvContentDisclaimer.setVisibility(View.VISIBLE);
            tvContentTermsAndConditions.setVisibility(View.GONE);
        }

        else if(view==tvTitleTermsAndConditions)
        {
            tvContentAboutLokacartPlus.setVisibility(View.GONE);
            tvContentDesignAndDevelopment.setVisibility(View.GONE);
            tvContentCosponsoredByNabard.setVisibility(View.GONE);
            tvContentCosponsoredByMSR.setVisibility(View.GONE);
            tvContentDisclaimer.setVisibility(View.GONE);
            tvContentTermsAndConditions.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }
}
