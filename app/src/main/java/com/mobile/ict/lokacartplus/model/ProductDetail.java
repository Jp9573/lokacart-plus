package com.mobile.ict.lokacartplus.model;

import com.google.gson.annotations.SerializedName;

import java.text.DecimalFormat;

/**
 * Created by Toshiba on 17/3/17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class ProductDetail {

    @SerializedName("unit")
    private String unit;
    @SerializedName("quantity")
    private int quantity;

    @SerializedName("moq")
    private int moq;
    @SerializedName("itemTotal")
    private double itemTotal;
    @SerializedName("prodQuality")
    private String prodQuality;

    @SerializedName("gst")
    private int gst;
    @SerializedName("imageUrl")
    private String imageUrl;
    @SerializedName("organization")
    private String organization;
    @SerializedName("name")
    private String name;
    @SerializedName("unitRate")
    private double unitRate;
    @SerializedName("logistics")
    private String logistics;
    @SerializedName("description")
    private String description;
    @SerializedName("id")
    private String id;

    public String getProdQuality() {
        return prodQuality;
    }

    public int getGst() {
        return gst;
    }

    public void setGst(int gst) {
        this.gst = gst;
    }

    public double getItemTotal() {
        return itemTotal;
    }

    public void setItemTotal(double itemTotal) {
        this.itemTotal = itemTotal;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getUnitRate() {
        DecimalFormat df2 = new DecimalFormat("#.00");
        return Double.valueOf(df2.format(unitRate));
    }

    public void setUnitRate(double unitRate) {
        this.unitRate = unitRate;
    }

    public String getLogistics() {
        return logistics;
    }

    public String getDescription() {
        return description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMoq() {
        return moq;
    }

    public void setMoq(int moq) {
        this.moq = moq;
    }

    public ProductDetail() {
        this.quantity = 0;
        this.itemTotal = this.quantity * this.unitRate;
    }

}