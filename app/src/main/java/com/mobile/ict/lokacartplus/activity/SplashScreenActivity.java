package com.mobile.ict.lokacartplus.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.android.volley.Request;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.utils.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashScreenActivity extends AppCompatActivity {

    private DBHelper dbHelper;
    private NetworkCommunicator networkCommunicator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        networkCommunicator = NetworkCommunicator.getInstance();

        if(Master.isNetworkAvailable(getApplicationContext())){
            // version update api call
            Master.showProgressDialog(this, getString(R.string.pdialog_loading));
            checkversion();
        }else {
            // toast of no internet connection
            Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
            redirectionFunction();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        //setting guest values of member
        MemberDetails.setMemberDetails();

        dbHelper= DBHelper.getInstance(this);

    }


    private void redirectionFunction(){
        // Duration of wait
        final int SPLASH_DISPLAY_LENGTH = 2000;

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {

                //check if all user details are present in database
                CheckAllDetailsPresent();

                //check if app opened for the first time
                if (SharedPreferenceConnector.readBoolean(getApplicationContext(), Master.STEPPER, Master.DEFAULT_STEPPER)) {

                    Intent i = new Intent(getApplicationContext(), WelcomeActivity.class);
                    startActivity(i);
                    finish();

                } else {

                    Intent i = new Intent(getApplicationContext(), DashboardActivity.class);
                    startActivity(i);
                    finish();

                }

            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void CheckAllDetailsPresent() {
        dbHelper.getProfile();
        String mobileNumber;
        mobileNumber = MemberDetails.getMobileNumber();
        if(mobileNumber.equals("guest")){
            Intent i = new Intent(getApplicationContext(), WelcomeActivity.class);
            startActivity(i);
            finish();
        }
    }


    private void checkversion() {
        String versionCode = "";
        // add package name string
        PackageInfo pInfo;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionCode = String.valueOf(pInfo.versionCode);
        }
        catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        networkCommunicator.data(Master.getVersionCheckAPI()+versionCode,
                Request.Method.GET,
                null,
                false,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null) {
                            try {
                                if(obj.getString("response").equals("0")){
                                    redirectionFunction();
                                } else if(obj.getString("response").equals("1")){
                                    showAlertDialog();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        Toast.makeText(getApplication(),"We are facing some technical issues. Please try again!",Toast.LENGTH_LONG).show();
                        redirectionFunction();
                    }
                },Master.SPLASH_SCREEN_ACTIVITY_TAG, getApplicationContext());

    }


    private void showAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //  Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.force_update_alert_dialog_message)
                .setTitle(R.string.force_update_alert_dialog_title);
        builder.setPositiveButton(R.string.force_update_alert_dialog_positive_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //open play store so that the user can update the app
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                try {
                    //this will open play store and show lokacart app
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    //if opening in play store fails open play store in browser
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }

            }
        });
        builder.setNegativeButton(R.string.force_update_alert_dialog_negative_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //close the app if user clicks close
                finish();
                System.exit(0);
            }
        });

        builder.setCancelable(false);

        //Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(networkCommunicator==null) {
            networkCommunicator = NetworkCommunicator.getInstance();
        }
    }

}
