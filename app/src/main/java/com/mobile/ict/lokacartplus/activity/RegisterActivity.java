package com.mobile.ict.lokacartplus.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.fragment.RegisterFragment1;
import com.mobile.ict.lokacartplus.fragment.RegisterFragment2;

public class RegisterActivity extends AppCompatActivity implements RegisterFragment1.OnFragment1InteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            RegisterFragment1 firstFragment = new RegisterFragment1();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            //firstFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, firstFragment).commit();

        }
    }

    @Override
    public void onFragment1Interaction(Bundle bundle) {

        RegisterFragment2 secondFragment = new RegisterFragment2();
        secondFragment.setArguments(bundle);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, secondFragment )
                .addToBackStack(null)
                .commit();

    }

}
