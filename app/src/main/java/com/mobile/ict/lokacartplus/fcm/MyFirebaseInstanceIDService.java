package com.mobile.ict.lokacartplus.fcm;

import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.

        if(Master.isNetworkAvailable(this)){
            sendRegistrationToServer(refreshedToken);
        }else {
            // toast of no internet connection
            Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
        }

    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        Map<String, String> params = new HashMap<>();
        params.put(Master.TOKEN, token);
        params.put("number", MemberDetails.getMobileNumber());

        NetworkCommunicator networkCommunicator;
        networkCommunicator = NetworkCommunicator.getInstance();

        networkCommunicator.data(Master.getRegisterTokenAPI(),
                Request.Method.POST,
                new JSONObject(params),
                true,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null) {
                            try {
                                if (obj.getString("response").equals("success")) {
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        VolleyLog.d("Firebase", "Error: " + error.getMessage());
                    }
                },Master.MY_FIREBASE_INSTANCE_ID_SERVICE_TAG, getApplicationContext());

    }
}