package com.mobile.ict.lokacartplus.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.model.ProductDetail;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;

import java.text.DecimalFormat;

/**
 * Created by Toshiba on 30-03-2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class ConfirmOrderAdapter extends RecyclerView.Adapter<ConfirmOrderAdapter.ViewHolder> {

    public ConfirmOrderAdapter() {

    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        //public final CardView mCardView;
        public final TextView tvProductName;
        public final TextView tvItemTotal;
        public final TextView tvOrgName;
        public final TextView tvMark;
        public final TextView tvQuantity;

        public ViewHolder(View v){
            super(v);

            tvProductName = v.findViewById(R.id.tvProductName);
            tvProductName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvItemTotal = v.findViewById(R.id.tvItemTotal);
            tvItemTotal.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_MEDIUM));

            tvMark = v.findViewById(R.id.tvMark);
            tvMark.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrgName = v.findViewById(R.id.tvOrgName);
            tvOrgName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvQuantity = v.findViewById(R.id.tvQuantity);
            tvQuantity.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

        }
    }

    @Override
    public ConfirmOrderAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_confirm_order,parent,false);

        // Return the ViewHolder

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        final ProductDetail productDetail = Master.cartList.get(position);
        try{
            holder.tvProductName.setText(productDetail.getName());
            holder.tvOrgName.setText(String.valueOf("from "+productDetail.getOrganization()));

            DecimalFormat df2 = new DecimalFormat("#.00");
            holder.tvItemTotal.setText(String.valueOf("\u20B9 "+ Double.valueOf(df2.format(productDetail.getItemTotal()))));

            holder.tvMark.setText(String.valueOf(position+1));
          if(productDetail.getQuantity()==1){
              holder.tvQuantity.setText(String.valueOf(productDetail.getQuantity()+" unit of"));
          } else if (Master.cartList.get(position).getQuantity()>1){
              holder.tvQuantity.setText(String.valueOf(productDetail.getQuantity()+" units of"));
          }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return Master.cartList.size();
    }

}