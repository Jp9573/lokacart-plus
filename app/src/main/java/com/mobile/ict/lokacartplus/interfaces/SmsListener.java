package com.mobile.ict.lokacartplus.interfaces;

/**
 * Created by Toshiba on 02-05-2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public interface SmsListener {
    void messageReceived(String messageText);
}