package com.mobile.ict.lokacartplus.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.model.OrdersDetail;
import com.mobile.ict.lokacartplus.R;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Created by Toshiba on 11-04-2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class PastOrdersAdapter extends RecyclerView.Adapter<PastOrdersAdapter.ViewHolder> {
    //double sum = 0.0;
    private final ArrayList<OrdersDetail> pastOrders;

    public PastOrdersAdapter(ArrayList<OrdersDetail> pastOrders) {
        this.pastOrders = pastOrders;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public final TextView tvProductName;
        public final TextView tvItemTotal;
        public final TextView tvOrgName;
        public final TextView tvQuantity;
        public final TextView tvOrderId;
        public final TextView tvStatus;
        public final TextView tvDeliveryDate;

        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout

            tvProductName = v.findViewById(R.id.tvProductName);
            tvProductName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvItemTotal = v.findViewById(R.id.tvItemTotal);
            tvItemTotal.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrgName = v.findViewById(R.id.tvOrgName);
            tvOrgName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_LIGHT));

            tvQuantity = v.findViewById(R.id.tvQuantity);
            tvQuantity.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrderId = v.findViewById(R.id.tvOrderId);
            tvOrderId.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvStatus = v.findViewById(R.id.tvStatus);
            tvStatus.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvDeliveryDate = v.findViewById(R.id.tvDeliveryDate);
            tvDeliveryDate.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

        }
    }

    @Override
    public PastOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_past_orders,parent,false);

        // Return the ViewHolder

        return new ViewHolder(v);
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        try {

            holder.tvProductName.setText(String.valueOf("ProductDetail : "+/*Master.*/pastOrders.get(position).getName()));
            holder.tvOrderId.setText(String.valueOf("Order ID : "+/*Master.*/pastOrders.get(position).getOrderItemId()));
            holder.tvOrgName.setText(String.valueOf(/*Master.*/pastOrders.get(position).getOrganization()));

            DecimalFormat df2 = new DecimalFormat("#.00");

            holder.tvItemTotal.setText(String.valueOf(Html.fromHtml("Total : \u20B9 " + "<font color=#439e47>" + Double.valueOf(df2.format(/*Master.*/pastOrders.get(position).getQuantity() * /*Master.*/pastOrders.get(position).getRate())) + "</font>")));
            holder.tvStatus.setText(Html.fromHtml("Status : " + "<font color=#439e47>" + /*Master.*/pastOrders.get(position).getStatus() + "</font>"));
            holder.tvQuantity.setText(String.valueOf("Quantity : "+/*Master.*/pastOrders.get(position).getQuantity()));

            if(/*Master.*/pastOrders.get(position).getStatus().equals("cancelled")){
                holder.tvDeliveryDate.setText("");
            } else {
                holder.tvDeliveryDate.setText(Html.fromHtml("On : " + "<font color=#439e47>" + /*Master.*/pastOrders.get(position).getDelivery_date() + "</font>"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return pastOrders.size();
    }



}