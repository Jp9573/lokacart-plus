package com.mobile.ict.lokacartplus.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.interfaces.NoInternetConnectionListener;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.activity.LoginActivity;
import com.mobile.ict.lokacartplus.activity.RegisterActivity;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

public class ProfileFragment extends Fragment {
    private ScrollView svProfile;
    private RelativeLayout rlGuest;
    private View rootView;

    private OnProfileFragmentInteractionListener mListener;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        setHasOptionsMenu(true);

        //code in onActivityCreated was here...

        return rootView;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(MemberDetails.getMobileNumber().equals("guest"))
        {
            rlGuest = rootView.findViewById(R.id.rlGuest);
            rlGuest.setVisibility(View.VISIBLE);

            svProfile = rootView.findViewById(R.id.svProfile);
            svProfile.setVisibility(View.GONE);

            TextView tvLoginNow = rootView.findViewById(R.id.tvLoginNow);
            tvLoginNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(getContext(), LoginActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    getActivity().finish();
                }
            });
            Button buttonRegister = rootView.findViewById(R.id.buttonRegister);
            buttonRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent i = new Intent(getContext(), RegisterActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    getActivity().finish();

                }
            });

        } else {

            TextView tvName = rootView.findViewById(R.id.tvName);
            tvName.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

            TextView tvAddress = rootView.findViewById(R.id.tvAddress);
            tvAddress.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

            TextView tvEmail = rootView.findViewById(R.id.tvEmail);
            tvEmail.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

            TextView tvPincode = rootView.findViewById(R.id.tvPincode);
            tvPincode.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

            TextView tvPhoneNumber = rootView.findViewById(R.id.tvPhoneNumber);
            tvPhoneNumber.setTypeface(Typer.set(getContext()).getFont(Font.ROBOTO_REGULAR));

            ImageView ivEditProfile = rootView.findViewById(R.id.ivEditProfile);

            ImageView ivProfilePic = rootView.findViewById(R.id.ivProfilePic);


            tvName.setText(String.valueOf(MemberDetails.getFname() + " " + MemberDetails.getLname()));

            String temp = "Address  : " + MemberDetails.getAddress();
            SpannableString greenSpannable = new SpannableString(temp);
            greenSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#439e47")), 0, 11, 0);
            tvAddress.setText(greenSpannable);

            if (!MemberDetails.getEmail().equals("")) {
                temp = "Email ID : " + MemberDetails.getEmail();
                greenSpannable = new SpannableString(temp);
                greenSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#439e47")), 0, 11, 0);
                tvEmail.setText(greenSpannable);
            } else {
                tvEmail.setVisibility(View.GONE);
            }


            temp = "Pin Code : " + MemberDetails.getPincode();
            greenSpannable = new SpannableString(temp);
            greenSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#439e47")), 0, 11, 0);
            tvPincode.setText(greenSpannable);

            temp = "Mobile No. : " + MemberDetails.getMobileNumber();
            greenSpannable = new SpannableString(temp);
            greenSpannable.setSpan(new ForegroundColorSpan(Color.parseColor("#439e47")), 0, 12, 0);
            tvPhoneNumber.setText(greenSpannable);

            Glide.get(getActivity()).clearMemory();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Drawable drawable1 = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_edit_profile, null);

                VectorDrawable vectorDrawable1 = (VectorDrawable) drawable1;

                ivEditProfile.setImageDrawable(vectorDrawable1);

            } else {
                ivEditProfile.setImageResource(R.drawable.ic_edit_profile);
            }

            ivEditProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(Master.isNetworkAvailable(getContext())){
                        onButtonPressed();
                    }else {
                        // toast of no internet connection
                        Toast.makeText(getContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                        NoInternetConnectionListener listener = (NoInternetConnectionListener) getContext();
                        listener.showNoInternetConnectionLayout();
                    }

                }
            });

            Glide.with(this).load(R.drawable.profile_pic)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(ivProfilePic);

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        DBHelper dbHelper;
        dbHelper = DBHelper.getInstance(getContext());
        if(dbHelper==null)dbHelper= DBHelper.getInstance(getActivity());
        dbHelper.getSignedInProfile();



        if(MemberDetails.getMobileNumber().equals("guest"))
        {
            if(svProfile != null){
                svProfile.setVisibility(View.GONE);
                rlGuest.setVisibility(View.VISIBLE);
            }

        } else {

            if(svProfile != null){
                svProfile.setVisibility(View.VISIBLE);
                rlGuest.setVisibility(View.GONE);
            }
        }

        getActivity().invalidateOptionsMenu();

    }

    private void onButtonPressed() {
        if (mListener != null) {
            mListener.onProfileFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnProfileFragmentInteractionListener) {
            mListener = (OnProfileFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnProfileFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnProfileFragmentInteractionListener {
        void onProfileFragmentInteraction();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

}
