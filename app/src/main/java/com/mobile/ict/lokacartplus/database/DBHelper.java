package com.mobile.ict.lokacartplus.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import com.mobile.ict.lokacartplus.model.ProductDetail;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import java.util.ArrayList;


public class DBHelper extends SQLiteOpenHelper {

    private static DBHelper dbHelperInstance;
    private static final int DATABASE_VERSION = 3;
    private static final String DATABASE_NAME = "lokacartplus.db";
    private static final String PROFILE_TABLE = "profile";
    private static final String CART_TABLE = "cart";

    public final class ProfileDataEntry implements BaseColumns {

        public static final String COLUMN_MOBILE_NUMBER = Master.MOBILENUMBER;
        public static final String COLUMN_FNAME = Master.FNAME;
        public static final String COLUMN_LNAME = Master.LNAME;
        public static final String COLUMN_EMAIL = Master.EMAIL;
        public static final String COLUMN_ADDRESS = Master.ADDRESS;
        public static final String COLUMN_PINCODE = Master.PINCODE;
        public static final String COLUMN_LOGIN = Master.LOGIN;

    }

    public final class CartDataEntry implements BaseColumns {

        public static final String COLUMN_ORG_NAME = Master.ORG_NAME;
        public static final String COLUMN_PRODUCT_ID = Master.ID;
        public static final String COLUMN_PRODUCT_NAME = Master.PRODUCT_NAME;
        public static final String COLUMN_MOBILE_NUMBER = Master.MOBILENUMBER;
        public static final String COLUMN_UNIT_RATE = Master.UNIT_RATE;
        public static final String COLUMN_ITEM_TOTAL = Master.ITEM_TOTAL;
        public static final String COLUMN_QUANTITY = Master.QUANTITY;
        public static final String COLUMN_IMAGE_URL = Master.IMAGE_URL;
        public static final String COLUMN_PRODUCT_UNIT = Master.PRODUCT_UNIT;
        public static final String COLUMN_PRODUCT_MOQ = Master.MINIMUM_ORDER_QUANTITY;
        public static final String COLUMN_GST = Master.GST;

    }

    public static synchronized DBHelper getInstance(Context context) {
        if (dbHelperInstance == null) {
            dbHelperInstance = new DBHelper(context.getApplicationContext());
        }
        return dbHelperInstance;
    }

    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + PROFILE_TABLE + " (" +
                ProfileDataEntry.COLUMN_MOBILE_NUMBER + " TEXT ," +
                ProfileDataEntry.COLUMN_FNAME + " TEXT , " +
                ProfileDataEntry.COLUMN_LNAME + " TEXT ," +
                ProfileDataEntry.COLUMN_EMAIL + " TEXT  , " +
                ProfileDataEntry.COLUMN_ADDRESS + " TEXT ," +
                ProfileDataEntry.COLUMN_PINCODE + " TEXT ," +
                ProfileDataEntry.COLUMN_LOGIN + " TEXT " +
                ")");

        db.execSQL("CREATE TABLE " + CART_TABLE + " (" +
                CartDataEntry.COLUMN_MOBILE_NUMBER + " TEXT ," +
                CartDataEntry.COLUMN_ORG_NAME + " TEXT , " +
                CartDataEntry.COLUMN_PRODUCT_ID + " TEXT ," +
                CartDataEntry.COLUMN_PRODUCT_NAME + " TEXT ," +
                CartDataEntry.COLUMN_UNIT_RATE + " TEXT  , " +
                CartDataEntry.COLUMN_ITEM_TOTAL + " TEXT ," +
                CartDataEntry.COLUMN_QUANTITY + " TEXT ," +
                CartDataEntry.COLUMN_IMAGE_URL + " TEXT ," +
                CartDataEntry.COLUMN_PRODUCT_UNIT + " TEXT ," +
                CartDataEntry.COLUMN_PRODUCT_MOQ + " TEXT , " +
                CartDataEntry.COLUMN_GST + " TEXT " +
                ")");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + PROFILE_TABLE);
        db.execSQL(" DROP TABLE IF EXISTS " + CART_TABLE);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public void addProduct(String unitPrice, String quantity, String itemTotal, String productName, String mobileNumber,
                          String orgName, String id, String imageURL, String unit, String moq,String gst) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor id_cursor = db.rawQuery("select * from " + CART_TABLE + " where " + CartDataEntry.COLUMN_PRODUCT_ID + "=" + "\"" + id + "\""
                        + " and " + CartDataEntry.COLUMN_MOBILE_NUMBER + " = " + "\"" + mobileNumber + "\""
                        + " and " + CartDataEntry.COLUMN_ORG_NAME + " = " + "\"" + orgName + "\"", null);

        if (id_cursor.getCount() == 0) {
            ContentValues values = new ContentValues();

            values.put(CartDataEntry.COLUMN_MOBILE_NUMBER, mobileNumber);
            values.put(CartDataEntry.COLUMN_PRODUCT_ID, id);
            values.put(CartDataEntry.COLUMN_PRODUCT_NAME, productName);
            values.put(CartDataEntry.COLUMN_UNIT_RATE, unitPrice);
            values.put(CartDataEntry.COLUMN_ITEM_TOTAL, itemTotal);
            values.put(CartDataEntry.COLUMN_ORG_NAME, orgName);
            values.put(CartDataEntry.COLUMN_QUANTITY, quantity);
            values.put(CartDataEntry.COLUMN_IMAGE_URL, imageURL);
            values.put(CartDataEntry.COLUMN_PRODUCT_UNIT, unit);
            values.put(CartDataEntry.COLUMN_PRODUCT_MOQ, moq);
            values.put(CartDataEntry.COLUMN_GST, gst);

            db.insert(CART_TABLE, null, values);

            Master.CART_ITEM_COUNT++;
            id_cursor.close();

            if (db.isOpen())
                db.close();


        } else {
            id_cursor.moveToNext();

            int qty = Integer.parseInt(quantity);

            int total = (int) (qty * Double.parseDouble(unitPrice));

            String WHERE = CartDataEntry.COLUMN_ORG_NAME + "= ?" + " and "
                    + CartDataEntry.COLUMN_PRODUCT_ID + "= ?" + " and "
                    + CartDataEntry.COLUMN_MOBILE_NUMBER + "= ?";

            ContentValues values = new ContentValues();
            values.put(CartDataEntry.COLUMN_MOBILE_NUMBER, mobileNumber);
            values.put(CartDataEntry.COLUMN_PRODUCT_ID, id);
            values.put(CartDataEntry.COLUMN_PRODUCT_NAME, productName);
            values.put(CartDataEntry.COLUMN_UNIT_RATE, unitPrice);
            values.put(CartDataEntry.COLUMN_ITEM_TOTAL, total + "");
            values.put(CartDataEntry.COLUMN_ORG_NAME, orgName);
            values.put(CartDataEntry.COLUMN_QUANTITY, qty + "");
            values.put(CartDataEntry.COLUMN_IMAGE_URL, imageURL);
            values.put(CartDataEntry.COLUMN_PRODUCT_UNIT, unit);
            values.put(CartDataEntry.COLUMN_PRODUCT_MOQ, moq);
            values.put(CartDataEntry.COLUMN_GST, gst);

            db.update(CART_TABLE, values, WHERE, new String[]{orgName, id, mobileNumber});
            id_cursor.close();

            if (db.isOpen())
                db.close();

        }
    }

    public void updateProduct(String unitPrice, String quantity, String itemTotal, String productName, String mobileNumber,
                              String orgName, String id, String imageURL, String unit, String moq,String gst) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor id_cursor = db.rawQuery("select * from " + CART_TABLE + " where "
                        + CartDataEntry.COLUMN_PRODUCT_ID + "=" + "\"" + id + "\""
                        + " and " + CartDataEntry.COLUMN_ORG_NAME + " = " + "\"" + orgName + "\"", null);

        if (id_cursor.getCount() != 0) {

            ContentValues values = new ContentValues();

            values.put(CartDataEntry.COLUMN_MOBILE_NUMBER, mobileNumber);
            values.put(CartDataEntry.COLUMN_PRODUCT_ID, id);
            values.put(CartDataEntry.COLUMN_PRODUCT_NAME, productName);
            values.put(CartDataEntry.COLUMN_UNIT_RATE, unitPrice);
            values.put(CartDataEntry.COLUMN_QUANTITY, quantity);
            values.put(CartDataEntry.COLUMN_ITEM_TOTAL, itemTotal);
            values.put(CartDataEntry.COLUMN_ORG_NAME, orgName);
            values.put(CartDataEntry.COLUMN_IMAGE_URL, imageURL);
            values.put(CartDataEntry.COLUMN_PRODUCT_UNIT, unit);
            values.put(CartDataEntry.COLUMN_PRODUCT_MOQ, moq);
            values.put(CartDataEntry.COLUMN_GST, gst);

            String WHERE = CartDataEntry.COLUMN_ORG_NAME + "= ?" + " and "
                    + CartDataEntry.COLUMN_PRODUCT_ID + "= ?" + " and " + CartDataEntry.COLUMN_MOBILE_NUMBER + "= ?";

            db.update(CART_TABLE, values, WHERE, new String[]{orgName, id, mobileNumber});

            id_cursor.close();

            if (db.isOpen()) db.close();
        }
    }

    public void addProfile() {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        db.execSQL("delete from " + PROFILE_TABLE);
        db.execSQL("VACUUM");

        values.put(ProfileDataEntry.COLUMN_MOBILE_NUMBER, MemberDetails.getMobileNumber());
        values.put(ProfileDataEntry.COLUMN_FNAME, MemberDetails.getFname());
        values.put(ProfileDataEntry.COLUMN_LNAME, MemberDetails.getLname());
        values.put(ProfileDataEntry.COLUMN_EMAIL, MemberDetails.getEmail());
        values.put(ProfileDataEntry.COLUMN_ADDRESS, MemberDetails.getAddress());
        values.put(ProfileDataEntry.COLUMN_PINCODE, MemberDetails.getPincode());
        values.put(ProfileDataEntry.COLUMN_LOGIN, "true");
        db.insert(PROFILE_TABLE, null, values);

        if (db.isOpen())
            db.close();

    }

    public void deleteProfile() {

        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from " + PROFILE_TABLE);
        db.execSQL("VACUUM");

        if (db.isOpen())
            db.close();

    }

    public void deleteProduct(String mobileNumber, String ID) {

        SQLiteDatabase db = this.getWritableDatabase();

        int deletedRows = db.delete(CART_TABLE,
                CartDataEntry.COLUMN_MOBILE_NUMBER + "= ?" + " AND " + CartDataEntry.COLUMN_PRODUCT_ID + "=  ?",
                new String[]{mobileNumber, ID});

        if(deletedRows > 0){
            Master.CART_ITEM_COUNT--;
        }

        if (db.isOpen())
            db.close();
    }

    public void deleteCart(String mobileNumber) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(CART_TABLE,
                CartDataEntry.COLUMN_MOBILE_NUMBER + "= ?" ,
                new String[]{mobileNumber});

        Master.CART_ITEM_COUNT = 0;

        if (db.isOpen())
            db.close();
    }


    public void getSignedInProfile() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor id_cursor =
                db.rawQuery("select * from " + PROFILE_TABLE + " where "
                        + ProfileDataEntry.COLUMN_LOGIN + "=" + "\"" + "true" + "\"", null);

        if (id_cursor.moveToNext()) {
            MemberDetails.setMobileNumber(id_cursor.getString(0));
            MemberDetails.setEmail(id_cursor.getString(3));
            MemberDetails.setFname(id_cursor.getString(1));
            MemberDetails.setLname(id_cursor.getString(2));
            MemberDetails.setAddress(id_cursor.getString(4));
            MemberDetails.setPincode(id_cursor.getString(5));
            id_cursor.close();
            if (db.isOpen())
                db.close();
        } else {
            id_cursor.close();
            if (db.isOpen()) db.close();
        }
    }

    public void getProfile() {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor id_cursor =
                db.rawQuery("select * from " + PROFILE_TABLE, null);

        if (id_cursor.moveToNext()) {
            MemberDetails.setMobileNumber(id_cursor.getString(0));
            MemberDetails.setEmail(id_cursor.getString(3));
            MemberDetails.setFname(id_cursor.getString(1));
            MemberDetails.setLname(id_cursor.getString(2));
            MemberDetails.setAddress(id_cursor.getString(4));
            MemberDetails.setPincode(id_cursor.getString(5));

            id_cursor.close();
            if (db.isOpen())
                db.close();

        } else {
            id_cursor.close();
            if (db.isOpen())
                db.close();

        }
    }

    public void updateProfile(String mobileNumber) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(ProfileDataEntry.COLUMN_FNAME, MemberDetails.getFname());
        values.put(ProfileDataEntry.COLUMN_LNAME, MemberDetails.getLname());
        values.put(ProfileDataEntry.COLUMN_ADDRESS, MemberDetails.getAddress());
        values.put(ProfileDataEntry.COLUMN_PINCODE, MemberDetails.getPincode());
        values.put(ProfileDataEntry.COLUMN_EMAIL, MemberDetails.getEmail());
        values.put(ProfileDataEntry.COLUMN_LOGIN, "true");

        String WHERE = ProfileDataEntry.COLUMN_MOBILE_NUMBER + "= ?";

        db.update(PROFILE_TABLE, values, WHERE, new String[]{mobileNumber});

        if (db.isOpen()) db.close();

    }

    public void changeMobileNumberCart(String oldNumber, String newNumber) {

        SQLiteDatabase db = this.getWritableDatabase();

            Cursor temp = db.rawQuery("select * from " + CART_TABLE + " where " + CartDataEntry.COLUMN_MOBILE_NUMBER + " = " + "\"" + oldNumber + "\"", null);

            if (temp.getCount() != 0) {

                ContentValues values1 = new ContentValues();

                values1.put(CartDataEntry.COLUMN_MOBILE_NUMBER, newNumber);

                String WHERE1 = CartDataEntry.COLUMN_MOBILE_NUMBER + "= ?";

                db.update(CART_TABLE, values1, WHERE1, new String[]{oldNumber});
            }

            temp.close();
            if (db.isOpen())
                db.close();

        if (db.isOpen())
            db.close();
    }

    public ArrayList<ProductDetail> getCartDetails(String mobileNumber) {
        ArrayList<ProductDetail> product_Detail_list = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("select * from " + CART_TABLE + " where " + CartDataEntry.COLUMN_MOBILE_NUMBER + " = " + "\"" + mobileNumber + "\"", null);

        while (cursor.moveToNext()) {
            ProductDetail temp = new ProductDetail();

            temp.setOrganization(cursor.getString(1));
            temp.setId(cursor.getString(2));
            temp.setName(cursor.getString(3));
            temp.setUnitRate(Double.parseDouble(cursor.getString(4)));
            temp.setItemTotal(Double.parseDouble(cursor.getString(5)));
            temp.setQuantity(Integer.parseInt(cursor.getString(6)));
            temp.setImageUrl(cursor.getString(7));
            temp.setUnit(cursor.getString(8));
            temp.setMoq(Integer.parseInt(cursor.getString(9)));
            temp.setGst(Integer.parseInt(cursor.getString(10)));

            product_Detail_list.add(temp);
        }
        cursor.close();

        if (db.isOpen())
            db.close();

        return product_Detail_list;
    }

    public int getCartQuantity(String mobileNumber, String id) {
       int cartQuantity = 0;

        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery("select * from " + CART_TABLE + " where " + CartDataEntry.COLUMN_MOBILE_NUMBER + " = " + "\"" + mobileNumber + "\""
                + " and " + CartDataEntry.COLUMN_PRODUCT_ID + " = " + "\"" + id + "\"", null);

        while (cursor.moveToNext()) {
            cartQuantity = (Integer.parseInt(cursor.getString(6)));
        }
        cursor.close();

        if (db.isOpen())
            db.close();

        return cartQuantity;
    }

    public String[] getCartProductIds(String mobileNumber) {

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from " + CART_TABLE + " where " + CartDataEntry.COLUMN_MOBILE_NUMBER + " = " + "\"" + mobileNumber + "\"", null);

        String[] ids = new String[cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()) {
            ids[i] = (cursor.getString(2));
            i++;
        }
        cursor.close();

        if (db.isOpen())
            db.close();

        return ids;
    }


    public int getCartItemsCount(String mobileNumber) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor id_cursor = db.rawQuery("select * from " + CART_TABLE + " where " + CartDataEntry.COLUMN_MOBILE_NUMBER + " = " + "\"" + mobileNumber + "\"", null);

        if (id_cursor.getCount() != 0) {
            int count = id_cursor.getCount();

            id_cursor.close();

            if (db.isOpen())
                db.close();

            return count;
        }

        id_cursor.close();

        if (db.isOpen())
            db.close();

        return 0;

    }

}

