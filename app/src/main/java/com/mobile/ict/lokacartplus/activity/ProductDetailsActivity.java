package com.mobile.ict.lokacartplus.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

public class ProductDetailsActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private TextView tvMoq;
    private TextView tvUnit;
    private TextView tvPrice;
    private int[] layouts, ids;
    private ImageButton minusButton;
    private int position = 0;
    private DBHelper dbHelper;
    /*private EditTextFocusChangeListener editTextFocusChangeListener;
    private EditTextActionDoneListener editTextActionDoneListener;*/
    private TextView etKgs;
    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        setTitle("");

        mContext = this;

        Toolbar toolbar = findViewById(R.id.product_detail_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);

        viewPager = findViewById(R.id.view_pager);

        Bundle b = getIntent().getExtras();
        position = b.getInt("position");

        TextView tvProductName = findViewById(R.id.tvProductName);
        tvProductName.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        /*editTextFocusChangeListener = new EditTextFocusChangeListener();
        editTextActionDoneListener = new EditTextActionDoneListener();*/
        etKgs = findViewById(R.id.etKgs);
        etKgs.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        tvUnit = findViewById(R.id.tvUnit);
        tvUnit.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        tvPrice = findViewById(R.id.tvPrice);
        tvPrice.setTypeface(Typer.set(this).getFont(Font.ROBOTO_MEDIUM));

        TextView tvOrgName = findViewById(R.id.tvOrgName);
        tvOrgName.setTypeface(Typer.set(this).getFont(Font.ROBOTO_LIGHT));

        TextView tvProductDescription = findViewById(R.id.tvProductDescription);
        tvProductDescription.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        TextView tvAboutProduct = findViewById(R.id.tvAboutProduct);
        tvAboutProduct.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        TextView tvProductQuality = findViewById(R.id.tvProductQuality);
        tvProductQuality.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        TextView tvProductQualityDescription = findViewById(R.id.tvProductQualityDescription);
        tvProductQualityDescription.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        TextView tvLogisticSupportDescription = findViewById(R.id.tvLogisticSupportDescription);
        tvLogisticSupportDescription.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        tvMoq = findViewById(R.id.tvMoq);
        tvMoq.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        minusButton = findViewById(R.id.buttonMinus);
        ImageButton plusButton = findViewById(R.id.buttonPlus);

        tvProductName.setText(Master.productDetailList.get(position).getName());
        tvPrice.setText(Html.fromHtml("1 " + Master.productDetailList.get(position).getUnit() + " for \u20B9 " + "<font color=#439e47>" + Master.productDetailList.get(position).getUnitRate() + "</font>"));
        tvOrgName.setText(Master.productDetailList.get(position).getOrganization());

        tvProductDescription.setText(Master.productDetailList.get(position).getDescription());
        tvLogisticSupportDescription.setText(Master.productDetailList.get(position).getLogistics());

        if (!Master.productDetailList.get(position).getProdQuality().equals("")) {
            tvProductQuality.setVisibility(View.VISIBLE);
            tvProductQualityDescription.setVisibility(View.VISIBLE);
            tvProductQualityDescription.setText(Master.productDetailList.get(position).getProdQuality());
        } else {
            tvProductQuality.setVisibility(View.GONE);
            tvProductQualityDescription.setVisibility(View.GONE);
        }

        String msg = "Min. Order - " + Master.productDetailList.get(position).getMoq() + " " + Master.productDetailList.get(position).getUnit();
        tvMoq.setText(msg);
        tvUnit.setText(String.valueOf(Master.productDetailList.get(position).getUnit()));
        etKgs.setText(String.valueOf(Master.productDetailList.get(position).getQuantity()));

        /*etKgs.setOnFocusChangeListener(editTextFocusChangeListener);
        editTextFocusChangeListener.updatePosition(position, etKgs,tvUnit, tvMoq, minusButton, this);

        etKgs.setOnEditorActionListener(editTextActionDoneListener);
        editTextActionDoneListener.updatePosition(position, etKgs,tvUnit, tvMoq, minusButton, this);*/

        etKgs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditEtKgsDialog();
            }
        });

        tvUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showEditEtKgsDialog();
            }
        });

        TextWatcher etKgsTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                DBHelper dbHelper = DBHelper.getInstance(mContext);

                if (!etKgs.getText().toString().equals("")) {

                    try {
                        minusButton.setEnabled(true);
                        int qty = Integer.parseInt(etKgs.getText().toString());

                        if (qty == 0) {
                            tvUnit.setVisibility(View.GONE);
                            tvMoq.setVisibility(View.GONE);
                            etKgs.setVisibility(View.GONE);
                            minusButton.setVisibility(View.GONE);
                            Master.productDetailList.get(position).setQuantity(0);
                            //delete product
                            dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.productDetailList.get(position).getId());
                            //invalidateOptionsMenu();
                            ((Activity) mContext).invalidateOptionsMenu();

                        } else if (qty < Master.productDetailList.get(position).getMoq()) {

                            Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                            etKgs.setText(String.valueOf(Master.productDetailList.get(position).getMoq()));
                            Master.productDetailList.get(position).setQuantity(Master.productDetailList.get(position).getMoq());
                            dbHelper.updateProduct(
                                    String.valueOf(Master.productDetailList.get(position).getUnitRate()),
                                    String.valueOf(Master.productDetailList.get(position).getMoq()),
                                    String.valueOf(Master.productDetailList.get(position).getUnitRate()*Master.productDetailList.get(position).getMoq()),
                                    String.valueOf(Master.productDetailList.get(position).getName()),
                                    String.valueOf(MemberDetails.getMobileNumber()),
                                    String.valueOf(Master.productDetailList.get(position).getOrganization()),
                                    String.valueOf(Master.productDetailList.get(position).getId()),
                                    String.valueOf(Master.productDetailList.get(position).getImageUrl()),
                                    String.valueOf(Master.productDetailList.get(position).getUnit()),
                                    String.valueOf(Master.productDetailList.get(position).getMoq()),
                                    String.valueOf(Master.productDetailList.get(position).getGst())

                            );

                        } else {
                            Master.productDetailList.get(position).setQuantity(qty);
                            dbHelper.updateProduct(
                                    String.valueOf(Master.productDetailList.get(position).getUnitRate()),
                                    String.valueOf(qty),
                                    String.valueOf(Master.productDetailList.get(position).getUnitRate()*qty),
                                    String.valueOf(Master.productDetailList.get(position).getName()),
                                    String.valueOf(MemberDetails.getMobileNumber()),
                                    String.valueOf(Master.productDetailList.get(position).getOrganization()),
                                    String.valueOf(Master.productDetailList.get(position).getId()),
                                    String.valueOf(Master.productDetailList.get(position).getImageUrl()),
                                    String.valueOf(Master.productDetailList.get(position).getUnit()),
                                    String.valueOf(Master.productDetailList.get(position).getMoq()),
                                    String.valueOf(Master.productDetailList.get(position).getGst())

                            );
                        }

                    } catch (NumberFormatException ignored) {
                    }
                } else {

                    tvUnit.setVisibility(View.GONE);
                    tvMoq.setVisibility(View.GONE);
                    etKgs.setVisibility(View.GONE);
                    minusButton.setVisibility(View.GONE);
                    Master.productDetailList.get(position).setQuantity(0);
                    //delete product
                    dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.productDetailList.get(position).getId());
                    //invalidateOptionsMenu();
                    ((Activity) mContext).invalidateOptionsMenu();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        etKgs.addTextChangedListener(etKgsTextWatcher);

        if (Master.productDetailList.get(position).getDescription().equals("")) {
            tvAboutProduct.setVisibility(View.GONE);
            tvProductDescription.setVisibility(View.GONE);
        }

        if (Master.productDetailList.get(position).getQuantity() == 0) {
            tvMoq.setVisibility(View.INVISIBLE);
            etKgs.setVisibility(View.GONE);
            minusButton.setVisibility(View.GONE);
            tvUnit.setVisibility(View.GONE);
        }

        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tvMoq.setVisibility(View.VISIBLE);
                etKgs.setVisibility(View.VISIBLE);
                minusButton.setVisibility(View.VISIBLE);
                tvUnit.setVisibility(View.VISIBLE);

                dbHelper = DBHelper.getInstance(getApplicationContext());
                int qty = Master.productDetailList.get(position).getQuantity();
                if (qty == 0) {
                    qty = Master.productDetailList.get(position).getMoq();
                    Master.productDetailList.get(position).setQuantity(qty);
                    dbHelper.addProduct(
                            String.valueOf(Master.productDetailList.get(position).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.productDetailList.get(position).getUnitRate() * qty),
                            String.valueOf(Master.productDetailList.get(position).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.productDetailList.get(position).getOrganization()),
                            String.valueOf(Master.productDetailList.get(position).getId()),
                            String.valueOf(Master.productDetailList.get(position).getImageUrl()),
                            String.valueOf(Master.productDetailList.get(position).getUnit()),
                            String.valueOf(Master.productDetailList.get(position).getMoq()),
                            String.valueOf(Master.productDetailList.get(position).getGst())
                    );
                    invalidateOptionsMenu();
                } else {
                    qty++;
                    Master.productDetailList.get(position).setQuantity(qty);
                    dbHelper.updateProduct(
                            String.valueOf(Master.productDetailList.get(position).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.productDetailList.get(position).getUnitRate() * qty),
                            String.valueOf(Master.productDetailList.get(position).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.productDetailList.get(position).getOrganization()),
                            String.valueOf(Master.productDetailList.get(position).getId()),
                            String.valueOf(Master.productDetailList.get(position).getImageUrl()),
                            String.valueOf(Master.productDetailList.get(position).getUnit()),
                            String.valueOf(Master.productDetailList.get(position).getMoq()),
                            String.valueOf(Master.productDetailList.get(position).getGst())
                    );
                }

                etKgs.setText(String.valueOf(qty));

            }
        });

        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dbHelper = DBHelper.getInstance(getApplicationContext());
                int qty = Master.productDetailList.get(position).getQuantity();
                qty--;

                if (qty < Master.productDetailList.get(position).getMoq()) {
                    tvMoq.setVisibility(View.INVISIBLE);
                    etKgs.setVisibility(View.GONE);
                    minusButton.setVisibility(View.GONE);
                    tvUnit.setVisibility(View.GONE);
                    Master.productDetailList.get(position).setQuantity(0);
                    //delete product
                    dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.productDetailList.get(position).getId());
                    invalidateOptionsMenu();
                } else {
                    Master.productDetailList.get(position).setQuantity(qty);
                    dbHelper.updateProduct(
                            String.valueOf(Master.productDetailList.get(position).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.productDetailList.get(position).getUnitRate() * qty),
                            String.valueOf(Master.productDetailList.get(position).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.productDetailList.get(position).getOrganization()),
                            String.valueOf(Master.productDetailList.get(position).getId()),
                            String.valueOf(Master.productDetailList.get(position).getImageUrl()),
                            String.valueOf(Master.productDetailList.get(position).getUnit()),
                            String.valueOf(Master.productDetailList.get(position).getMoq()),
                            String.valueOf(Master.productDetailList.get(position).getGst())
                    );
                    etKgs.setText(String.valueOf(qty));
                }

            }
        });


        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.product_imageview_slide1
        };

        ids = new int[]{
                R.id.ivProduct1
        };

        // making notification bar transparent
        changeStatusBarColor();

        MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        invalidateOptionsMenu();
    }

    void showEditEtKgsDialog() {
        LayoutInflater li = LayoutInflater.from(this);
        View dialogView = li.inflate(R.layout.edit_kgs_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        alertDialogBuilder.setTitle("Enter Quantity");
        alertDialogBuilder.setView(dialogView);
        final EditText userInput = dialogView.findViewById(R.id.editTextQty);
        userInput.setText(etKgs.getText().toString());

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                etKgs.setText(userInput.getText());
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    @Override
    public boolean onSupportNavigateUp() {
        Master.isFromCart = true;
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Master.isFromCart = true;
        finish();
    }

    //	viewpager change listener
    private final ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    /**
     * View pager adapter
     */
    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int positions) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[positions], container, false);
            container.addView(view);

            ImageView ivProduct = view.findViewById(ids[positions]);


            Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_lc_plus_icon, null);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Glide.with(getApplication()).load(Master.productDetailList.get(position).getImageUrl())
                        .crossFade()
                        .placeholder(R.drawable.ic_lc_plus_icon)
                        .fitCenter()
                        .diskCacheStrategy(DiskCacheStrategy.ALL).into(ivProduct);
            } else {
                Glide.with(getApplication()).load(Master.productDetailList.get(position).getImageUrl()).thumbnail(0.5f)
                        .crossFade()
                        .placeholder(drawable)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL).into(ivProduct);
            }

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_product_details, menu);

        MenuItem item = menu.findItem(R.id.action_cart);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        Master.setBadgeCount(this, icon, Master.CART_ITEM_COUNT);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_cart) {
            Intent i = new Intent(ProductDetailsActivity.this, CartActivity.class);
            startActivity(i);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (dbHelper == null) dbHelper = DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();

        String msg = "Min. Order - " + Master.productDetailList.get(position).getMoq() + " " + Master.productDetailList.get(position).getUnit();
        tvMoq.setText(msg);
        tvUnit.setText(String.valueOf(Master.productDetailList.get(position).getUnit()));
        etKgs.setText(String.valueOf(Master.productDetailList.get(position).getQuantity()));
        tvPrice.setText(Html.fromHtml("1 " + Master.productDetailList.get(position).getUnit() + " for \u20B9 " + "<font color=#439e47>" + Master.productDetailList.get(position).getUnitRate() + "</font>"));

        invalidateOptionsMenu();

    }
}
