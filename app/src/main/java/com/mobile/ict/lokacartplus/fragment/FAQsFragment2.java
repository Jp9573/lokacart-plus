package com.mobile.ict.lokacartplus.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.activity.FAQsActivity;

public class FAQsFragment2 extends Fragment {

    private int position;
    private View rootView;

    public FAQsFragment2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt("position");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_faqs2, container, false);

        // This is used for implementing back button
        ((FAQsActivity)getActivity()).changeToolBar();

        //code in onActivityCreated was here...

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FAQsActivity.navItem = 2;

        CharSequence[] FAQsCategory = getResources().getTextArray(R.array.category_faqs_titles);
        ((FAQsActivity) getActivity()).tvHeader.setText(FAQsCategory[position]);

        TextView tvSubCategory = rootView.findViewById(R.id.tvSubCategory);
        tvSubCategory.setTypeface(Typer.set(rootView.getContext()).getFont(Font.ROBOTO_REGULAR));
        tvSubCategory.setMovementMethod(new ScrollingMovementMethod());

        CharSequence[] SubCategory = getResources().getTextArray(R.array.category_faqs_content);

        tvSubCategory.setText(SubCategory[position]);
    }

}
