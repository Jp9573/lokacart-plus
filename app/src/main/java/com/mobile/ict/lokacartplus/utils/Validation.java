package com.mobile.ict.lokacartplus.utils;

import java.util.regex.Pattern;

/**
 * Created by Toshiba on 9/3/17.
 */

@SuppressWarnings("DefaultFileTemplate")
public class Validation {

    public static boolean isValidEmail(String target) {
        return !target.isEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isPasswordMatching(String password, String confirmPassword) {
        return !password.isEmpty() && !confirmPassword.isEmpty() && password.equals(confirmPassword);
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        return (phoneNumber.length() == 10 && ("+91" + phoneNumber).matches("(^)(?:\\+91)([\\d]){10}$"));
    }

    public static boolean isValidPincode(String pincode) {
        return (pincode.length() == 6);
    }

    public static boolean isNull(String string) {
        return string.isEmpty() || string.equals("") || string.equals(null);
    }

    public static boolean isValidPassword(String password) {
        // min 1 LC, min 1 UC, min 1 digit, min 1 spl char, min 8 and max 32 chars
        //return Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%_*&!]).{8,32})").matcher(password).matches();

        // min 8 and max 32 chars
        return Pattern.compile("[A-Za-z\\d$@!%*#?&]{8,32}$").matcher(password).matches();
    }

}
