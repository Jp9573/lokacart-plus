package com.mobile.ict.lokacartplus.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.bumptech.glide.Glide;
import com.mobile.ict.lokacartplus.activity.FeedbackActivity;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.utils.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.utils.Validation;
import com.mobile.ict.lokacartplus.activity.CartActivity;
import com.mobile.ict.lokacartplus.activity.DashboardActivity;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterFragment2 extends Fragment {

    private String EnterPhoneNumber;
    private String EnterPassword;

    private EditText etFirstName, etLastName, etAddress, etPincode, etEmail;
    private NetworkCommunicator networkCommunicator;
    private View rootView;

    public RegisterFragment2() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            EnterPassword = getArguments().getString("EnterPassword");
            EnterPhoneNumber = getArguments().getString("EnterPhoneNumber");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_register2, container, false);

        //code in onActivityCreated was here...

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        etFirstName = rootView.findViewById(R.id.etFirstName);
        etLastName = rootView.findViewById(R.id.etLastName);
        etAddress = rootView.findViewById(R.id.etAddress);
        etPincode = rootView.findViewById(R.id.etPincode);
        etEmail = rootView.findViewById(R.id.etEmail);
        networkCommunicator = NetworkCommunicator.getInstance(/*getContext()*/);

        Button submitButton = rootView.findViewById(R.id.submitButton);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validation()){
                    if(Master.isNetworkAvailable(getContext())){
                        Master.showProgressDialog(getContext(), getString(R.string.pdialog_loading));
                        makeJsonObjReq();
                    }else {
                        Toast.makeText(getContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                    }
                }

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(networkCommunicator==null) {
            networkCommunicator = NetworkCommunicator.getInstance();
        }
    }

    private void makeJsonObjReq() {
        Map<String, String> params = new HashMap<>();
        params.put(Master.PASSWORD, EnterPassword);
        params.put(Master.MOBILENUMBER, EnterPhoneNumber);
        params.put(Master.FNAME, etFirstName.getText().toString().trim());
        params.put(Master.LNAME, etLastName.getText().toString().trim());
        params.put(Master.ADDRESS, etAddress.getText().toString().trim());
        params.put(Master.EMAIL, etEmail.getText().toString().trim());
        params.put(Master.PINCODE, etPincode.getText().toString().trim());

        networkCommunicator.data(Master.getRegistrationFormURL(),
                Request.Method.POST,
                new JSONObject(params),
                false,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null) {
                            checkStatus(obj);
                        }

                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        Toast.makeText(getContext(), getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
                    }
                },Master.REGISTER2_FRAGMENT_TAG, getContext());

    }

    private void checkStatus(JSONObject response){

        try {
            if(response.getString("status").equals("success")){

                try {
                    new MemberDetails(etFirstName.getText().toString().trim(),etLastName.getText().toString().trim(), etAddress.getText().toString().trim(), etPincode.getText().toString().trim(), etEmail.getText().toString().trim(), EnterPhoneNumber);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                DBHelper dbHelper = DBHelper.getInstance(getContext());


                Glide.get(getContext()).clearMemory(); //clears memory of stepper activity images.

                SharedPreferenceConnector.writeBoolean(getContext(), Master.STEPPER, false);

                if(Master.REDIRECTED_FROM_CART){

                    dbHelper.changeMobileNumberCart("guest", EnterPhoneNumber);  //change mobile number in local db
                    dbHelper.addProfile();

                    Master.REDIRECTED_FROM_CART = false;

                    Intent i = new Intent(getContext(), CartActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    getActivity().finish();

                } else if(Master.REDIRECTED_FROM_FEEDBACK){

                    dbHelper.changeMobileNumberCart("guest", EnterPhoneNumber);  //change mobile number in local db
                    dbHelper.addProfile();

                    Master.REDIRECTED_FROM_FEEDBACK = false;

                    Intent i = new Intent(getContext(), FeedbackActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    getActivity().finish();

                } else {

                    dbHelper.addProfile();

                    Intent i = new Intent(getContext(), DashboardActivity.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    getActivity().finish();
                }
            }
            else {
                Toast.makeText(getContext(), response.getString("status"),
                        Toast.LENGTH_LONG).show();

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private boolean validation(){

        if(Validation.isNull(etFirstName.getText().toString().trim())){
            etFirstName.requestFocus();
            etFirstName.setError("First Name is required");
            return false;
        }

        if(Validation.isNull(etLastName.getText().toString().trim())){
            etLastName.requestFocus();
            etLastName.setError("Last Name is required");
            return false;
        }

        if(Validation.isNull(etAddress.getText().toString().trim())){
            etAddress.requestFocus();
            etAddress.setError("Address is required");
            return false;
        }

        if(!Validation.isValidPincode(etPincode.getText().toString().trim())){
            etPincode.requestFocus();
            etPincode.setError("Please enter valid pincode");
            return false;
        }

        if(!Validation.isValidEmail(etEmail.getText().toString().trim()) && !Validation.isNull(etEmail.getText().toString().trim())){
            etEmail.requestFocus();
            etEmail.setError("Please enter valid Email-id");
            return false;
        }

        return true;
    }


}
