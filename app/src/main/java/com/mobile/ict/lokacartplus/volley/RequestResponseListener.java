package com.mobile.ict.lokacartplus.volley;


import com.mobile.ict.lokacartplus.network.NetworkException;

public interface RequestResponseListener {

    interface Listener{
        <T> void onResponse(T response);
    }

    interface ErrorListener{
        void onError(NetworkException error);
    }

}
