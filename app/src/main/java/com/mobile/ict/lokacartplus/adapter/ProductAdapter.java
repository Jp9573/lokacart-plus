package com.mobile.ict.lokacartplus.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.activity.ProductDetailsActivity;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

/**
 * Created by Toshiba on 16-02-2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder>  {
    private final Context mContext;
    private DBHelper dbHelper;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder{
        // each data item is just a string in this case
        public final TextView tvProductName;
        public final TextView tvMoq;
        public final TextView tvPrice;
        public final TextView tvOrgName;
        public final TextView tvUnit;
        public final ImageView productImageView;
        public final ImageButton minusButton;
        public final ImageButton plusButton;
        public final View clickView;
        /*public final EditTextFocusChangeListener editTextFocusChangeListener;
        public final EditTextActionDoneListener editTextActionDoneListener;*/
        public final TextView etKgs;

        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout

            /*editTextFocusChangeListener = new EditTextFocusChangeListener();
            editTextActionDoneListener = new EditTextActionDoneListener();*/
            etKgs = v.findViewById(R.id.etKgs);
            etKgs.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvProductName = v.findViewById(R.id.tvProductName);
            tvProductName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvMoq = v.findViewById(R.id.tvMoq);
            tvMoq.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvUnit = v.findViewById(R.id.tvUnit);
            tvUnit.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvPrice = v.findViewById(R.id.tvPrice);
            tvPrice.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrgName = v.findViewById(R.id.tvOrgName);
            tvOrgName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_LIGHT));

            productImageView = v.findViewById(R.id.product_imageview);
            minusButton = v.findViewById(R.id.buttonMinus);
            plusButton = v.findViewById(R.id.buttonPlus);
            clickView = v.findViewById(R.id.clickView);

            clickView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        Bundle b = new Bundle();
                        b.putString("id", Master.productDetailList.get(getAdapterPosition()).getId());
                        b.putInt("position", getAdapterPosition());
                        b.putString("Kgs",String.valueOf(Master.productDetailList.get(getAdapterPosition()).getQuantity()));
                        Intent i = new Intent(view.getContext(), ProductDetailsActivity.class);
                        i.putExtras(b);
                        view.getContext().startActivity(i);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });

            productImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        Bundle b = new Bundle();
                        b.putString("id", Master.productDetailList.get(getAdapterPosition()).getId());
                        b.putInt("position", getAdapterPosition());
                        b.putString("Kgs",String.valueOf(Master.productDetailList.get(getAdapterPosition()).getQuantity()));
                        Intent i = new Intent(view.getContext(), ProductDetailsActivity.class);
                        i.putExtras(b);
                        view.getContext().startActivity(i);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });

        }

    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProductAdapter(Context context) {
        mContext = context;
    }

    @Override
    public ProductAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_product,parent,false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        try{
            if(Master.productDetailList.get(position).getQuantity()==0) {
                holder.tvMoq.setVisibility(View.GONE);
                holder.etKgs.setVisibility(View.GONE);
                holder.minusButton.setVisibility(View.GONE);
                holder.tvUnit.setVisibility(View.GONE);
            }

            holder.tvProductName.setText(Master.productDetailList.get(position).getName());
            holder.tvOrgName.setText(Master.productDetailList.get(position).getOrganization());
            holder.tvPrice.setText(Html.fromHtml("1 "+ Master.productDetailList.get(position).getUnit() + " for \u20B9 " + "<font color=#439e47>" + Master.productDetailList.get(position).getUnitRate() + "</font>"));
            String msg = "Min. Order - "+ Master.productDetailList.get(position).getMoq() + " " + Master.productDetailList.get(position).getUnit();
            holder.tvMoq.setText(msg);
            holder.tvUnit.setText(String.valueOf(Master.productDetailList.get(position).getUnit()));
            holder.etKgs.setText(String.valueOf(Master.productDetailList.get(position).getQuantity()));

            holder.etKgs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showEditEtKgsDialog(holder.etKgs);
                }
            });

            holder.tvUnit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showEditEtKgsDialog(holder.etKgs);
                }
            });

            TextWatcher etKgsTextWatcher = new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    DBHelper dbHelper = DBHelper.getInstance(mContext);

                    if (!holder.etKgs.getText().toString().equals("")) {

                        try {
                            holder.minusButton.setEnabled(true);
                            int qty = Integer.parseInt(holder.etKgs.getText().toString());

                            if (qty == 0) {
                                holder.tvUnit.setVisibility(View.GONE);
                                holder.tvMoq.setVisibility(View.GONE);
                                holder.etKgs.setVisibility(View.GONE);
                                holder.minusButton.setVisibility(View.GONE);
                                Master.productDetailList.get(position).setQuantity(0);
                                //delete product
                                dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.productDetailList.get(position).getId());
                                //invalidateOptionsMenu();
                                ((Activity) mContext).invalidateOptionsMenu();

                            } else if (qty < Master.productDetailList.get(position).getMoq()) {

                                Toast.makeText(mContext, "Sorry, Can't reduce quantity than min. order", Toast.LENGTH_SHORT).show();

                                holder.etKgs.setText(String.valueOf(Master.productDetailList.get(position).getMoq()));
                                Master.productDetailList.get(position).setQuantity(Master.productDetailList.get(position).getMoq());
                                dbHelper.updateProduct(
                                        String.valueOf(Master.productDetailList.get(position).getUnitRate()),
                                        String.valueOf(Master.productDetailList.get(position).getMoq()),
                                        String.valueOf(Master.productDetailList.get(position).getUnitRate()*Master.productDetailList.get(position).getMoq()),
                                        String.valueOf(Master.productDetailList.get(position).getName()),
                                        String.valueOf(MemberDetails.getMobileNumber()),
                                        String.valueOf(Master.productDetailList.get(position).getOrganization()),
                                        String.valueOf(Master.productDetailList.get(position).getId()),
                                        String.valueOf(Master.productDetailList.get(position).getImageUrl()),
                                        String.valueOf(Master.productDetailList.get(position).getUnit()),
                                        String.valueOf(Master.productDetailList.get(position).getMoq()),
                                        String.valueOf(Master.productDetailList.get(position).getGst())

                                );

                            } else {
                                Master.productDetailList.get(position).setQuantity(qty);
                                dbHelper.updateProduct(
                                        String.valueOf(Master.productDetailList.get(position).getUnitRate()),
                                        String.valueOf(qty),
                                        String.valueOf(Master.productDetailList.get(position).getUnitRate()*qty),
                                        String.valueOf(Master.productDetailList.get(position).getName()),
                                        String.valueOf(MemberDetails.getMobileNumber()),
                                        String.valueOf(Master.productDetailList.get(position).getOrganization()),
                                        String.valueOf(Master.productDetailList.get(position).getId()),
                                        String.valueOf(Master.productDetailList.get(position).getImageUrl()),
                                        String.valueOf(Master.productDetailList.get(position).getUnit()),
                                        String.valueOf(Master.productDetailList.get(position).getMoq()),
                                        String.valueOf(Master.productDetailList.get(position).getGst())
                                );
                            }

                        } catch (NumberFormatException ignored) {
                        }
                    } else {

                        holder.tvUnit.setVisibility(View.GONE);
                        holder.tvMoq.setVisibility(View.GONE);
                        holder.etKgs.setVisibility(View.GONE);
                        holder.minusButton.setVisibility(View.GONE);
                        Master.productDetailList.get(position).setQuantity(0);
                        //delete product
                        dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.productDetailList.get(position).getId());
                        //invalidateOptionsMenu();
                        ((Activity) mContext).invalidateOptionsMenu();
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            };

            holder.etKgs.addTextChangedListener(etKgsTextWatcher);

            /*holder.etKgs.setOnEditorActionListener(holder.editTextActionDoneListener);
            holder.editTextActionDoneListener.updatePosition(position, holder.etKgs,holder.tvUnit, holder.tvMoq, holder.minusButton, mContext);*/

            /*holder.etKgs.setOnFocusChangeListener(holder.editTextFocusChangeListener);
            holder.editTextFocusChangeListener.updatePosition(position, holder.etKgs,holder.tvUnit, holder.tvMoq, holder.minusButton, mContext);*/

        }catch (Exception e){
            e.printStackTrace();
        }

        Drawable drawable = ResourcesCompat.getDrawable(mContext.getResources(),R.mipmap.ic_launcher_icon,null);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            Glide.with(mContext).load(Master.productDetailList.get(position).getImageUrl()).thumbnail(0.5f)
                    .crossFade()
                    .placeholder(R.mipmap.ic_launcher_icon)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.productImageView);
        } else {
            Glide.with(mContext).load(Master.productDetailList.get(position).getImageUrl()).thumbnail(0.5f)
                    .crossFade()
                    .placeholder(drawable)
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(holder.productImageView);
        }

        holder.plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                holder.tvMoq.setVisibility(View.VISIBLE);
                holder.etKgs.setVisibility(View.VISIBLE);
                holder.minusButton.setVisibility(View.VISIBLE);
                holder.tvUnit.setVisibility(View.VISIBLE);

                dbHelper = DBHelper.getInstance(mContext);
                int qty =  Master.productDetailList.get(position).getQuantity();
                if(qty == 0){
                    qty = Master.productDetailList.get(position).getMoq();
                    Master.productDetailList.get(position).setQuantity(qty);
                    dbHelper.addProduct(
                            String.valueOf(Master.productDetailList.get(position).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.productDetailList.get(position).getUnitRate()*qty),
                            String.valueOf(Master.productDetailList.get(position).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.productDetailList.get(position).getOrganization()),
                            String.valueOf(Master.productDetailList.get(position).getId()),
                            String.valueOf(Master.productDetailList.get(position).getImageUrl()),
                            String.valueOf(Master.productDetailList.get(position).getUnit()),
                            String.valueOf(Master.productDetailList.get(position).getMoq()),
                            String.valueOf(Master.productDetailList.get(position).getGst())
                    );
                    ((Activity) mContext).invalidateOptionsMenu();
                } else {
                    qty++;
                    Master.productDetailList.get(position).setQuantity(qty);
                    dbHelper.updateProduct(
                            String.valueOf(Master.productDetailList.get(position).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.productDetailList.get(position).getUnitRate()*qty),
                            String.valueOf(Master.productDetailList.get(position).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.productDetailList.get(position).getOrganization()),
                            String.valueOf(Master.productDetailList.get(position).getId()),
                            String.valueOf(Master.productDetailList.get(position).getImageUrl()),
                            String.valueOf(Master.productDetailList.get(position).getUnit()),
                            String.valueOf(Master.productDetailList.get(position).getMoq()),
                            String.valueOf(Master.productDetailList.get(position).getGst())
                    );
                }

                holder.etKgs.setText(String.valueOf(qty));

            }
        });

        holder.minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dbHelper = DBHelper.getInstance(mContext);
                int qty =  Master.productDetailList.get(position).getQuantity();
                    qty--;

                if(qty<Master.productDetailList.get(position).getMoq()){
                    holder.tvMoq.setVisibility(View.GONE);
                    holder.etKgs.setVisibility(View.GONE);
                    holder.minusButton.setVisibility(View.GONE);
                    holder.tvUnit.setVisibility(View.GONE);
                    Master.productDetailList.get(position).setQuantity(0);
                    //delete product
                    dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.productDetailList.get(position).getId());
                    //invalidateOptionsMenu();
                    ((Activity) mContext).invalidateOptionsMenu();
                }else {
                    Master.productDetailList.get(position).setQuantity(qty);
                    dbHelper.updateProduct(
                            String.valueOf(Master.productDetailList.get(position).getUnitRate()),
                            String.valueOf(qty),
                            String.valueOf(Master.productDetailList.get(position).getUnitRate()*qty),
                            String.valueOf(Master.productDetailList.get(position).getName()),
                            String.valueOf(MemberDetails.getMobileNumber()),
                            String.valueOf(Master.productDetailList.get(position).getOrganization()),
                            String.valueOf(Master.productDetailList.get(position).getId()),
                            String.valueOf(Master.productDetailList.get(position).getImageUrl()),
                            String.valueOf(Master.productDetailList.get(position).getUnit()),
                            String.valueOf(Master.productDetailList.get(position).getMoq()),
                            String.valueOf(Master.productDetailList.get(position).getGst())
                    );
                    holder.etKgs.setText(String.valueOf(qty));
                }

            }
        });
    }

    void showEditEtKgsDialog(final TextView etkgs) {
        LayoutInflater li = LayoutInflater.from(mContext);
        View dialogView = li.inflate(R.layout.edit_kgs_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);

        alertDialogBuilder.setTitle("Enter Quantity");
        alertDialogBuilder.setView(dialogView);
        final EditText userInput = dialogView.findViewById(R.id.editTextQty);
        userInput.setText(etkgs.getText().toString());

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                etkgs.setText(userInput.getText());
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.setCancelable(true);
        alertDialog.show();
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return Master.productDetailList.size();
    }
}