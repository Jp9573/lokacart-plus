package com.mobile.ict.lokacartplus.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;

import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.model.ProductDetail;
import com.mobile.ict.lokacartplus.model.ProductTypeDetail;

import java.util.ArrayList;

/**
 * Created by Toshiba on 16-03-2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class Master {

    private static final String serverURL = "https://www.best-erp.com/ruralict/";

    //private static final String serverURL = "http://ruralict.cse.iitb.ac.in/ruralict/";

    public static String getLoginAPI() {
        return serverURL + "app/logincheck";
    }

    public static String getChangePasswordAPI() {
        return serverURL + "app/changelcppassword";
    }

    public static String getTypeWiseProductsAPI() {
        return serverURL + "api/products/search/byType/lcptypewiseproducts?orgabbr=lcart";
    }

    public static String getCancelOrderAPI() {
        return serverURL + "app/cancellcporders";
    }

    public static String getPlaceOrderAPI() {
        return serverURL + "app/addorders";
    }

    public static String getCartToCheckoutAPI() {
        return serverURL + "app/carttocheckout";
    }

    public static String getSendFeedbackAPI() {
        return serverURL + "app/lcpfeedback";
    }

    public static String getGenerateOTPAPI() {
        return serverURL + "app/forgotlcppassword";
    }

    public static String getOrdersAPI() {
        return serverURL + "app/vieworder";
    }

    public static String getRegistrationFormURL() {
        return serverURL + "app/newregister";
    }

    public static String getNumberCheckAPI() {
        return serverURL + "app/checkNumber";
    }

    public static String getVersionCheckAPI() {
        return serverURL + "app/versionchecklcp?version=";
    }

    public static String getEditProfileAPI() {
        return serverURL + "app/changeprofile";
    }

    public static String getSearchAndSort() {
        return serverURL + "app/lcpsearch";
    }

    public static String getRegisterTokenAPI() {
        return serverURL + "app/fcmregistration";
    }

    public static String getDeregisterTokenAPI() {
        return serverURL + "app/fcmderegistermanual";
    }

    public static final String SCROLLING_ACTIVITY_TAG = "scrolling_activity";
    public static final String CART_ACTIVITY_TAG = "cart_activity";
    public static final String CONFIRM_ORDER_ACTIVITY_TAG = "confirm_order_activity";
    public static final String SPLASH_SCREEN_ACTIVITY_TAG = "splash_screen_activity";
    public static final String LOGIN_ACTIVITY_TAG = "login_activity";
    public static final String RESET_PASSWORD_ACTIVITY_TAG = "reset_password_activity";
    public static final String CURRENT_ORDERS_ADAPTER_TAG = "current_orders_adapter";
    public static final String MY_FIREBASE_INSTANCE_ID_SERVICE_TAG = "firebase_instance_id_service";
    public static final String REGISTER1_FRAGMENT_TAG = "register1_fragment";
    public static final String REGISTER2_FRAGMENT_TAG = "register2_fragment";

    // tags used to attach the fragments
    public static final String ORDERS_FRAGMENT_TAG = "OrdersDetail"; //navItemIndex = 1
    public static final String PRODUCT_FRAGMENT_TAG = "ProductDetail"; //navItemIndex = 0
    public static final String PROFILE_FRAGMENT_TAG = "Profile"; //navItemIndex = 2
    public static final String ABOUT_US_FRAGMENT_TAG = "About Us"; //navItemIndex = 3
    public static final String CONTACT_US_FRAGMENT_TAG = "Contact Us"; //navItemIndex = 4
    public static final String FAQs_TAG = "FAQs"; //navItemIndex = 5
    public static final String FEEDBACK_ACTIVITY_TAG = "Feedback"; //navItemIndex = 6
    public static final String WATCH_VIDEOS_TAG = "Watch Videos"; //navItemIndex = 7
    public static final String HOME_TAG = "Home";  //navItemIndex = 11
    public static final String EDIT_PROFILE_FRAGMENT_TAG = "edit_profile_fragment"; //navItemIndex = 12
    public static final String SEARCH_FRAGMENT_TAG = "search_fragment"; //navItemIndex = 13

    public static final int NAV_PRODUCT_INDEX = 0;
    public static final int NAV_ORDERS_INDEX = 1;
    public static final int NAV_PROFILE_INDEX = 2;
    public static final int NAV_ABOUT_US_INDEX = 3;
    public static final int NAV_CONTACT_US_INDEX = 4;
    public static final int NAV_FAQS_INDEX = 5;
    public static final int NAV_FEEDBACK_INDEX = 6;
    public static final int NAV_WATCH_VIDEOS_INDEX = 7;
    public static final int NAV_HOME_INDEX = 11;
    public static final int NAV_EDIT_PROFILE_INDEX = 12;
    public static final int NAV_SEARCH_INDEX = 13;

    // index to identify current nav menu item
    public static int navItemIndex = 100, prevNavItemIndex = 100;

    public static String CURRENT_TAG = Master.PRODUCT_FRAGMENT_TAG;

    public static int CART_ITEM_COUNT = 0;

    public static final String
            PRODUCT_NAME = "pname",
            ITEM_TOTAL = "itemTotal",
            QUANTITY = "quantity",
            ID = "id",
            PRODUCT_UNIT = "unit",
            MINIMUM_ORDER_QUANTITY = "moq",
            IMAGE_URL = "imageurl",
            GST = "gst";

    public static final String
            FNAME = "firstname",
            LNAME = "lastname",
            EMAIL = "email",
            ADDRESS = "address",
            MOBILENUMBER = "phone",
            PASSWORD = "password",
            PINCODE = "pincode",
            UNIT_RATE = "unitRate",
            STEPPER = "stepper",
            ORG_NAME = "organizationName",
            PRODUCT_TYPE = "type",
            QUERY = "query",
            TOKEN = "token",
            AUTH_USERNAME = "lokacart@cse.iitb.ac.in",//"ruralict.iitb@gmail.com",
            AUTH_PASSWORD = "password",
            SORT_FLAG = "flag";

    public static final Boolean DEFAULT_STEPPER = true;

    public static Boolean REDIRECTED_FROM_CART = false;
    public static Boolean REDIRECTED_FROM_FEEDBACK = false;

    public static final String LOGIN = "login";

    public static final int MY_PERMISSIONS_REQUEST_READ_SMS = 1;

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager;
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetworkInfo != null && activeNetworkInfo.isConnected());
    }

    public static String productTypeValue = null;
    public static ArrayList<ProductDetail> productDetailList = null;
    public static ArrayList<ProductDetail> cartList = null;
    public static ArrayList<ProductTypeDetail> product_type = null;

    public static boolean isFromCart = false;
    private static ProgressDialog pDialog;

    public static void showProgressDialog(Context context, String message) {
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(message);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public static void dismissProgressDialog() {
        if (pDialog != null && pDialog.isShowing())
            pDialog.dismiss();
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, int count) {

        if (Build.VERSION.SDK_INT <= 15) {
            return;
        }

        CartIconDrawable badge;


        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof CartIconDrawable) {
            badge = (CartIconDrawable) reuse;
        } else {
            badge = new CartIconDrawable(context);
        }
        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    public static void updateProductList() {
        for (int i = 0; i < Master.cartList.size(); ++i) {
            for (int j = 0; j < Master.productDetailList.size(); ++j) {
                if (Master.productDetailList.get(j).getId().equals(Master.cartList.get(i).getId())) {
                    Master.productDetailList.get(j).setQuantity(Master.cartList.get(i).getQuantity());
                    break;
                }
            }
        }
    }

}