package com.mobile.ict.lokacartplus.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

public class FeedbackActivity extends AppCompatActivity {

    private EditText etFeedback;
    private final String TAG = FeedbackActivity.class.getSimpleName();
    private NetworkCommunicator networkCommunicator;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        networkCommunicator = NetworkCommunicator.getInstance();

        etFeedback = findViewById(R.id.etFeedback);
        etFeedback.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        Button buttonSendFeedback = findViewById(R.id.buttonSendFeedback);
        buttonSendFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!etFeedback.getText().toString().trim().equals("")){
                    if(Master.isNetworkAvailable(getApplicationContext())){
                        sendFeedback();
                    }else {
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                    }

                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(networkCommunicator==null) {
            networkCommunicator = NetworkCommunicator.getInstance();
        }
    }

    private void LoginRegisterDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        builder.setMessage(getResources().getString(R.string.feedback_alert_do_you_want_to_Login_Register));
        builder.setCancelable(true);
        builder.setPositiveButton(
                getString(R.string.dialog_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                        Master.REDIRECTED_FROM_FEEDBACK = true;
                        Intent i = new Intent(getApplication(), LoginActivity.class);
                        startActivity(i);

                    }
                });

        builder.setNegativeButton(
                getString(R.string.dialog_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );
        AlertDialog alert11 = builder.create();
        alert11.show();

    }

    private void sendFeedback(){
        Master.showProgressDialog(mContext, "Sending feedback...");

        JSONObject sendFeedbackObject = new JSONObject();
        try {
            sendFeedbackObject.put("content", etFeedback.getText().toString().trim());
            if(MemberDetails.getMobileNumber().equals("guest")) {
                //sendFeedbackObject.put("phonenumber", String.valueOf("1111111111"));
                Master.dismissProgressDialog();
                LoginRegisterDialog();
                return;
            } else {
                sendFeedbackObject.put("phonenumber", String.valueOf(MemberDetails.getMobileNumber()));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        networkCommunicator.data(Master.getSendFeedbackAPI(),
                Request.Method.POST,
                sendFeedbackObject,
                false,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        // Log.d(TAG, result.toString());
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null) {
                            try {
                                if(obj.getString("response").equals("success")){
                                    Toast.makeText(getApplicationContext(), "Feedback Successfully Sent! ", Toast.LENGTH_LONG).show();
                                    etFeedback.setText("");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Master.dismissProgressDialog();
                        Toast.makeText(getApplicationContext(),getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
                        //buttonSendFeedback.setEnabled(true);
                    }
                },Master.FEEDBACK_ACTIVITY_TAG, getApplicationContext());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
