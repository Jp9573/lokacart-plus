package com.mobile.ict.lokacartplus.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.interfaces.NoInternetConnectionListener;
import com.mobile.ict.lokacartplus.model.OrdersDetail;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Toshiba on 11-04-2017.
 */

@SuppressWarnings("DefaultFileTemplate")
public class CurrentOrdersAdapter extends RecyclerView.Adapter<CurrentOrdersAdapter.ViewHolder> {
    private final Context mContext;
    private final ArrayList<OrdersDetail> currentOrders;
    private final ArrayList<OrdersDetail> pastOrders;
    private OrdersInterface mCallback;

    public CurrentOrdersAdapter(Context mContext,
                                ArrayList<OrdersDetail> currentOrders,
                                ArrayList<OrdersDetail> pastOrders, OrdersInterface ordersInterface) {
        this.mContext = mContext;
        this.currentOrders = currentOrders;
        this.pastOrders = pastOrders;
        mCallback = ordersInterface;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public final TextView tvProductName;
        public final TextView tvItemTotal;
        public final TextView tvOrgName;
        public final TextView tvMark;
        public final TextView tvQuantity;
        public final TextView tvOrderId;
        public final TextView tvCancelOrder;
        public final TextView tvDeliveryDate;
        public final AppCompatImageView orderStatusImageView;

        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout

            tvProductName = v.findViewById(R.id.tvProductName);
            tvProductName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvItemTotal = v.findViewById(R.id.tvItemTotal);
            tvItemTotal.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvMark = v.findViewById(R.id.tvMark);
            tvMark.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrgName = v.findViewById(R.id.tvOrgName);
            tvOrgName.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_LIGHT));

            tvQuantity = v.findViewById(R.id.tvQuantity);
            tvQuantity.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvOrderId = v.findViewById(R.id.tvOrderId);
            tvOrderId.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvCancelOrder = v.findViewById(R.id.tvCancelOrder);
            tvCancelOrder.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            tvDeliveryDate = v.findViewById(R.id.tvDeliveryDate);
            tvDeliveryDate.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

            orderStatusImageView = v.findViewById(R.id.order_status_imageview);
        }
    }

    @Override
    public CurrentOrdersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_current_orders,parent,false);

        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        try{
            holder.tvProductName.setText(currentOrders.get(position).getName());
            holder.tvOrderId.setText(String.valueOf("Order ID : "+currentOrders.get(position).getOrderItemId()));
            holder.tvOrgName.setText(String.valueOf(currentOrders.get(position).getOrganization()));

            DecimalFormat df2 = new DecimalFormat("#.00");

            holder.tvItemTotal.setText(String.valueOf("\u20B9 "+ (Double.valueOf(df2.format(currentOrders.get(position).getQuantity() * currentOrders.get(position).getRate())))));
            holder.tvMark.setText(String.valueOf(position+1));

            holder.tvCancelOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelOrderDialog(currentOrders.get(position).getOrderItemId(), position);
                }
            });

            if(currentOrders.get(position).getQuantity()==1){
                holder.tvQuantity.setText(String.valueOf(currentOrders.get(position).getQuantity()+" unit of"));
            } else if (currentOrders.get(position).getQuantity()>1){
                holder.tvQuantity.setText(String.valueOf(currentOrders.get(position).getQuantity()+" units of"));
            }

            if(currentOrders.get(position).getStatus().equals("placed")){
                holder.tvCancelOrder.setVisibility(View.VISIBLE);
                holder.tvDeliveryDate.setVisibility(View.GONE);
                holder.orderStatusImageView.setImageResource(R.drawable.ic_slider_placed_svg);
            }

            if(currentOrders.get(position).getStatus().equals("processed")){
                holder.tvCancelOrder.setVisibility(View.GONE);
                holder.tvDeliveryDate.setVisibility(View.VISIBLE);
                holder.tvDeliveryDate.setText(String.valueOf("Expected Delivery Dt. "+ currentOrders.get(position).getDelivery_date()));
                holder.orderStatusImageView.setImageResource(R.drawable.ic_slider_processed_svg);
            }

            if(currentOrders.get(position).getStatus().equals("delivered")){
                holder.tvCancelOrder.setVisibility(View.GONE);
                holder.tvDeliveryDate.setVisibility(View.VISIBLE);
                holder.tvDeliveryDate.setText(String.valueOf("Delivery Dt. "+ currentOrders.get(position).getDelivery_date()));
                holder.orderStatusImageView.setImageResource(R.drawable.ic_slider_delivered_svg);
            }

            if(currentOrders.get(position).getStatus().equals("cancelled")){
                holder.tvDeliveryDate.setVisibility(View.VISIBLE);
                String msg = "Order cancelled";
                holder.tvDeliveryDate.setText(msg);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void cancelOrderDialog(final String orderItemId, final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.AlertDialogCustom));
        builder.setMessage(mContext.getResources().getString(R.string.alert_do_you_want_to_Cancel_Order));
        builder.setCancelable(true);
        builder.setPositiveButton(
                mContext.getString(R.string.dialog_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                        if(Master.isNetworkAvailable(mContext)){
                            Master.showProgressDialog(mContext, "Cancelling...");
                            makeCancelOrderReq(orderItemId, position);
                        }else {
                            // toast of no internet connection
                            Toast.makeText(mContext, mContext.getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                            NoInternetConnectionListener listener = (NoInternetConnectionListener) mContext;
                            listener.showNoInternetConnectionLayout();
                        }

                    }
                });

        builder.setNegativeButton(
                mContext.getString(R.string.dialog_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                }
        );
        AlertDialog alert11 = builder.create();
        alert11.show();

    }

    private void makeCancelOrderReq(String orderItemId,final int position) {
        NetworkCommunicator networkCommunicator;
        networkCommunicator = NetworkCommunicator.getInstance(/*mContext.getApplicationContext()*/);

        Map<String, String> params = new HashMap<>();
        params.put("itemId", orderItemId);

        networkCommunicator.data(Master.getCancelOrderAPI(),
                Request.Method.POST,
                new JSONObject(params),
                true,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null) {

                            try {
                                if (obj.getString("status").equals("success")) {

                                    Toast.makeText(mContext.getApplicationContext(), "Order cancelled Successfully, refreshing the data.", Toast.LENGTH_LONG).show();

                                    // used to move card from current to past orders
                                    mCallback.updateAdapter(position, currentOrders, pastOrders);

                                    //to refresh data
                                    Master.showProgressDialog(mContext, "Fetching OrdersDetail...");
                                    mCallback.showOrdersFunction();

                                } else if (obj.getString("status").equals("failure")) {

                                    Toast.makeText(mContext.getApplicationContext(), obj.getString("reason"), Toast.LENGTH_LONG).show();

                                    // used to move card from current to past orders
                                    mCallback.updateAdapter(position, currentOrders, pastOrders);

                                    //to refresh data
                                    Master.showProgressDialog(mContext, "Fetching OrdersDetail...");
                                    mCallback.showOrdersFunction();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                    }
                },Master.CURRENT_ORDERS_ADAPTER_TAG, mContext.getApplicationContext());

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return currentOrders.size();
    }

    public interface OrdersInterface {
        void showOrdersFunction();
        void updateAdapter(int position, ArrayList<OrdersDetail> currentOrders, ArrayList<OrdersDetail> pastOrders);
    }

}