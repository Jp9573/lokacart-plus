package com.mobile.ict.lokacartplus.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.fragment.FAQsFragment1;

public class FAQsActivity extends AppCompatActivity {

    private Toolbar toolbar;
    public static int navItem = 1;
    public TextView tvHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faqs);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);

        tvHeader = findViewById(R.id.tvHeader);

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        if (findViewById(R.id.fragment_container) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            FAQsFragment1 firstFragment = new FAQsFragment1();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            //firstFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, firstFragment).commit();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == android.R.id.home) {
            // finish the activity
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void changeToolBar()
    {
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white);
    }

    @Override
    public boolean onSupportNavigateUp() {
        if(navItem == 2) {

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new FAQsFragment1())
                    .commit();

            tvHeader.setText(getString(R.string.choose_catageroy));

        }
        return true;
    }


    @Override
    public void onBackPressed() {
        if(navItem == 2) {

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragment_container, new FAQsFragment1())
                    .commit();

            tvHeader.setText(getString(R.string.choose_catageroy));

            return;

        }

        super.onBackPressed();
    }


}
