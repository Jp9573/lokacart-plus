package com.mobile.ict.lokacartplus.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.interfaces.DeleteProductListener;
import com.mobile.ict.lokacartplus.model.ProductDetail;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.utils.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.adapter.CartAdapter;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class CartActivity extends AppCompatActivity implements DeleteProductListener {

    private RecyclerView cartRecyclerView;
    private Button checkoutButton;
    private RelativeLayout emptyCartRelativeLayout;
    private RelativeLayout totalRelativeLayout;
    private LinearLayout itemsLinearLayout;
    private CartAdapter cartAdapter;
    private TextView cartTotal,tvItemTotal;
    private DBHelper dbHelper;
    private double sum = 0.0;
    private final String TAG = CartActivity.class.getSimpleName();
    private final DecimalFormat df2 = new DecimalFormat("#.00");
    private NetworkCommunicator networkCommunicator;
    private Context mContext;
    private int size;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Master.cartList = new ArrayList<>();
        mContext = this;
        setContentView(R.layout.activity_cart);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("My Cart");

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);

        checkoutButton = findViewById(R.id.buttonCheckout);

        networkCommunicator = NetworkCommunicator.getInstance();

        Button buttonShopNow = findViewById(R.id.buttonShopNow);
        buttonShopNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferenceConnector.writeString(getApplicationContext(), "redirectto", "homepage");
                Intent i = new Intent(getApplicationContext(),DashboardActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                CartActivity.this.finish();

            }
        });

        emptyCartRelativeLayout = findViewById(R.id.cartEmptyRelativeLayout);
        emptyCartRelativeLayout.setVisibility(View.GONE);

        itemsLinearLayout = findViewById(R.id.ll_items);
        totalRelativeLayout = findViewById(R.id.rl_total);

        cartRecyclerView = findViewById(R.id.cart_recycler_view);
        cartRecyclerView.setLayoutManager(layoutManager);
        cartRecyclerView.setHasFixedSize(false);
        cartRecyclerView.setNestedScrollingEnabled(false);

        cartTotal = findViewById(R.id.tvCartTotal);
        cartTotal.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));

        tvItemTotal = findViewById(R.id.tvItemTotal);
        tvItemTotal.setTypeface(Typer.set(this).getFont(Font.ROBOTO_REGULAR));


        dbHelper = DBHelper.getInstance(this);
        if(Master.cartList!=null){
            Master.cartList.clear();
        }

        Master.cartList = dbHelper.getCartDetails(MemberDetails.getMobileNumber());

        if (Master.cartList.isEmpty()) {
            Master.CART_ITEM_COUNT = 0;
            invalidateOptionsMenu();

            checkoutButton.setVisibility(View.GONE);
            itemsLinearLayout.setVisibility(View.GONE);
            totalRelativeLayout.setVisibility(View.GONE);
            emptyCartRelativeLayout.setVisibility(View.VISIBLE);
            cartRecyclerView.setVisibility(View.GONE);

        } else {

            emptyCartRelativeLayout.setVisibility(View.GONE);
            cartRecyclerView.setVisibility(View.VISIBLE);
            checkoutButton.setVisibility(View.VISIBLE);
            itemsLinearLayout.setVisibility(View.VISIBLE);
            totalRelativeLayout.setVisibility(View.VISIBLE);

            size = Master.cartList.size();
            Master.CART_ITEM_COUNT = size;
            invalidateOptionsMenu();

            for (int i = 0; i < size; ++i) {
                sum = sum + Master.cartList.get(i).getItemTotal();
            }
            int  tot = (int)Math.round(sum);
            cartTotal.setText(String.valueOf("\u20B9 " + df2.format(tot)));

            setTvItemTotal();

        }


        cartAdapter = new CartAdapter(this, cartTotal, tvItemTotal);
        cartRecyclerView.setAdapter(cartAdapter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        checkoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(MemberDetails.getMobileNumber().equals("guest")){
                    LoginRegisterDialog();
                } else {
                    if(Master.isNetworkAvailable(getApplicationContext()))
                    {
                        Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
                        checkPriceAndQuantity();
                    }else
                    {
                        // toast of no internet connection
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                    }

                }
            }
        });

    }

    private void setTvItemTotal(){
        if(Master.CART_ITEM_COUNT>1) {
            String msg = "You have " + Master.CART_ITEM_COUNT + " items";
            tvItemTotal.setText(msg);
        } else {
            String msg = "You have " + Master.CART_ITEM_COUNT + " item";
            tvItemTotal.setText(msg);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        saveCart();
        Master.isFromCart = true;
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        saveCart();

        Master.isFromCart = true;
        finish();
    }

    private void checkPriceAndQuantity(){

        JSONArray orderItemsArray = new JSONArray();

        size = Master.cartList.size();
        for(int i = 0; i< size; i++){
            JSONObject orderItemsObject = new JSONObject();
            try {

                orderItemsObject.put("prod_id", String.valueOf(Master.cartList.get(i).getId()));
                orderItemsObject.put("cartquantity", String.valueOf(Master.cartList.get(i).getQuantity()));
                orderItemsObject.put("unitRate", String.valueOf(Master.cartList.get(i).getUnitRate()));
                orderItemsObject.put("gst", String.valueOf(Master.cartList.get(i).getGst()));
                orderItemsObject.put("moq", String.valueOf(Master.cartList.get(i).getMoq()));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            orderItemsArray.put(orderItemsObject);
        }

        JSONObject OrderItemsCheckObject = new JSONObject();
        try {
            OrderItemsCheckObject.put("orderItems", orderItemsArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        sum = 0.0;
        for (int i = 0; i < size; ++i) {
            sum = sum + Master.cartList.get(i).getItemTotal();
        }

        networkCommunicator.data(Master.getCartToCheckoutAPI(),
                Request.Method.POST,
                OrderItemsCheckObject,
                false,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null) {
                            if(checkStatus(obj)) {
                                int  tot = (int)Math.round(sum);

                                Intent i = new Intent(getApplication(), ConfirmOrderActivity.class);
                                i.putExtra("cartTotal", String.valueOf(df2.format(tot)));
                                startActivity(i);
                            }
                        }
                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getApplicationContext(),getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
                    }
                },Master.CART_ACTIVITY_TAG, getApplicationContext());

    }

    private boolean checkStatus(JSONObject jsonObject){
        JSONArray pricesJSONArray;

        try {
            pricesJSONArray = jsonObject.getJSONArray("prices");
            StringBuilder errorString1 = new StringBuilder();
            StringBuilder errorString2 = new StringBuilder();
            StringBuilder errorString3 = new StringBuilder();

            boolean unitRateflag = false, quantityflag = false, moqflag=false;

            for(int i = 0; i < pricesJSONArray.length(); i++) {
                JSONObject jo = pricesJSONArray.getJSONObject(i);

                String quantity = jo.getString("quantity");
                String unitRate = jo.getString("unitRate");
                String moq = jo.getString("moq");

                ProductDetail productDetail = Master.cartList.get(i);
                if(productDetail.getMoq() != Integer.valueOf(jo.getString("moqvalue"))) {
                    Master.cartList.get(i).setMoq(Integer.valueOf(jo.getString("moqvalue")));
                    cartAdapter.notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(), "Minimum order quantity of " + jo.getString("name") + " has been updated to " + jo.getString("moqvalue"), Toast.LENGTH_LONG).show();
                }

                if(unitRate.equals("false") || moq.equals("false")) {

                    if(unitRate.equals("false")) {
                        unitRateflag = true;
                        errorString1.append("GST/Price changed. Price of ").append(jo.getString("name")).append(" has been updated to ").append(formatValue(Math.ceil(Double.valueOf(jo.getString("currentRate"))))).append("\n");

                    }
                    if(moq.equals("false")) {
                        moqflag = true;
                        errorString2.append("Minimum order quantity of ").append(jo.getString("name")).append(" has been updated to ").append(jo.getString("moqvalue")).append("\n");

                    }
                    String productId = jo.getString("id");
                    String currentRate = formatValue(Math.ceil(Double.valueOf(jo.getString("currentRate"))));//jo.getString("currentRate");
                    String gst = jo.getString("gst");
                    String moqqty = jo.getString("moqvalue");

                    updateCart(productId, currentRate,gst,moqqty);
                    cartAdapter.notifyDataSetChanged();
                }

                if(quantity.equals("false")) {
                    quantityflag = true;
                    errorString3.append("Only ").append(jo.getString("avilablequantity")).append(" ").append(jo.getString("unit")).append(" available of ").append(jo.getString("name")).append("\n");
                }

            }

            if(unitRateflag&&moqflag && quantityflag)
            {
                Toast.makeText(getApplicationContext(), errorString1 + "\n" + errorString2+ "\n" + errorString3 , Toast.LENGTH_LONG).show();
                return false;
            }
            else if(unitRateflag&&!moqflag && !quantityflag)
            {
                Toast.makeText(getApplicationContext(), errorString1.toString(), Toast.LENGTH_LONG).show();
                return false;
            }
            else if(unitRateflag && !moqflag && quantityflag)
            {
                Toast.makeText(getApplicationContext(), errorString1 + "\n" + errorString3  , Toast.LENGTH_LONG).show();
                return false;
            }
            else if(unitRateflag && moqflag && !quantityflag)
            {
                Toast.makeText(getApplicationContext(), errorString1 + "\n" + errorString2  , Toast.LENGTH_LONG).show();
                return false;
            }
            else if(!unitRateflag && moqflag && quantityflag)
            {
                Toast.makeText(getApplicationContext(), errorString2 + "\n" + errorString3  , Toast.LENGTH_LONG).show();
                return false;
            }
            else if(!unitRateflag && moqflag && !quantityflag)
            {
                Toast.makeText(getApplicationContext(), errorString2.toString(), Toast.LENGTH_LONG).show();
                return false;
            }
            else if(!unitRateflag && !moqflag && quantityflag)
            {
                Toast.makeText(getApplicationContext(), errorString3.toString(), Toast.LENGTH_LONG).show();
                return false;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return true;
    }

    public String formatValue(double d) {
        if(d == (long) d) {
            return String.format("%d", (long) d);
        }else {
            return new DecimalFormat("##.##").format(d);
        }
    }

    private void updateCart(String productId, String currentRate,String gst,String moq) {

        size = Master.productDetailList.size();
        for (int j = 0; j < size; ++j) {
            ProductDetail productDetail = Master.productDetailList.get(j);
            if (productDetail.getId().equals(productId)) {
                productDetail.setUnitRate(Double.parseDouble(currentRate));
                productDetail.setGst(Integer.parseInt(gst));
                productDetail.setMoq(Integer.parseInt(moq));

                break;
            }
        }

        size = Master.cartList.size();
        for (int j = 0; j < size; ++j)
        {
            ProductDetail productDetail = Master.cartList.get(j);
            if (productDetail.getId().equals(productId))
            {
                productDetail.setUnitRate(Double.parseDouble(currentRate));
                productDetail.setGst(Integer.parseInt(gst));
                productDetail.setMoq(Integer.parseInt(moq));

                /*if(productDetail.getMoq() != Integer.parseInt(moq)) {
                    Toast.makeText(getApplicationContext(), "Minimum order quantity of " + productDetail.getName() + " has been updated to " + moq, Toast.LENGTH_LONG).show();
                }*/

                if(productDetail.getQuantity() < Integer.parseInt(moq))
                {
                    int diff = Integer.parseInt(moq) - productDetail.getQuantity();
                    int qty= diff+ productDetail.getQuantity();
                    productDetail.setQuantity(qty);

                }

                //For changing total price if any change in unit price

                productDetail.setItemTotal(productDetail.getQuantity() * productDetail.getUnitRate());

                dbHelper.updateProduct(
                        String.valueOf(productDetail.getUnitRate()),
                        String.valueOf(productDetail.getQuantity()),
                        String.valueOf(productDetail.getItemTotal()),
                        String.valueOf(productDetail.getName()),
                        MemberDetails.getMobileNumber(),
                        String.valueOf(productDetail.getOrganization()),
                        String.valueOf(productDetail.getId()),
                        String.valueOf(productDetail.getImageUrl()),
                        String.valueOf(productDetail.getUnit()),
                        String.valueOf(productDetail.getMoq()),
                        String.valueOf(productDetail.getGst())
                );

                break;
            }
        }

        //For updating grand total at the end

        sum = 0.0;
        for (int i = 0; i < size; ++i) {
            sum = sum + Master.cartList.get(i).getItemTotal();
        }
        cartTotal.setText(String.valueOf("\u20B9 " + Double.valueOf(df2.format(sum))));

    }

    private void saveCart() {
        size = Master.cartList.size();
        for (int i = 0; i < size; i++) {
            if (Master.cartList.get(i).getQuantity() == 0)
            {
                dbHelper.deleteProduct(MemberDetails.getMobileNumber(), Master.cartList.get(i).getId());
            }
            else
            {
                dbHelper.updateProduct(
                        String.valueOf(Master.cartList.get(i).getUnitRate()),
                        String.valueOf(Master.cartList.get(i).getQuantity()),
                        String.valueOf(Master.cartList.get(i).getItemTotal()),
                        String.valueOf(Master.cartList.get(i).getName()),
                        MemberDetails.getMobileNumber(),
                        String.valueOf(Master.cartList.get(i).getOrganization()),
                        String.valueOf(Master.cartList.get(i).getId()),
                        String.valueOf(Master.cartList.get(i).getImageUrl()),
                        String.valueOf(Master.cartList.get(i).getUnit()),
                        String.valueOf(Master.cartList.get(i).getMoq()),
                        String.valueOf(Master.cartList.get(i).getGst())
                );

            }
        }
    }

    @Override
    public void deleteProduct(final int position, final String productID) {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        builder.setMessage(getResources().getString(R.string.alert_do_you_really_want_to_remove));
        builder.setCancelable(true);
        builder.setPositiveButton(
                getString(R.string.dialog_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        size= Master.productDetailList.size();
                        for (int j = 0; j < size; ++j) {
                            ProductDetail productDetail = Master.productDetailList.get(j);
                            if (productDetail.getId().equals(productID)) {
                                productDetail.setQuantity(0);
                                break;
                            }
                        }

                        Master.cartList.remove(position);
                        cartRecyclerView.removeViewAt(position);
                        cartAdapter.notifyItemRemoved(position);
                        cartAdapter.notifyItemRangeChanged(position, Master.cartList.size());
                        cartAdapter.notifyDataSetChanged();
                        cartRecyclerView.invalidate();

                        dialog.dismiss();

                        dbHelper.deleteProduct(MemberDetails.getMobileNumber(), productID);

                        if (Master.cartList.isEmpty()) {
                            Master.CART_ITEM_COUNT = 0;
                            checkoutButton.setVisibility(View.GONE);

                            emptyCartRelativeLayout.setVisibility(View.VISIBLE);

                            itemsLinearLayout.setVisibility(View.GONE);
                            totalRelativeLayout.setVisibility(View.GONE);
                            cartRecyclerView.setVisibility(View.GONE);

                        } else {

                            sum = 0.0;
                            size = Master.cartList.size();
                            for (int i = 0; i < size; ++i) {
                                sum = sum + Master.cartList.get(i).getItemTotal();
                            }

                            cartTotal.setText(String.valueOf("\u20B9 " + Double.valueOf(df2.format(sum))));
                            setTvItemTotal();
                        }
                    }
                });

        builder.setNegativeButton(
                getString(R.string.dialog_cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }
        );
        AlertDialog alert11 = builder.create();
        alert11.show();

    }

    private void LoginRegisterDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.AlertDialogCustom));
        builder.setMessage(getResources().getString(R.string.alert_do_you_want_to_Login_Register));
        builder.setCancelable(true);
        builder.setPositiveButton(
                getString(R.string.dialog_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();
                        Master.REDIRECTED_FROM_CART = true;
                        Intent i = new Intent(getApplication(), LoginActivity.class);
                        startActivity(i);

                    }
                });

        builder.setNegativeButton(
                getString(R.string.dialog_no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                }
        );
        AlertDialog alert11 = builder.create();
        alert11.show();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(dbHelper==null)dbHelper=DBHelper.getInstance(this);
        dbHelper.getSignedInProfile();

        if(networkCommunicator==null) {
            networkCommunicator = NetworkCommunicator.getInstance();
        }

        if (Master.cartList.isEmpty()) {
            Master.CART_ITEM_COUNT = 0;
            invalidateOptionsMenu();

            checkoutButton.setVisibility(View.GONE);
            itemsLinearLayout.setVisibility(View.GONE);
            totalRelativeLayout.setVisibility(View.GONE);
            emptyCartRelativeLayout.setVisibility(View.VISIBLE);
            cartRecyclerView.setVisibility(View.GONE);

        } else {

            sum = 0.0;
            size = Master.cartList.size();
            for (int i = 0; i < size; ++i) {
                sum = sum + Master.cartList.get(i).getItemTotal();
            }

            cartTotal.setText(String.valueOf("\u20B9 " + Double.valueOf(df2.format(sum))));
            setTvItemTotal();

            cartAdapter = new CartAdapter(this, cartTotal, tvItemTotal);
            cartRecyclerView.setAdapter(cartAdapter);

        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        saveCart();
    }

}
