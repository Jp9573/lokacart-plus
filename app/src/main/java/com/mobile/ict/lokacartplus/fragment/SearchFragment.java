package com.mobile.ict.lokacartplus.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.activity.DashboardActivity;
import com.mobile.ict.lokacartplus.adapter.SearchAdapter;


public class SearchFragment extends Fragment {

    private RecyclerView searchRecyclerView;
    private RelativeLayout rlNoMatchSearchFragment;
    private View rootView;

    public SearchFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_search, container, false);

        //code in onActivityCreated was here...

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // This is used for implementing back button
        ((DashboardActivity)getActivity()).changeToolBar("Search");

        rlNoMatchSearchFragment = rootView.findViewById(R.id.rlNoMatchSearchFragment);

        searchRecyclerView = rootView.findViewById(R.id.search_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        searchRecyclerView.setNestedScrollingEnabled(false);
        searchRecyclerView.setHasFixedSize(false);

        RecyclerView.LayoutManager searchLayoutManager = new LinearLayoutManager(getActivity());
        searchRecyclerView.setLayoutManager(searchLayoutManager);

    }

    @Override
    public void onResume() {
        super.onResume();

        if (Master.productDetailList != null){
            if (Master.productDetailList.size() > 0){
                rlNoMatchSearchFragment.setVisibility(View.GONE);
                try {
                    Master.updateProductList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                RecyclerView.Adapter searchAdapter = new SearchAdapter(getContext());
                searchRecyclerView.setAdapter(searchAdapter);
            } else {
            rlNoMatchSearchFragment.setVisibility(View.VISIBLE);
            }
        }

    }

}
