package com.mobile.ict.lokacartplus.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.utils.Validation;
import com.mobile.ict.lokacartplus.activity.LoginActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterFragment1 extends Fragment {

    private EditText etEnterPhoneNumber, etEnterPassword, etConfirmPassword;
    private final String TAG = RegisterFragment1.class.getSimpleName();
    private TextInputLayout etEnterPasswordLayout, etConfirmPasswordLayout;
    private NetworkCommunicator networkCommunicator;
    private OnFragment1InteractionListener mListener;
    private Context mContext;
    private View rootView;

    public RegisterFragment1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_register1, container, false);

        mContext = getContext();

        //code in onActivityCreated was here...

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        etEnterPasswordLayout = rootView.findViewById(R.id.etEnterPasswordLayout);
        etConfirmPasswordLayout = rootView.findViewById(R.id.etConfirmPasswordLayout);
        networkCommunicator = NetworkCommunicator.getInstance(/*getContext()*/);

        etEnterPhoneNumber = rootView.findViewById(R.id.etEnterPhoneNumber);
        etEnterPassword = rootView.findViewById(R.id.etEnterPassword);
        etConfirmPassword = rootView.findViewById(R.id.etConfirmPassword);
        TextView tvLoginNow = rootView.findViewById(R.id.tvLoginNow);
        tvLoginNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getContext(), LoginActivity.class);
                startActivity(i);
            }
        });
        Button nextButton = rootView.findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validation()){

                    if(Master.isNetworkAvailable(getContext())){
                        Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
                        checkNumberExist();
                    }else {
                        Toast.makeText(getContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                    }

                }

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(networkCommunicator==null) {
            networkCommunicator = NetworkCommunicator.getInstance();
        }
    }

    private void onButtonPressed(Bundle bundle) {
        if (mListener != null) {
            mListener.onFragment1Interaction(bundle);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragment1InteractionListener) {
            mListener = (OnFragment1InteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragment1InteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragment1InteractionListener {
        void onFragment1Interaction(Bundle bundle);
    }

    private boolean validation(){

        if(!Validation.isValidPhoneNumber(etEnterPhoneNumber.getText().toString().trim())){
            etEnterPhoneNumber.requestFocus();
            etEnterPhoneNumber.setError("Please enter valid Phone Number");
            return false;
        }
        if(!Validation.isValidPassword(etEnterPassword.getText().toString().trim())){
            etEnterPasswordLayout.requestFocus();
            etEnterPasswordLayout.setBackgroundResource(R.drawable.tv_border_red);
            Toast.makeText(getContext(), "Please enter valid password", Toast.LENGTH_LONG).show();
            return false;
        } else {
            etEnterPasswordLayout.requestFocus();
            etEnterPasswordLayout.setBackgroundResource(R.color.etBackground);
        }

        if(!Validation.isPasswordMatching(etEnterPassword.getText().toString().trim(), etConfirmPassword.getText().toString().trim())){
            etConfirmPasswordLayout.requestFocus();
            etConfirmPasswordLayout.setBackgroundResource(R.drawable.tv_border_red);
            Toast.makeText(getContext(), "Password didn't match", Toast.LENGTH_LONG).show();
            return false;
        } else{
            etConfirmPasswordLayout.requestFocus();
            etConfirmPasswordLayout.setBackgroundResource(R.color.etBackground);
        }

        return true;
    }


    private void checkNumberExist() {
        Map<String, String> params = new HashMap<>();
        params.put("password", etEnterPassword.getText().toString().trim());
        params.put("phonenumber", etEnterPhoneNumber.getText().toString().trim());

        networkCommunicator.data(Master.getNumberCheckAPI(),
                Request.Method.POST,
                new JSONObject(params),
                false,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null) {
                            try {
                                if(obj.getString("status").equals("success")){
                                    if(obj.getString("exist").equals("no")){
                                        Bundle b = new Bundle();
                                        b.putString("EnterPhoneNumber", etEnterPhoneNumber.getText().toString().trim());
                                        b.putString("EnterPassword", etEnterPassword.getText().toString().trim());
                                        onButtonPressed(b);
                                    } else if(obj.getString("exist").equals("yes")){
                                        Toast.makeText(getContext(), "Phone number already registered! Please Login.",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getContext(),getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
                    }
                },Master.REGISTER1_FRAGMENT_TAG, getContext());

    }

}
