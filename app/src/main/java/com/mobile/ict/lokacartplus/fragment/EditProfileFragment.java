package com.mobile.ict.lokacartplus.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyLog;
import com.mobile.ict.lokacartplus.interfaces.NoInternetConnectionListener;
import com.mobile.ict.lokacartplus.network.NetworkCommunicator;
import com.mobile.ict.lokacartplus.network.NetworkException;
import com.mobile.ict.lokacartplus.network.NetworkResponse;
import com.mobile.ict.lokacartplus.utils.Master;
import com.mobile.ict.lokacartplus.R;
import com.mobile.ict.lokacartplus.utils.SharedPreferenceConnector;
import com.mobile.ict.lokacartplus.utils.Validation;
import com.mobile.ict.lokacartplus.activity.DashboardActivity;
import com.mobile.ict.lokacartplus.database.DBHelper;
import com.mobile.ict.lokacartplus.model.MemberDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class EditProfileFragment extends Fragment {

    private EditText etFirstName, etLastName, etAddress, etPincode, etEmail;
    private final String TAG = RegisterFragment2.class.getSimpleName();
    private NetworkCommunicator networkCommunicator;
    private OnEditProfileFragmentInteractionListener mListener;
    private Context mContext;
    private View rootView;

    public EditProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_edit_profile, container, false);
        setHasOptionsMenu(true);

        // This is used for implementing back button
        ((DashboardActivity)getActivity()).changeToolBar("Edit Profile");

        mContext = getContext();

        //code in onActivityCreated was here...

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        networkCommunicator = NetworkCommunicator.getInstance(/*getContext()*/);

        etFirstName = rootView.findViewById(R.id.etFirstName);
        etFirstName.setText(MemberDetails.getFname());

        etFirstName.addTextChangedListener(new TextWatcher() {

            private boolean mAllowChange = true;
            private CharSequence mPreviousCharSequence = "";
            private final Pattern mPattern = Pattern.compile("[A-Za-z]*");

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAllowChange = mPattern.matcher(s.toString()).matches();
                if(mAllowChange) {
                    mPreviousCharSequence = s;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!mAllowChange) {
                    if(mPattern.matcher(mPreviousCharSequence.toString()).matches()) {
                        etFirstName.setText(mPreviousCharSequence);
                    } else {
                        etFirstName.setText("");
                    }
                }
            }
        });

        etLastName = rootView.findViewById(R.id.etLastName);
        etLastName.setText(MemberDetails.getLname());

        etAddress = rootView.findViewById(R.id.etAddress);
        etAddress.setText(MemberDetails.getAddress());

        etPincode = rootView.findViewById(R.id.etPincode);
        etPincode.setText(MemberDetails.getPincode());

        etEmail = rootView.findViewById(R.id.etEmail);
        etEmail.setText(MemberDetails.getEmail());

        Button submitButton = rootView.findViewById(R.id.submitButton);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validation()){
                    if(Master.isNetworkAvailable(getContext())){
                        Master.showProgressDialog(mContext, getString(R.string.pdialog_loading));
                        makeJsonObjReq();
                    }else {
                        // toast of no internet connection
                        Toast.makeText(getContext(), getString(R.string.toast_check_internet), Toast.LENGTH_LONG).show();
                        NoInternetConnectionListener listener = (NoInternetConnectionListener) getContext();
                        listener.showNoInternetConnectionLayout();
                    }

                }

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        if(networkCommunicator==null) {
            networkCommunicator = NetworkCommunicator.getInstance();
        }
    }

    private void onButtonPressed() {
        if (mListener != null) {
            mListener.onEditProfileFragmentInteraction();
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnEditProfileFragmentInteractionListener) {
            mListener = (OnEditProfileFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnEditProfileFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnEditProfileFragmentInteractionListener {
        void onEditProfileFragmentInteraction();
    }

    private void makeJsonObjReq() {

        Map<String, String> params = new HashMap<>();
        params.put(Master.MOBILENUMBER, MemberDetails.getMobileNumber());
        params.put(Master.FNAME, etFirstName.getText().toString().trim());
        params.put(Master.LNAME, etLastName.getText().toString().trim());
        params.put(Master.ADDRESS, etAddress.getText().toString().trim());
        params.put(Master.EMAIL, etEmail.getText().toString().trim());
        params.put(Master.PINCODE, etPincode.getText().toString().trim());

        networkCommunicator.data(Master.getEditProfileAPI(),
                Request.Method.POST,
                new JSONObject(params),
                false,new NetworkResponse.Listener() {

                    @Override
                    public void onResponse(Object result) {
                        Master.dismissProgressDialog();
                        String response = (String)result;
                        JSONObject obj = null;
                        try {
                            obj = new JSONObject(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(obj != null) {
                            checkStatus(obj);
                        }

                    }
                }, new NetworkResponse.ErrorListener()
                {
                    @Override
                    public void onError(NetworkException error) {
                        Master.dismissProgressDialog();
                        VolleyLog.d(TAG, "Error: " + error.getMessage());
                        Toast.makeText(getContext(), getString(R.string.toast_technical_issue),Toast.LENGTH_LONG).show();
                    }
                },Master.EDIT_PROFILE_FRAGMENT_TAG, getContext());

    }

    private void checkStatus(JSONObject response){

        try {
            if(response.getString("status").equals("success")){

                try {
                    new MemberDetails(etFirstName.getText().toString().trim(),etLastName.getText().toString().trim(), etAddress.getText().toString().trim(), etPincode.getText().toString().trim(), etEmail.getText().toString().trim(), MemberDetails.getMobileNumber());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                DBHelper dbHelper = DBHelper.getInstance(getContext());
                dbHelper.updateProfile(MemberDetails.getMobileNumber());

                ((DashboardActivity)getActivity()).loadNavHeader();

                SharedPreferenceConnector.writeBoolean(getContext(), Master.STEPPER, false);

                onButtonPressed();
                final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
            else {
                Toast.makeText(getContext(), response.getString("status"),
                        Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private boolean validation(){

        if(Validation.isNull(etFirstName.getText().toString().trim())){
            etFirstName.setError("First Name is required");
            return false;
        }

        if(Validation.isNull(etLastName.getText().toString().trim())){
            etLastName.setError("Last Name is required");
            return false;
        }

        if(Validation.isNull(etAddress.getText().toString().trim())){
            etAddress.setError("Address is required");
            return false;
        }

        if(!Validation.isValidPincode(etPincode.getText().toString().trim())){
            etPincode.setError("Please enter valid pincode");
            return false;
        }

        if(!Validation.isValidEmail(etEmail.getText().toString().trim()) && !Validation.isNull(etEmail.getText().toString().trim())){
            etEmail.setError("Please enter valid Email-id");
            return false;
        }

        return true;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

}
