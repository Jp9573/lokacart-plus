package com.mobile.ict.lokacartplus.network;


public class NetworkException extends Exception {

    public NetworkException() {}

    public NetworkException(String message){
        super(message);
    }
}
