package com.mobile.ict.lokacartplus.interfaces;

/**
 * Created by root on 23/4/16.
 */
@SuppressWarnings("DefaultFileTemplate")
public interface DeleteProductListener {
    void deleteProduct(int position, String productID);
}
