package com.mobile.ict.lokacartplus.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.elmargomez.typer.Font;
import com.elmargomez.typer.Typer;
import com.mobile.ict.lokacartplus.R;

/**
 * Created by Toshiba on 17-05-2017.
 */


@SuppressWarnings("DefaultFileTemplate")
public class FAQsAdapter extends RecyclerView.Adapter<FAQsAdapter.ViewHolder> {
    private final String[] FAQsCategory;

    public FAQsAdapter(String[] FAQsCategory) {
        this.FAQsCategory = FAQsCategory;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public final TextView tvLabel;

        public ViewHolder(View v){
            super(v);
            // Get the widget reference from the custom layout
            tvLabel = v.findViewById(R.id.tvLabel);
            tvLabel.setTypeface(Typer.set(v.getContext()).getFont(Font.ROBOTO_REGULAR));

        }
    }

    @Override
    public FAQsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_faqs1,parent,false);
        // Return the ViewHolder
        return new ViewHolder(v);
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        try{
            holder.tvLabel.setText(FAQsCategory[position]);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return FAQsCategory.length;
    }

}