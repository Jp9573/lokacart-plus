package com.mobile.ict.lokacartplus.utils;

import android.content.Context;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AppCompatDelegate;

public class LokacartPlusApplication extends MultiDexApplication {

	//private static final String TAG = LokacartPlusApplication.class.getSimpleName();

	public static Context context;

	private static LokacartPlusApplication mInstance;

	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
		context = this.getApplicationContext();
	}

	static{
		AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
	}

	public static synchronized LokacartPlusApplication getInstance() {
		return mInstance;
	}

}
