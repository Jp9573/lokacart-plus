package com.mobile.ict.lokacartplus.volley;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.mobile.ict.lokacartplus.utils.LokacartPlusApplication;

public class VolleyRequestQueue{

    private static VolleyRequestQueue mInstance;
    private RequestQueue mRequestQueue;

    private VolleyRequestQueue(){
        mRequestQueue = getRequestQueue();
    }

    public static synchronized VolleyRequestQueue getInstance() {
        if (mInstance == null) {

            mInstance = new VolleyRequestQueue();
        }
        return mInstance;
    }

    private RequestQueue getRequestQueue()
    {
        if (mRequestQueue == null)
        {
            mRequestQueue = Volley.newRequestQueue(LokacartPlusApplication.context);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req)
    {
        getRequestQueue().add(req);
    }
}