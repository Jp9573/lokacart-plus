package com.mobile.ict.lokacartplus.fcm;

import android.app.NotificationManager;
import android.content.Context;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.mobile.ict.lokacartplus.R;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private static int broadcastId = 100;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {

            String NotificationTAG = remoteMessage.getNotification().getTag();

            switch (NotificationTAG) {
                case "0":
                    sendChangedOrderNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
                    break;
                case "1":
                    sendDeletedOrderNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
                    break;
                case "2":
                    sendProcessedOrderNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
                    break;
                case "3":
                    sendDeliveredOrderNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
                    break;
                case "4":
                    sendBroadcastMessageNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
                    break;
            }

        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendChangedOrderNotification(String messageBody, String messageTitle) {

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,"FCMLCP")
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setGroup("changed_orders");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0/* ID of notification */, notificationBuilder.build());
    }


    private void sendDeletedOrderNotification(String messageBody, String messageTitle) {

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "FCMLCP")
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setGroup("deleted_orders");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(1/* ID of notification */, notificationBuilder.build());
    }

    private void sendProcessedOrderNotification(String messageBody, String messageTitle) {

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "FCMLCP")
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setGroup("processed_orders");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(2/* ID of notification */, notificationBuilder.build());
    }

    private void sendDeliveredOrderNotification(String messageBody, String messageTitle) {

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "FCMLCP")
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setGroup("delivered_orders");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(3/* ID of notification */, notificationBuilder.build());
    }

    private void sendBroadcastMessageNotification(String messageBody, String messageTitle) {

        broadcastId++;

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, "FCMLCP")
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setGroup("broadcast_message");

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(broadcastId/* ID of notification */, notificationBuilder.build());
    }

}